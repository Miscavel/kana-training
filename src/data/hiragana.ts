import { Entry } from "../interface/entry";

export function getHiraganaList(): Array<Entry> {
    return [
        {
            "id": "hiragana_0",
            "japanese": "あ",
            "romanization": "a",
            "isMonograph": true
        },
        {
            "id": "hiragana_1",
            "japanese": "い",
            "romanization": "i",
            "isMonograph": true
        },
        {
            "id": "hiragana_2",
            "japanese": "う",
            "romanization": "u",
            "isMonograph": true
        },
        {
            "id": "hiragana_3",
            "japanese": "え",
            "romanization": "e",
            "isMonograph": true
        },
        {
            "id": "hiragana_4",
            "japanese": "お",
            "romanization": "o",
            "isMonograph": true
        },
        {
            "id": "hiragana_5",
            "japanese": "か",
            "romanization": "ka",
            "isMonograph": true
        },
        {
            "id": "hiragana_6",
            "japanese": "き",
            "romanization": "ki",
            "isMonograph": true
        },
        {
            "id": "hiragana_7",
            "japanese": "く",
            "romanization": "ku",
            "isMonograph": true
        },
        {
            "id": "hiragana_8",
            "japanese": "け",
            "romanization": "ke",
            "isMonograph": true
        },
        {
            "id": "hiragana_9",
            "japanese": "こ",
            "romanization": "ko",
            "isMonograph": true
        },
        {
            "id": "hiragana_10",
            "japanese": "さ",
            "romanization": "sa",
            "isMonograph": true
        },
        {
            "id": "hiragana_11",
            "japanese": "し",
            "romanization": "shi",
            "isMonograph": true
        },
        {
            "id": "hiragana_12",
            "japanese": "す",
            "romanization": "su",
            "isMonograph": true
        },
        {
            "id": "hiragana_13",
            "japanese": "せ",
            "romanization": "se",
            "isMonograph": true
        },
        {
            "id": "hiragana_14",
            "japanese": "そ",
            "romanization": "so",
            "isMonograph": true
        },
        {
            "id": "hiragana_15",
            "japanese": "た",
            "romanization": "ta",
            "isMonograph": true
        },
        {
            "id": "hiragana_16",
            "japanese": "ち",
            "romanization": "chi",
            "isMonograph": true
        },
        {
            "id": "hiragana_17",
            "japanese": "つ",
            "romanization": "tsu",
            "isMonograph": true
        },
        {
            "id": "hiragana_18",
            "japanese": "て",
            "romanization": "te",
            "isMonograph": true
        },
        {
            "id": "hiragana_19",
            "japanese": "と",
            "romanization": "to",
            "isMonograph": true
        },
        {
            "id": "hiragana_20",
            "japanese": "な",
            "romanization": "na",
            "isMonograph": true
        },
        {
            "id": "hiragana_21",
            "japanese": "に",
            "romanization": "ni",
            "isMonograph": true
        },
        {
            "id": "hiragana_22",
            "japanese": "ぬ",
            "romanization": "nu",
            "isMonograph": true
        },
        {
            "id": "hiragana_23",
            "japanese": "ね",
            "romanization": "ne",
            "isMonograph": true
        },
        {
            "id": "hiragana_24",
            "japanese": "の",
            "romanization": "no",
            "isMonograph": true
        },
        {
            "id": "hiragana_25",
            "japanese": "は",
            "romanization": "ha",
            "isMonograph": true
        },
        {
            "id": "hiragana_26",
            "japanese": "ひ",
            "romanization": "hi",
            "isMonograph": true
        },
        {
            "id": "hiragana_27",
            "japanese": "ふ",
            "romanization": "fu",
            "isMonograph": true
        },
        {
            "id": "hiragana_28",
            "japanese": "へ",
            "romanization": "he",
            "isMonograph": true
        },
        {
            "id": "hiragana_29",
            "japanese": "ほ",
            "romanization": "ho",
            "isMonograph": true
        },
        {
            "id": "hiragana_30",
            "japanese": "ま",
            "romanization": "ma",
            "isMonograph": true
        },
        {
            "id": "hiragana_31",
            "japanese": "み",
            "romanization": "mi",
            "isMonograph": true
        },
        {
            "id": "hiragana_32",
            "japanese": "む",
            "romanization": "mu",
            "isMonograph": true
        },
        {
            "id": "hiragana_33",
            "japanese": "め",
            "romanization": "me",
            "isMonograph": true
        },
        {
            "id": "hiragana_34",
            "japanese": "も",
            "romanization": "mo",
            "isMonograph": true
        },
        {
            "id": "hiragana_35",
            "japanese": "や",
            "romanization": "ya",
            "isMonograph": true
        },
        {
            "id": "hiragana_36",
            "japanese": "ゆ",
            "romanization": "yu",
            "isMonograph": true
        },
        {
            "id": "hiragana_37",
            "japanese": "よ",
            "romanization": "yo",
            "isMonograph": true
        },
        {
            "id": "hiragana_38",
            "japanese": "ら",
            "romanization": "ra",
            "isMonograph": true
        },
        {
            "id": "hiragana_39",
            "japanese": "り",
            "romanization": "ri",
            "isMonograph": true
        },
        {
            "id": "hiragana_40",
            "japanese": "る",
            "romanization": "ru",
            "isMonograph": true
        },
        {
            "id": "hiragana_41",
            "japanese": "れ",
            "romanization": "re",
            "isMonograph": true
        },
        {
            "id": "hiragana_42",
            "japanese": "ろ",
            "romanization": "ro",
            "isMonograph": true
        },
        {
            "id": "hiragana_43",
            "japanese": "わ",
            "romanization": "wa",
            "isMonograph": true
        },
        {
            "id": "hiragana_44",
            "japanese": "を",
            "romanization": "wo",
            "isMonograph": true
        },
        {
            "id": "hiragana_45",
            "japanese": "ん",
            "romanization": "n",
            "isMonograph": true
        },
        {
            "id": "hiragana_46",
            "japanese": "が",
            "romanization": "ga",
            "isMonograph": false
        },
        {
            "id": "hiragana_47",
            "japanese": "ぎ",
            "romanization": "gi",
            "isMonograph": false
        },
        {
            "id": "hiragana_48",
            "japanese": "ぐ",
            "romanization": "gu",
            "isMonograph": false
        },
        {
            "id": "hiragana_49",
            "japanese": "げ",
            "romanization": "ge",
            "isMonograph": false
        },
        {
            "id": "hiragana_50",
            "japanese": "ご",
            "romanization": "go",
            "isMonograph": false
        },
        {
            "id": "hiragana_51",
            "japanese": "ざ",
            "romanization": "za",
            "isMonograph": false
        },
        {
            "id": "hiragana_52",
            "japanese": "じ",
            "romanization": "ji",
            "isMonograph": false
        },
        {
            "id": "hiragana_53",
            "japanese": "ず",
            "romanization": "zu",
            "isMonograph": false
        },
        {
            "id": "hiragana_54",
            "japanese": "ぜ",
            "romanization": "ze",
            "isMonograph": false
        },
        {
            "id": "hiragana_55",
            "japanese": "ぞ",
            "romanization": "zo",
            "isMonograph": false
        },
        {
            "id": "hiragana_56",
            "japanese": "だ",
            "romanization": "da",
            "isMonograph": false
        },
        {
            "id": "hiragana_57",
            "japanese": "ぢ",
            "romanization": "ji",
            "isMonograph": false
        },
        {
            "id": "hiragana_58",
            "japanese": "づ",
            "romanization": "zu",
            "isMonograph": false
        },
        {
            "id": "hiragana_59",
            "japanese": "で",
            "romanization": "de",
            "isMonograph": false
        },
        {
            "id": "hiragana_60",
            "japanese": "ど",
            "romanization": "do",
            "isMonograph": false
        },
        {
            "id": "hiragana_61",
            "japanese": "ば",
            "romanization": "ba",
            "isMonograph": false
        },
        {
            "id": "hiragana_62",
            "japanese": "び",
            "romanization": "bi",
            "isMonograph": false
        },
        {
            "id": "hiragana_63",
            "japanese": "ぶ",
            "romanization": "bu",
            "isMonograph": false
        },
        {
            "id": "hiragana_64",
            "japanese": "べ",
            "romanization": "be",
            "isMonograph": false
        },
        {
            "id": "hiragana_65",
            "japanese": "ぼ",
            "romanization": "bo",
            "isMonograph": false
        },
        {
            "id": "hiragana_66",
            "japanese": "ぱ",
            "romanization": "pa",
            "isMonograph": false
        },
        {
            "id": "hiragana_67",
            "japanese": "ぴ",
            "romanization": "pi",
            "isMonograph": false
        },
        {
            "id": "hiragana_68",
            "japanese": "ぷ",
            "romanization": "pu",
            "isMonograph": false
        },
        {
            "id": "hiragana_69",
            "japanese": "ぺ",
            "romanization": "pe",
            "isMonograph": false
        },
        {
            "id": "hiragana_70",
            "japanese": "ぽ",
            "romanization": "po",
            "isMonograph": false
        },
        {
            "id": "hiragana_71",
            "japanese": "ゔ",
            "romanization": "vu",
            "isMonograph": false
        },
        {
            "id": "hiragana_72",
            "japanese": "きゃ",
            "romanization": "kya",
            "isMonograph": false
        },
        {
            "id": "hiragana_73",
            "japanese": "きゅ",
            "romanization": "kyu",
            "isMonograph": false
        },
        {
            "id": "hiragana_74",
            "japanese": "きょ",
            "romanization": "kyo",
            "isMonograph": false
        },
        {
            "id": "hiragana_75",
            "japanese": "しゃ",
            "romanization": "sha",
            "isMonograph": false
        },
        {
            "id": "hiragana_76",
            "japanese": "しゅ",
            "romanization": "shu",
            "isMonograph": false
        },
        {
            "id": "hiragana_77",
            "japanese": "しょ",
            "romanization": "sho",
            "isMonograph": false
        },
        {
            "id": "hiragana_78",
            "japanese": "ちゃ",
            "romanization": "cha",
            "isMonograph": false
        },
        {
            "id": "hiragana_79",
            "japanese": "ちゅ",
            "romanization": "chu",
            "isMonograph": false
        },
        {
            "id": "hiragana_80",
            "japanese": "ちょ",
            "romanization": "cho",
            "isMonograph": false
        },
        {
            "id": "hiragana_81",
            "japanese": "にゃ",
            "romanization": "nya",
            "isMonograph": false
        },
        {
            "id": "hiragana_82",
            "japanese": "にゅ",
            "romanization": "nyu",
            "isMonograph": false
        },
        {
            "id": "hiragana_83",
            "japanese": "にょ",
            "romanization": "nyo",
            "isMonograph": false
        },
        {
            "id": "hiragana_84",
            "japanese": "ひゃ",
            "romanization": "hya",
            "isMonograph": false
        },
        {
            "id": "hiragana_85",
            "japanese": "ひゅ",
            "romanization": "hyu",
            "isMonograph": false
        },
        {
            "id": "hiragana_86",
            "japanese": "ひょ",
            "romanization": "hyo",
            "isMonograph": false
        },
        {
            "id": "hiragana_87",
            "japanese": "みゃ",
            "romanization": "mya",
            "isMonograph": false
        },
        {
            "id": "hiragana_88",
            "japanese": "みゅ",
            "romanization": "myu",
            "isMonograph": false
        },
        {
            "id": "hiragana_89",
            "japanese": "みょ",
            "romanization": "myo",
            "isMonograph": false
        },
        {
            "id": "hiragana_90",
            "japanese": "りゃ",
            "romanization": "rya",
            "isMonograph": false
        },
        {
            "id": "hiragana_91",
            "japanese": "りゅ",
            "romanization": "ryu",
            "isMonograph": false
        },
        {
            "id": "hiragana_92",
            "japanese": "りょ",
            "romanization": "ryo",
            "isMonograph": false
        },
        {
            "id": "hiragana_93",
            "japanese": "ぎゃ",
            "romanization": "gya",
            "isMonograph": false
        },
        {
            "id": "hiragana_94",
            "japanese": "ぎゅ",
            "romanization": "gyu",
            "isMonograph": false
        },
        {
            "id": "hiragana_95",
            "japanese": "ぎょ",
            "romanization": "gyo",
            "isMonograph": false
        },
        {
            "id": "hiragana_96",
            "japanese": "じゃ",
            "romanization": "ja",
            "isMonograph": false
        },
        {
            "id": "hiragana_97",
            "japanese": "じゅ",
            "romanization": "ju",
            "isMonograph": false
        },
        {
            "id": "hiragana_98",
            "japanese": "じょ",
            "romanization": "jo",
            "isMonograph": false
        },
        {
            "id": "hiragana_99",
            "japanese": "ぢゃ",
            "romanization": "ja",
            "isMonograph": false
        },
        {
            "id": "hiragana_100",
            "japanese": "ぢゅ",
            "romanization": "ju",
            "isMonograph": false
        },
        {
            "id": "hiragana_101",
            "japanese": "ぢょ",
            "romanization": "jo",
            "isMonograph": false
        },
        {
            "id": "hiragana_102",
            "japanese": "びゃ",
            "romanization": "bya",
            "isMonograph": false
        },
        {
            "id": "hiragana_103",
            "japanese": "びゅ",
            "romanization": "byu",
            "isMonograph": false
        },
        {
            "id": "hiragana_104",
            "japanese": "びょ",
            "romanization": "byo",
            "isMonograph": false
        },
        {
            "id": "hiragana_105",
            "japanese": "ぴゃ",
            "romanization": "pya",
            "isMonograph": false
        },
        {
            "id": "hiragana_106",
            "japanese": "ぴゅ",
            "romanization": "pyu",
            "isMonograph": false
        },
        {
            "id": "hiragana_107",
            "japanese": "ぴょ",
            "romanization": "pyo",
            "isMonograph": false
        }
    ];
}