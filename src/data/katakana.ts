import { Entry } from "../interface/entry";

export function getKatakanaList(): Array<Entry> {
    return [
        {
            "id": "katakana_0",
            "japanese": "ア",
            "romanization": "a",
            "isMonograph": true
        },
        {
            "id": "katakana_1",
            "japanese": "イ",
            "romanization": "i",
            "isMonograph": true
        },
        {
            "id": "katakana_2",
            "japanese": "ウ",
            "romanization": "u",
            "isMonograph": true
        },
        {
            "id": "katakana_3",
            "japanese": "エ",
            "romanization": "e",
            "isMonograph": true
        },
        {
            "id": "katakana_4",
            "japanese": "オ",
            "romanization": "o",
            "isMonograph": true
        },
        {
            "id": "katakana_5",
            "japanese": "カ",
            "romanization": "ka",
            "isMonograph": true
        },
        {
            "id": "katakana_6",
            "japanese": "キ",
            "romanization": "ki",
            "isMonograph": true
        },
        {
            "id": "katakana_7",
            "japanese": "ク",
            "romanization": "ku",
            "isMonograph": true
        },
        {
            "id": "katakana_8",
            "japanese": "ケ",
            "romanization": "ke",
            "isMonograph": true
        },
        {
            "id": "katakana_9",
            "japanese": "コ",
            "romanization": "ko",
            "isMonograph": true
        },
        {
            "id": "katakana_10",
            "japanese": "サ",
            "romanization": "sa",
            "isMonograph": true
        },
        {
            "id": "katakana_11",
            "japanese": "シ",
            "romanization": "shi",
            "isMonograph": true
        },
        {
            "id": "katakana_12",
            "japanese": "ス",
            "romanization": "su",
            "isMonograph": true
        },
        {
            "id": "katakana_13",
            "japanese": "セ",
            "romanization": "se",
            "isMonograph": true
        },
        {
            "id": "katakana_14",
            "japanese": "ソ",
            "romanization": "so",
            "isMonograph": true
        },
        {
            "id": "katakana_15",
            "japanese": "タ",
            "romanization": "ta",
            "isMonograph": true
        },
        {
            "id": "katakana_16",
            "japanese": "チ",
            "romanization": "chi",
            "isMonograph": true
        },
        {
            "id": "katakana_17",
            "japanese": "ツ",
            "romanization": "tsu",
            "isMonograph": true
        },
        {
            "id": "katakana_18",
            "japanese": "テ",
            "romanization": "te",
            "isMonograph": true
        },
        {
            "id": "katakana_19",
            "japanese": "ト",
            "romanization": "to",
            "isMonograph": true
        },
        {
            "id": "katakana_20",
            "japanese": "ナ",
            "romanization": "na",
            "isMonograph": true
        },
        {
            "id": "katakana_21",
            "japanese": "ニ",
            "romanization": "ni",
            "isMonograph": true
        },
        {
            "id": "katakana_22",
            "japanese": "ヌ",
            "romanization": "nu",
            "isMonograph": true
        },
        {
            "id": "katakana_23",
            "japanese": "ネ",
            "romanization": "ne",
            "isMonograph": true
        },
        {
            "id": "katakana_24",
            "japanese": "ノ",
            "romanization": "no",
            "isMonograph": true
        },
        {
            "id": "katakana_25",
            "japanese": "ハ",
            "romanization": "ha",
            "isMonograph": true
        },
        {
            "id": "katakana_26",
            "japanese": "ヒ",
            "romanization": "hi",
            "isMonograph": true
        },
        {
            "id": "katakana_27",
            "japanese": "フ",
            "romanization": "fu",
            "isMonograph": true
        },
        {
            "id": "katakana_28",
            "japanese": "ヘ",
            "romanization": "he",
            "isMonograph": true
        },
        {
            "id": "katakana_29",
            "japanese": "ホ",
            "romanization": "ho",
            "isMonograph": true
        },
        {
            "id": "katakana_30",
            "japanese": "マ",
            "romanization": "ma",
            "isMonograph": true
        },
        {
            "id": "katakana_31",
            "japanese": "ミ",
            "romanization": "mi",
            "isMonograph": true
        },
        {
            "id": "katakana_32",
            "japanese": "ム",
            "romanization": "mu",
            "isMonograph": true
        },
        {
            "id": "katakana_33",
            "japanese": "メ",
            "romanization": "me",
            "isMonograph": true
        },
        {
            "id": "katakana_34",
            "japanese": "モ",
            "romanization": "mo",
            "isMonograph": true
        },
        {
            "id": "katakana_35",
            "japanese": "ヤ",
            "romanization": "ya",
            "isMonograph": true
        },
        {
            "id": "katakana_36",
            "japanese": "ユ",
            "romanization": "yu",
            "isMonograph": true
        },
        {
            "id": "katakana_37",
            "japanese": "ヨ",
            "romanization": "yo",
            "isMonograph": true
        },
        {
            "id": "katakana_38",
            "japanese": "ラ",
            "romanization": "ra",
            "isMonograph": true
        },
        {
            "id": "katakana_39",
            "japanese": "リ",
            "romanization": "ri",
            "isMonograph": true
        },
        {
            "id": "katakana_40",
            "japanese": "ル",
            "romanization": "ru",
            "isMonograph": true
        },
        {
            "id": "katakana_41",
            "japanese": "レ",
            "romanization": "re",
            "isMonograph": true
        },
        {
            "id": "katakana_42",
            "japanese": "ロ",
            "romanization": "ro",
            "isMonograph": true
        },
        {
            "id": "katakana_43",
            "japanese": "ワ",
            "romanization": "wa",
            "isMonograph": true
        },
        {
            "id": "katakana_44",
            "japanese": "ヲ",
            "romanization": "wo",
            "isMonograph": true
        },
        {
            "id": "katakana_45",
            "japanese": "ン",
            "romanization": "n",
            "isMonograph": true
        },
        {
            "id": "katakana_46",
            "japanese": "ガ",
            "romanization": "ga",
            "isMonograph": false
        },
        {
            "id": "katakana_47",
            "japanese": "ギ",
            "romanization": "gi",
            "isMonograph": false
        },
        {
            "id": "katakana_48",
            "japanese": "グ",
            "romanization": "gu",
            "isMonograph": false
        },
        {
            "id": "katakana_49",
            "japanese": "ゲ",
            "romanization": "ge",
            "isMonograph": false
        },
        {
            "id": "katakana_50",
            "japanese": "ゴ",
            "romanization": "go",
            "isMonograph": false
        },
        {
            "id": "katakana_51",
            "japanese": "ザ",
            "romanization": "za",
            "isMonograph": false
        },
        {
            "id": "katakana_52",
            "japanese": "ジ",
            "romanization": "ji",
            "isMonograph": false
        },
        {
            "id": "katakana_53",
            "japanese": "ズ",
            "romanization": "zu",
            "isMonograph": false
        },
        {
            "id": "katakana_54",
            "japanese": "ゼ",
            "romanization": "ze",
            "isMonograph": false
        },
        {
            "id": "katakana_55",
            "japanese": "ゾ",
            "romanization": "zo",
            "isMonograph": false
        },
        {
            "id": "katakana_56",
            "japanese": "ダ",
            "romanization": "da",
            "isMonograph": false
        },
        {
            "id": "katakana_57",
            "japanese": "ヂ",
            "romanization": "ji",
            "isMonograph": false
        },
        {
            "id": "katakana_58",
            "japanese": "ヅ",
            "romanization": "zu",
            "isMonograph": false
        },
        {
            "id": "katakana_59",
            "japanese": "デ",
            "romanization": "de",
            "isMonograph": false
        },
        {
            "id": "katakana_60",
            "japanese": "ド",
            "romanization": "do",
            "isMonograph": false
        },
        {
            "id": "katakana_61",
            "japanese": "バ",
            "romanization": "ba",
            "isMonograph": false
        },
        {
            "id": "katakana_62",
            "japanese": "ビ",
            "romanization": "bi",
            "isMonograph": false
        },
        {
            "id": "katakana_63",
            "japanese": "ブ",
            "romanization": "bu",
            "isMonograph": false
        },
        {
            "id": "katakana_64",
            "japanese": "ベ",
            "romanization": "be",
            "isMonograph": false
        },
        {
            "id": "katakana_65",
            "japanese": "ボ",
            "romanization": "bo",
            "isMonograph": false
        },
        {
            "id": "katakana_66",
            "japanese": "パ",
            "romanization": "pa",
            "isMonograph": false
        },
        {
            "id": "katakana_67",
            "japanese": "ピ",
            "romanization": "pi",
            "isMonograph": false
        },
        {
            "id": "katakana_68",
            "japanese": "プ",
            "romanization": "pu",
            "isMonograph": false
        },
        {
            "id": "katakana_69",
            "japanese": "ペ",
            "romanization": "pe",
            "isMonograph": false
        },
        {
            "id": "katakana_70",
            "japanese": "ポ",
            "romanization": "po",
            "isMonograph": false
        },
        {
            "id": "katakana_71",
            "japanese": "ヴ",
            "romanization": "vu",
            "isMonograph": false
        },
        {
            "id": "katakana_72",
            "japanese": "キャ",
            "romanization": "kya",
            "isMonograph": false
        },
        {
            "id": "katakana_73",
            "japanese": "キュ",
            "romanization": "kyu",
            "isMonograph": false
        },
        {
            "id": "katakana_74",
            "japanese": "キョ",
            "romanization": "kyo",
            "isMonograph": false
        },
        {
            "id": "katakana_75",
            "japanese": "シャ",
            "romanization": "sha",
            "isMonograph": false
        },
        {
            "id": "katakana_76",
            "japanese": "シュ",
            "romanization": "shu",
            "isMonograph": false
        },
        {
            "id": "katakana_77",
            "japanese": "ショ",
            "romanization": "sho",
            "isMonograph": false
        },
        {
            "id": "katakana_78",
            "japanese": "チャ",
            "romanization": "cha",
            "isMonograph": false
        },
        {
            "id": "katakana_79",
            "japanese": "チュ",
            "romanization": "chu",
            "isMonograph": false
        },
        {
            "id": "katakana_80",
            "japanese": "チョ",
            "romanization": "cho",
            "isMonograph": false
        },
        {
            "id": "katakana_81",
            "japanese": "ニャ",
            "romanization": "nya",
            "isMonograph": false
        },
        {
            "id": "katakana_82",
            "japanese": "ニュ",
            "romanization": "nyu",
            "isMonograph": false
        },
        {
            "id": "katakana_83",
            "japanese": "ニョ",
            "romanization": "nyo",
            "isMonograph": false
        },
        {
            "id": "katakana_84",
            "japanese": "ヒャ",
            "romanization": "hya",
            "isMonograph": false
        },
        {
            "id": "katakana_85",
            "japanese": "ヒュ",
            "romanization": "hyu",
            "isMonograph": false
        },
        {
            "id": "katakana_86",
            "japanese": "ヒョ",
            "romanization": "hyo",
            "isMonograph": false
        },
        {
            "id": "katakana_87",
            "japanese": "ミャ",
            "romanization": "mya",
            "isMonograph": false
        },
        {
            "id": "katakana_88",
            "japanese": "ミュ",
            "romanization": "myu",
            "isMonograph": false
        },
        {
            "id": "katakana_89",
            "japanese": "ミョ",
            "romanization": "myo",
            "isMonograph": false
        },
        {
            "id": "katakana_90",
            "japanese": "リャ",
            "romanization": "rya",
            "isMonograph": false
        },
        {
            "id": "katakana_91",
            "japanese": "リュ",
            "romanization": "ryu",
            "isMonograph": false
        },
        {
            "id": "katakana_92",
            "japanese": "リョ",
            "romanization": "ryo",
            "isMonograph": false
        },
        {
            "id": "katakana_93",
            "japanese": "ギャ",
            "romanization": "gya",
            "isMonograph": false
        },
        {
            "id": "katakana_94",
            "japanese": "ギュ",
            "romanization": "gyu",
            "isMonograph": false
        },
        {
            "id": "katakana_95",
            "japanese": "ギョ",
            "romanization": "gyo",
            "isMonograph": false
        },
        {
            "id": "katakana_96",
            "japanese": "ジャ",
            "romanization": "ja",
            "isMonograph": false
        },
        {
            "id": "katakana_97",
            "japanese": "ジュ",
            "romanization": "ju",
            "isMonograph": false
        },
        {
            "id": "katakana_98",
            "japanese": "ジョ",
            "romanization": "jo",
            "isMonograph": false
        },
        {
            "id": "katakana_99",
            "japanese": "ヂャ",
            "romanization": "ja",
            "isMonograph": false
        },
        {
            "id": "katakana_100",
            "japanese": "ヂュ",
            "romanization": "ju",
            "isMonograph": false
        },
        {
            "id": "katakana_101",
            "japanese": "ヂョ",
            "romanization": "jo",
            "isMonograph": false
        },
        {
            "id": "katakana_102",
            "japanese": "ビャ",
            "romanization": "bya",
            "isMonograph": false
        },
        {
            "id": "katakana_103",
            "japanese": "ビュ",
            "romanization": "byu",
            "isMonograph": false
        },
        {
            "id": "katakana_104",
            "japanese": "ビョ",
            "romanization": "byo",
            "isMonograph": false
        },
        {
            "id": "katakana_105",
            "japanese": "ピャ",
            "romanization": "pya",
            "isMonograph": false
        },
        {
            "id": "katakana_106",
            "japanese": "ピュ",
            "romanization": "pyu",
            "isMonograph": false
        },
        {
            "id": "katakana_107",
            "japanese": "ピョ",
            "romanization": "pyo",
            "isMonograph": false
        }
    ];
}