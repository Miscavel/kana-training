// Raw N5 vocabs
export function getN5Vocabulary() {
    return [
        [
            "1",
            "浴びる",
            "abiru (あびる)",
            "Verb, Ichidan verb, Transitive verb",
            "to bathe, to shower"
        ],
        [
            "2",
            "危ない",
            "abunai (あぶない)",
            "Adjective, い-adjective",
            "dangerous"
        ],
        [
            "3",
            "あっち",
            "acchi",
            "Pronoun",
            "over there"
        ],
        [
            "4",
            "あちら",
            "achira",
            "Pronoun",
            "there"
        ],
        [
            "5",
            "上げる",
            "ageru (あげる)",
            "Verb, Ichidan verb, Transitive verb",
            "to raise; to elevate"
        ],
        [
            "6",
            "赤",
            "aka (あか)",
            "Noun",
            "red; crimson; scarlet​"
        ],
        [
            "7",
            "赤い",
            "akai (あかい)",
            "Adjective, い-adjective",
            "red; crimson; scarlet​"
        ],
        [
            "8",
            "明るい",
            "akarui (あかるい)",
            "Adjective, い-adjective",
            "bright; light"
        ],
        [
            "9",
            "開ける",
            "akeru (あける)",
            "Verb, Ichidan verb, Transitive verb",
            "to open (a door, etc.); to unwrap (e.g. parcel, package); to unlock"
        ],
        [
            "10",
            "秋",
            "aki (あき)",
            "Noun",
            "autumn; fall"
        ],
        [
            "11",
            "開く",
            "aku (あく)",
            "Verb, Godan verb, Intransitive verb",
            "to open (e.g. doors, business, etc)"
        ],
        [
            "12",
            "甘い",
            "amai (あまい)",
            "Adjective, い-adjective",
            "sweet; sweet-tasting; sugary; naive; indulgent"
        ],
        [
            "13",
            "飴",
            "ame (あめ)",
            "Noun",
            "candy"
        ],
        [
            "14",
            "雨",
            "ame (あめ)",
            "Noun",
            "rain"
        ],
        [
            "15",
            "あなた",
            "anata",
            "Pronoun",
            "you"
        ],
        [
            "16",
            "姉",
            "ane (あね)",
            "Noun",
            "older sister; elder sister​"
        ],
        [
            "17",
            "兄",
            "ani (あに)",
            "Noun",
            "elder brother; older brother​"
        ],
        [
            "18",
            "あの",
            "ano",
            "Pre-noun adjectival",
            "that"
        ],
        [
            "19",
            "青",
            "ao (あお)",
            "Noun",
            "blue; azure"
        ],
        [
            "20",
            "青い",
            "aoi (あおい)",
            "Adjective, い-adjective",
            "blue; azure"
        ],
        [
            "21",
            "アパート",
            "apaato",
            "Noun, Katakana",
            "apartment"
        ],
        [
            "22",
            "洗う",
            "arau (あらう)",
            "Verb, Godan verb, Transitive verb",
            "to wash"
        ],
        [
            "23",
            "あれ",
            "are",
            "Pronoun",
            "that"
        ],
        [
            "24",
            "ある",
            "aru",
            "Verb, Godan verb, Intransitive verb",
            "to be, to have"
        ],
        [
            "25",
            "歩く",
            "aruku (あるく)",
            "Verb, Godan verb, Intransitive verb",
            "to walk"
        ],
        [
            "26",
            "朝",
            "asa (あさ)",
            "Noun",
            "morning"
        ],
        [
            "27",
            "朝ご飯",
            "asagohan (あさごはん)",
            "Noun",
            "breakfast"
        ],
        [
            "28",
            "明後日",
            "asatte (あさって)",
            "Noun",
            "day after tomorrow"
        ],
        [
            "29",
            "足",
            "ashi (あし)",
            "Noun",
            "foot; leg; paw; arm"
        ],
        [
            "30",
            "明日",
            "ashita (あした)",
            "Noun, Temporal noun",
            "tomorrow"
        ],
        [
            "31",
            "遊ぶ",
            "asobu (あそぶ)",
            "Verb, Godan verb, Intransitive verb",
            "to play; to enjoy oneself"
        ],
        [
            "32",
            "あそこ",
            "asoko",
            "Pronoun",
            "over there"
        ],
        [
            "33",
            "頭",
            "atama (あたま)",
            "Noun",
            "head"
        ],
        [
            "34",
            "新しい",
            "atarashii (あたらしい)",
            "Adjective, い-adjective",
            "new; novel; fresh; recent; latest"
        ],
        [
            "35",
            "暖かい",
            "atatakai (あたたかい)",
            "Adjective, い-adjective",
            "warm"
        ],
        [
            "36",
            "後",
            "ato (あと)",
            "Noun",
            "behind; after; remainder; left; also"
        ],
        [
            "37",
            "厚い",
            "atsui (あつい)",
            "Adjective, い-adjective",
            "thick"
        ],
        [
            "38",
            "暑い",
            "atsui (あつい)",
            "Adjective, い-adjective",
            "hot; sultry"
        ],
        [
            "39",
            "熱い",
            "atsui (あつい)",
            "Adjective, い-adjective",
            "hot"
        ],
        [
            "40",
            "会う",
            "au (あう)",
            "Verb, Godan verb, Intransitive verb",
            "to meet; to encounter; to see"
        ],
        [
            "41",
            "晩ご飯",
            "bangohan (ばんごはん)",
            "Noun",
            "dinner; evening meal"
        ],
        [
            "42",
            "番号",
            "bangou (ばんごう)",
            "Noun",
            "number"
        ],
        [
            "43",
            "バス",
            "basu",
            "Noun, Katakana",
            "bus"
        ],
        [
            "44",
            "バター",
            "bataa",
            "Noun, Katakana",
            "butter​"
        ],
        [
            "45",
            "ベッド",
            "beddo",
            "Noun, Katakana",
            "bed"
        ],
        [
            "46",
            "勉強",
            "benkyou (べんきょう)",
            "Noun, Suru verb",
            "to study"
        ],
        [
            "47",
            "便利",
            "benri (べんり)",
            "な-adjective",
            "convenient; handy; useful"
        ],
        [
            "48",
            "ボールペン",
            "boorupen",
            "Noun, Katakana",
            "ball-point pen"
        ],
        [
            "49",
            "ボタン",
            "botan",
            "Noun, Katakana",
            "button"
        ],
        [
            "50",
            "帽子",
            "boushi (ぼうし)",
            "Noun",
            "hat; cap"
        ],
        [
            "51",
            "文章",
            "bunshou (ぶんしょう)",
            "Noun",
            "sentence"
        ],
        [
            "52",
            "豚肉",
            "butaniku (ぶたにく)",
            "Noun",
            "pork"
        ],
        [
            "53",
            "病院",
            "byouin (びょういん)",
            "Noun",
            "hospital"
        ],
        [
            "54",
            "病気",
            "byouki (びょうき)",
            "Noun",
            "illness; disease; sickness"
        ],
        [
            "55",
            "茶色",
            "chairo (ちゃいろ)",
            "Noun",
            "brown"
        ],
        [
            "56",
            "茶碗",
            "chawan (ちゃわん)",
            "Noun",
            "rice bowl; tea cup; teacup"
        ],
        [
            "57",
            "父",
            "chichi (ちち)",
            "Noun",
            "father"
        ],
        [
            "58",
            "違う",
            "chigau (ちがう)",
            "Verb, Godan verb, Intransitive verb",
            "to differ"
        ],
        [
            "59",
            "小さい",
            "chiisai (ちいさい)",
            "Adjective, い-adjective",
            "small; little; tiny"
        ],
        [
            "60",
            "小さな",
            "chiisana (ちいさな)",
            "Pre-noun adjectival",
            "small; little; tiny​"
        ],
        [
            "61",
            "近い",
            "chikai (ちかい)",
            "い-adjective",
            "near; close"
        ],
        [
            "62",
            "地下鉄",
            "chikatetsu (ちかてつ)",
            "Noun",
            "subway; underground train"
        ],
        [
            "63",
            "地図",
            "chizu (ちず)",
            "Noun",
            "map"
        ],
        [
            "64",
            "ちょっと",
            "chotto",
            "Adverb",
            "a little"
        ],
        [
            "65",
            "丁度",
            "choudo (ちょうど)",
            "Adverb",
            "exactly"
        ],
        [
            "66",
            "台所",
            "daidokoro (だいどころ)",
            "Noun",
            "kitchen"
        ],
        [
            "67",
            "大学",
            "daigaku (だいがく)",
            "Noun",
            "university; college"
        ],
        [
            "68",
            "大丈夫",
            "daijoubu (だいじょうぶ)",
            "Adjective, な-adjective, Adverb",
            "OK; okay; alright; problem free"
        ],
        [
            "69",
            "大好き",
            "daisuki (だいすき)",
            "Adjective, な-adjective",
            "love; like; like very much"
        ],
        [
            "70",
            "だんだん",
            "dandan",
            "Adverb",
            "gradually"
        ],
        [
            "71",
            "誰",
            "dare (だれ)",
            "Pronoun",
            "who"
        ],
        [
            "72",
            "誰か",
            "dareka (だれか)",
            "Pronoun",
            "someone; somebody"
        ],
        [
            "73",
            "出す",
            "dasu (だす)",
            "Verb, Godan verb, Transitive verb",
            "to take out; to get out; to put out; to reveal"
        ],
        [
            "74",
            "出口",
            "deguchi (でぐち)",
            "Noun",
            "exit; gateway; way out"
        ],
        [
            "75",
            "出かける",
            "dekakeru (でかける)",
            "Verb, Ichidan verb, Intransitive verb",
            "to go out; to leave; to depart"
        ],
        [
            "76",
            "電気",
            "denki (でんき)",
            "Noun",
            "electricity"
        ],
        [
            "77",
            "電車",
            "densha (でんしゃ)",
            "Noun",
            "train; electric train"
        ],
        [
            "78",
            "電話",
            "denwa (でんわ)",
            "Noun, Verb, Suru verb",
            "telephone (call / device)l; phone call"
        ],
        [
            "79",
            "デパート",
            "depaato",
            "Noun, Katakana",
            "department store"
        ],
        [
            "80",
            "出る",
            "deru (でる)",
            "Verb, Ichidan verb, Intransitive verb",
            "to leave; to exit; to appear; to go out"
        ],
        [
            "81",
            "ドア",
            "doa",
            "Noun, Katakana",
            "door"
        ],
        [
            "82",
            "どっち",
            "docchi",
            "Pronoun",
            "which; which one"
        ],
        [
            "83",
            "どちら",
            "dochira",
            "Pronoun",
            "which of two"
        ],
        [
            "84",
            "どこ",
            "doko",
            "Pronoun",
            "where; what place​"
        ],
        [
            "85",
            "どなた",
            "donata",
            "Noun",
            "who"
        ],
        [
            "86",
            "どの",
            "dono",
            "Pre-noun adjectival",
            "which"
        ],
        [
            "87",
            "どれ",
            "dore",
            "Pronoun",
            "which (of three or more)​"
        ],
        [
            "88",
            "どう",
            "dou",
            "Adverb",
            "how; in what way; how about​"
        ],
        [
            "89",
            "動物",
            "doubutsu (どうぶつ)",
            "Noun",
            "animal"
        ],
        [
            "90",
            "どうも",
            "doumo",
            "Adverb",
            "thank you; thanks"
        ],
        [
            "91",
            "どうぞ",
            "douzo",
            "Adverb",
            "please"
        ],
        [
            "92",
            "土曜日",
            "doyoubi (どようび)",
            "Noun",
            "Saturday"
        ],
        [
            "93",
            "絵",
            "e (え)",
            "Noun",
            "picture"
        ],
        [
            "94",
            "ええ",
            "ee",
            "Noun",
            "yes; that is correct; right"
        ],
        [
            "95",
            "映画",
            "eiga (えいが)",
            "Noun",
            "movie; film"
        ],
        [
            "96",
            "映画館",
            "eigakan (えいがかん)",
            "Noun",
            "movie theater; cinema"
        ],
        [
            "97",
            "英語",
            "eigo (えいご)",
            "Noun",
            "English language"
        ],
        [
            "98",
            "駅",
            "eki (えき)",
            "Noun",
            "station"
        ],
        [
            "99",
            "鉛筆",
            "enpitsu (えんぴつ)",
            "Noun",
            "pencil"
        ],
        [
            "100",
            "エレベーター",
            "erebeetaa",
            "Noun, Katakana",
            "elevator"
        ],
        [
            "101",
            "フィルム",
            "firumu",
            "Noun, Katakana",
            "film"
        ],
        [
            "102",
            "フォーク",
            "fooku",
            "Noun, Katakana",
            "fork"
        ],
        [
            "103",
            "吹く",
            "fuku (ふく)",
            "Verb, Godan verb, Intransitive verb",
            "to blow (of the wind)"
        ],
        [
            "104",
            "服",
            "fuku (ふく)",
            "Noun",
            "clothes"
        ],
        [
            "105",
            "降る",
            "furu (ふる)",
            "Verb, Godan verb, Intransitive verb",
            "to fall"
        ],
        [
            "106",
            "古い",
            "furui (ふるい)",
            "い-adjective",
            "old (not used for people)"
        ],
        [
            "107",
            "二人",
            "futari (ふたり)",
            "Noun",
            "two people; pair; couple"
        ],
        [
            "108",
            "二つ",
            "futatsu (ふたつ)",
            "Noun, Numeric",
            "two; 2"
        ],
        [
            "109",
            "太い",
            "futoi (ふとい)",
            "Adjective, い-adjective",
            "fat; thick"
        ],
        [
            "110",
            "二日",
            "futsuka (ふつか)",
            "Noun",
            "the second day of the month / 2 days"
        ],
        [
            "111",
            "封筒",
            "fuutou (ふうとう)",
            "Noun",
            "envelope"
        ],
        [
            "112",
            "冬",
            "fuyu (ふゆ)",
            "Noun",
            "winter"
        ],
        [
            "113",
            "外国",
            "gaikoku (がいこく)",
            "Noun",
            "foreign country"
        ],
        [
            "114",
            "外国人",
            "gaikokujin (がいこくじん)",
            "Noun",
            "foreigner; foreign citizen; foreign national; alien; non-Japanese"
        ],
        [
            "115",
            "学校",
            "gakkou (がっこう)",
            "Noun",
            "school"
        ],
        [
            "116",
            "学生",
            "gakusei (がくせい)",
            "Noun",
            "student"
        ],
        [
            "117",
            "玄関",
            "genkan (げんかん)",
            "Noun",
            "entrance"
        ],
        [
            "118",
            "元気",
            "genki (げんき)",
            "Noun, Adjective, な-adjective",
            "lively; full of spirit; energetic; healthy"
        ],
        [
            "119",
            "月曜日",
            "getsuyoubi (げつようび)",
            "Noun",
            "Monday"
        ],
        [
            "120",
            "銀行",
            "ginkou (ぎんこう)",
            "Noun",
            "bank"
        ],
        [
            "121",
            "ギター",
            "gitaa",
            "Noun, Katakana",
            "guitar"
        ],
        [
            "122",
            "五",
            "go (ご)",
            "Noun, Numeric",
            "five; 5"
        ],
        [
            "123",
            "午後",
            "gogo (ごご)",
            "Noun",
            "afternoon; p.m."
        ],
        [
            "124",
            "ご飯",
            "gohan (ごはん)",
            "Noun",
            "cooked rice, meal"
        ],
        [
            "125",
            "午前",
            "gozen (ごぜん)",
            "Noun",
            "morning; a.m."
        ],
        [
            "126",
            "グラム",
            "guramu",
            "Noun, Katakana",
            "gram"
        ],
        [
            "127",
            "牛肉",
            "gyuuniku (ぎゅうにく)",
            "Noun",
            "beef"
        ],
        [
            "128",
            "牛乳",
            "gyuunyuu (ぎゅうにゅう)",
            "Noun",
            "(cow's) milk"
        ],
        [
            "129",
            "歯",
            "ha (は)",
            "Noun",
            "tooth"
        ],
        [
            "130",
            "八",
            "hachi (はち)",
            "Noun, Numeric",
            "eight: 8"
        ],
        [
            "131",
            "葉書",
            "hagaki (はがき)",
            "Noun",
            "postcard"
        ],
        [
            "132",
            "母",
            "haha (はは)",
            "Noun",
            "mother"
        ],
        [
            "133",
            "はい",
            "hai",
            "Noun",
            "yes; that is correct​"
        ],
        [
            "134",
            "入る",
            "hairu (はいる)",
            "Verb, Godan verb, Intransitive verb",
            "to enter; to go into"
        ],
        [
            "135",
            "灰皿",
            "haizara (はいざら)",
            "Noun",
            "ashtray"
        ],
        [
            "136",
            "始まる",
            "hajimaru (はじまる)",
            "Verb, Godan verb, Intransitive verb",
            "to begin"
        ],
        [
            "137",
            "初めて",
            "hajimete (はじめて)",
            "Adverb",
            "for the first time"
        ],
        [
            "138",
            "箱",
            "hako (はこ)",
            "Noun",
            "box; crate"
        ],
        [
            "139",
            "履く",
            "haku (はく)",
            "Verb, Godan verb, Transitive verb",
            "to wear, to put on trousers"
        ],
        [
            "140",
            "半",
            "han (はん)",
            "Noun",
            "half; semi-; half-past"
        ],
        [
            "141",
            "花",
            "hana (はな)",
            "Noun",
            "flower"
        ],
        [
            "142",
            "鼻",
            "hana (はな)",
            "Noun",
            "nose"
        ],
        [
            "143",
            "話",
            "hanashi (はなし)",
            "Noun",
            "talk; speech; chat; conversation​"
        ],
        [
            "144",
            "話す",
            "hanasu (はなす)",
            "Verb, Godan verb, Transitive verb",
            "to speak; to talk; to converse"
        ],
        [
            "145",
            "半分",
            "hanbun (はんぶん)",
            "Noun",
            "half"
        ],
        [
            "146",
            "ハンカチ",
            "hankachi",
            "Noun, Katakana",
            "handkerchief​"
        ],
        [
            "147",
            "晴れ",
            "hare (はれ)",
            "Noun",
            "clear weather"
        ],
        [
            "148",
            "晴れる",
            "hareru (はれる)",
            "Verb, Ichidan verb, Intransitive verb",
            "to be sunny"
        ],
        [
            "149",
            "貼る",
            "haru (はる)",
            "Verb, Godan verb, Transitive verb",
            "to stick; to paste"
        ],
        [
            "150",
            "春",
            "haru (はる)",
            "Noun",
            "spring; springtime"
        ],
        [
            "151",
            "箸",
            "hashi (はし)",
            "Noun",
            "chopsticks"
        ],
        [
            "152",
            "橋",
            "hashi (はし)",
            "Noun",
            "bridge"
        ],
        [
            "153",
            "走る",
            "hashiru (はしる)",
            "Verb, Godan verb, Intransitive verb",
            "to run"
        ],
        [
            "154",
            "二十歳",
            "hatachi (はたち)",
            "Noun",
            "20 years old; twenty years old"
        ],
        [
            "155",
            "働く",
            "hataraku (はたらく)",
            "Verb, Godan verb, Intransitive verb",
            "to work"
        ],
        [
            "156",
            "二十日",
            "hatsuka (はつか)",
            "Noun",
            "twentieth day of the month / 20 days"
        ],
        [
            "157",
            "早い",
            "hayai (はやい)",
            "Adjective, い-adjective",
            "fast; early"
        ],
        [
            "158",
            "速い",
            "hayai (はやい)",
            "Adjective, い-adjective",
            "fast; quick; hasty; brisk"
        ],
        [
            "159",
            "辺",
            "hen (へん)",
            "Noun",
            "area"
        ],
        [
            "160",
            "下手",
            "heta (へた)",
            "Noun, Adjective, な-adjective",
            "unskillful; poor; awkward​"
        ],
        [
            "161",
            "部屋",
            "heya (へや)",
            "Noun",
            "room"
        ],
        [
            "162",
            "左",
            "hidari (ひだり)",
            "Noun",
            "left; left hand side"
        ],
        [
            "163",
            "東",
            "higashi (ひがし)",
            "Noun",
            "east"
        ],
        [
            "164",
            "飛行機",
            "hikouki (ひこうき)",
            "Noun",
            "airplane; aircraft"
        ],
        [
            "165",
            "引く",
            "hiku (ひく)",
            "Verb, Godan verb, Transitive verb",
            "to pull"
        ],
        [
            "166",
            "弾く",
            "hiku (ひく)",
            "Verb, Godan verb, Transitive verb",
            "to play"
        ],
        [
            "167",
            "低い",
            "hikui (ひくい)",
            "い-adjective",
            "short,low"
        ],
        [
            "168",
            "暇",
            "hima (ひま)",
            "Noun, な-adjective",
            "free time"
        ],
        [
            "169",
            "広い",
            "hiroi (ひろい)",
            "い-adjective",
            "spacious; vast; wide"
        ],
        [
            "170",
            "昼",
            "hiru (ひる)",
            "Noun",
            "noon; midday; daytime; lunch"
        ],
        [
            "171",
            "昼ご飯",
            "hirugohan (ひるごはん)",
            "Noun",
            "lunch"
        ],
        [
            "172",
            "人",
            "hito (ひと)",
            "Noun",
            "person; human"
        ],
        [
            "173",
            "一人",
            "hitori (ひとり)",
            "Noun, Adverb",
            "one person​; alone; single"
        ],
        [
            "174",
            "一つ",
            "hitotsu (ひとつ)",
            "Noun, Numeric",
            "one thing; only"
        ],
        [
            "175",
            "ほか",
            "hoka",
            "Noun",
            "other (place, thing, person); the rest"
        ],
        [
            "176",
            "本",
            "hon (ほん)",
            "Noun",
            "book; volume; script"
        ],
        [
            "177",
            "本棚",
            "hondana (ほんだな)",
            "Noun",
            "bookshelf; bookcase"
        ],
        [
            "178",
            "本当",
            "hontou (ほんとう)",
            "Noun, Adjective, な-adjective",
            "truth; reality; actuality; fact"
        ],
        [
            "179",
            "欲しい",
            "hoshii (ほしい)",
            "Adjective, い-adjective",
            "want"
        ],
        [
            "180",
            "細い",
            "hosoi (ほそい)",
            "Adjective, い-adjective",
            "thin; slender"
        ],
        [
            "181",
            "ホテル",
            "hoteru",
            "Noun, Katakana",
            "hotel"
        ],
        [
            "182",
            "百",
            "hyaku (ひゃく)",
            "Noun, Numeric",
            "100; hundred"
        ],
        [
            "183",
            "一",
            "ichi (いち)",
            "Noun, Prefix, Suffix, Numeric",
            "one; best; first; foremost; start"
        ],
        [
            "184",
            "一番",
            "ichiban (いちばん)",
            "Noun, Adverb",
            "number one; first; 1st, first place​; best; most​"
        ],
        [
            "185",
            "一日",
            "ichinichi (いちにち)",
            "Noun, Adverbial Noun",
            "one day, all day"
        ],
        [
            "186",
            "家",
            "ie (いえ)",
            "Noun",
            "house, residence, family"
        ],
        [
            "187",
            "いかが",
            "ikaga",
            "Adverb",
            "how; in what way; how about​"
        ],
        [
            "188",
            "池",
            "ike (いけ)",
            "Noun",
            "pond"
        ],
        [
            "189",
            "行く",
            "iku (いく)",
            "Verb, Godan verb, Intransitive verb",
            "to go; to move"
        ],
        [
            "190",
            "いくら",
            "ikura",
            "Noun, Adverb",
            "how much?; how many?​"
        ],
        [
            "191",
            "いくつ",
            "ikutsu",
            "Adverb",
            "how many?,how old?"
        ],
        [
            "192",
            "今",
            "ima (いま)",
            "Noun, Adverbial Noun",
            "now; the present time; soon"
        ],
        [
            "193",
            "意味",
            "imi (いみ)",
            "Noun",
            "meaning; significance; sense"
        ],
        [
            "194",
            "妹",
            "imouto (いもうと)",
            "Noun",
            "younger sister"
        ],
        [
            "195",
            "犬",
            "inu (いぬ)",
            "Noun",
            "dog"
        ],
        [
            "196",
            "入れる",
            "ireru (いれる)",
            "Verb, Ichidan verb, Transitive verb",
            "to put in; to let in; to take in; to bring in; to insert; to install"
        ],
        [
            "197",
            "入口",
            "iriguchi (いりぐち)",
            "Noun",
            "entrance; entry; gate"
        ],
        [
            "198",
            "色",
            "iro (いろ)",
            "Noun",
            "colour; color"
        ],
        [
            "199",
            "色々",
            "iroiro (いろいろ)",
            "Noun, Adjective, な-adjective, Adverb",
            "various"
        ],
        [
            "200",
            "居る",
            "iru (いる)",
            "Verb, Ichidan verb, Intransitive verb",
            "to be, to have"
        ],
        [
            "201",
            "要る",
            "iru (いる)",
            "Verb, Godan verb, Intransitive verb",
            "to be needed"
        ],
        [
            "202",
            "医者",
            "isha (いしゃ)",
            "Noun",
            "(medical) doctor; physician"
        ],
        [
            "203",
            "忙しい",
            "isogashii (いそがしい)",
            "Adjective, い-adjective",
            "busy"
        ],
        [
            "204",
            "一緒",
            "issho (いっしょ)",
            "Noun",
            "together; at the same time; same; identical"
        ],
        [
            "205",
            "椅子",
            "isu (いす)",
            "Noun",
            "chair"
        ],
        [
            "206",
            "痛い",
            "itai (いたい)",
            "Adjective, い-adjective",
            "painful; sore​"
        ],
        [
            "207",
            "いつ",
            "itsu",
            "Pronoun",
            "when"
        ],
        [
            "208",
            "五日",
            "itsuka (いつか)",
            "Noun",
            "the fifth day of the month / 5 days"
        ],
        [
            "209",
            "五つ",
            "itsutsu (いつつ)",
            "Noun, Numeric",
            "five; 5"
        ],
        [
            "210",
            "言う",
            "iu (いう)",
            "Verb, Godan verb",
            "to say; to call"
        ],
        [
            "211",
            "嫌",
            "iya (いや)",
            "Noun, Adjective, な-adjective",
            "unpleasant"
        ],
        [
            "212",
            "じゃあ",
            "jaa",
            "Conjunction",
            "then; well; so; well then"
        ],
        [
            "213",
            "字引",
            "jibiki (じびき)",
            "Noun",
            "dictionary"
        ],
        [
            "214",
            "自分",
            "jibun (じぶん)",
            "Pronoun",
            "myself; yourself; oneself; himself; herself; i; me"
        ],
        [
            "215",
            "自動車",
            "jidousha (じどうしゃ)",
            "Noun",
            "automobile; motorcar; motor vehicle; car"
        ],
        [
            "216",
            "時間",
            "jikan (じかん)",
            "Noun",
            "time; hour(s)"
        ],
        [
            "217",
            "辞書",
            "jisho (じしょ)",
            "Noun",
            "dictionary"
        ],
        [
            "218",
            "自転車",
            "jitensha (じてんしゃ)",
            "Noun",
            "bicycle"
        ],
        [
            "219",
            "丈夫",
            "joubu (じょうぶ)",
            "Adjective, な-adjective",
            "strong, durable"
        ],
        [
            "220",
            "上手",
            "jouzu (じょうず)",
            "Noun, Adjective, な-adjective",
            "skillful; skilled; proficient; good (at)"
        ],
        [
            "221",
            "授業",
            "jugyou (じゅぎょう)",
            "Noun, Suru verb",
            "lesson; class work"
        ],
        [
            "222",
            "十",
            "juu (じゅう)",
            "Noun, Numeric",
            "ten; 10"
        ],
        [
            "223",
            "かばん",
            "kaban",
            "Noun",
            "bag; basket​"
        ],
        [
            "224",
            "花瓶",
            "kabin (かびん)",
            "Noun",
            "a vase"
        ],
        [
            "225",
            "角",
            "kado (かど)",
            "Noun",
            "a corner; angle​"
        ],
        [
            "226",
            "帰る",
            "kaeru (かえる)",
            "Verb, Godan verb, Intransitive verb",
            "to go back​"
        ],
        [
            "227",
            "返す",
            "kaesu (かえす)",
            "Verb, Godan verb, Transitive verb",
            "to return something"
        ],
        [
            "228",
            "鍵",
            "kagi (かぎ)",
            "Noun",
            "key"
        ],
        [
            "229",
            "階段",
            "kaidan (かいだん)",
            "Noun",
            "stairs; stairway; staircase"
        ],
        [
            "230",
            "買い物",
            "kaimono (かいもの)",
            "Noun",
            "shopping; purchased goods"
        ],
        [
            "231",
            "会社",
            "kaisha (かいしゃ)",
            "Noun",
            "company; corporation"
        ],
        [
            "232",
            "掛かる",
            "kakaru (かかる)",
            "Verb, Godan verb, Intransitive verb",
            "to take (a resource, e.g. time or money)"
        ],
        [
            "233",
            "掛ける",
            "kakeru (かける)",
            "Verb, Ichidan verb, Transitive verb",
            "to hang up; to make (a call)​;"
        ],
        [
            "234",
            "書く",
            "kaku (かく)",
            "Verb, Godan verb, Transitive verb",
            "to write; to compose; to pen; to draw"
        ],
        [
            "235",
            "カメラ",
            "kamera",
            "Noun, Katakana",
            "camera"
        ],
        [
            "236",
            "紙",
            "kami (かみ)",
            "Noun",
            "paper"
        ],
        [
            "237",
            "漢字",
            "kanji (かんじ)",
            "Noun",
            "kanji"
        ],
        [
            "238",
            "カップ",
            "kappu",
            "Noun, Katakana",
            "cup"
        ],
        [
            "239",
            "体",
            "karada (からだ)",
            "Noun",
            "body"
        ],
        [
            "240",
            "辛い",
            "karai (からい)",
            "い-adjective",
            "spicy"
        ],
        [
            "241",
            "カレー",
            "karee",
            "Noun, Katakana",
            "curry"
        ],
        [
            "242",
            "カレンダー",
            "karendaa",
            "Noun, Katakana",
            "calendar"
        ],
        [
            "243",
            "借りる",
            "kariru (かりる)",
            "Verb, Ichidan verb, Transitive verb",
            "to borrow"
        ],
        [
            "244",
            "軽い",
            "karui (かるい)",
            "Adjective, い-adjective",
            "light"
        ],
        [
            "245",
            "傘",
            "kasa (かさ)",
            "Noun",
            "umbrella"
        ],
        [
            "246",
            "貸す",
            "kasu (かす)",
            "Verb, Godan verb, Transitive verb",
            "to lend; to loan"
        ],
        [
            "247",
            "方",
            "kata (かた)",
            "Noun",
            "way of doing something"
        ],
        [
            "248",
            "家庭",
            "katei (かてい)",
            "Noun",
            "household"
        ],
        [
            "249",
            "買う",
            "kau (かう)",
            "Verb, Godan verb, Transitive verb",
            "to buy; to purchase"
        ],
        [
            "250",
            "川",
            "kawa (かわ)",
            "Noun, Suffix",
            "river; stream​"
        ],
        [
            "251",
            "可愛い",
            "kawaii (かわいい)",
            "Adjective, い-adjective",
            "cute"
        ],
        [
            "252",
            "火曜日",
            "kayoubi (かようび)",
            "Noun",
            "Tuesday"
        ],
        [
            "253",
            "風邪",
            "kaze (かぜ)",
            "Noun",
            "a cold"
        ],
        [
            "254",
            "風",
            "kaze (かぜ)",
            "Noun",
            "wind"
        ],
        [
            "255",
            "家族",
            "kazoku (かぞく)",
            "Noun",
            "family; members of a family"
        ],
        [
            "256",
            "警官",
            "keikan (けいかん)",
            "Noun",
            "policeman; police officer"
        ],
        [
            "257",
            "結婚",
            "kekkon (けっこん)",
            "Noun, Suru verb",
            "marriage"
        ],
        [
            "258",
            "結構",
            "kekkou (けっこう)",
            "Noun, Adverbial Noun, Adjective, な-adjective, Adverb",
            "splendid, enough"
        ],
        [
            "259",
            "今朝",
            "kesa (けさ)",
            "Noun, Temporal noun",
            "this morning"
        ],
        [
            "260",
            "消す",
            "kesu (けす)",
            "Verb, Godan verb, Transitive verb",
            "to erase, to turn off power"
        ],
        [
            "261",
            "木",
            "ki (き)",
            "Noun",
            "tree; shrub; bush; wood; timber"
        ],
        [
            "262",
            "消える",
            "kieru (きえる)",
            "Verb, Ichidan verb, Intransitive verb",
            "to disappear"
        ],
        [
            "263",
            "黄色い",
            "kiiroi (きいろい)",
            "Adjective, い-adjective",
            "yellow"
        ],
        [
            "264",
            "聞く",
            "kiku (きく)",
            "Verb, Godan verb, Transitive verb",
            "to hear; to listen (to music); to ask; to learn of"
        ],
        [
            "265",
            "昨日",
            "kinou (きのう)",
            "Noun",
            "yesterday"
        ],
        [
            "266",
            "金曜日",
            "kinyoubi (きんようび)",
            "Noun",
            "Friday"
        ],
        [
            "267",
            "切符",
            "kippu (きっぷ)",
            "Noun",
            "ticket"
        ],
        [
            "268",
            "嫌い",
            "kirai (きらい)",
            "Noun, Adjective, な-adjective",
            "hate"
        ],
        [
            "269",
            "綺麗",
            "kirei (きれい)",
            "Adjective, な-adjective",
            "pretty; lovely; beautiful"
        ],
        [
            "270",
            "キログラム",
            "kiro guramu",
            "Noun, Katakana",
            "kilogram"
        ],
        [
            "271",
            "キロメートル",
            "kiro meetoru",
            "Noun, Katakana",
            "kilometer"
        ],
        [
            "272",
            "切る",
            "kiru (きる)",
            "Verb, Godan verb, Transitive verb",
            "to cut"
        ],
        [
            "273",
            "着る",
            "kiru (きる)",
            "Verb, Ichidan verb, Transitive verb",
            "to wear"
        ],
        [
            "274",
            "喫茶店",
            "kissaten (きっさてん)",
            "Noun",
            "coffee shop; tearoom; cafe"
        ],
        [
            "275",
            "北",
            "kita (きた)",
            "Noun",
            "north"
        ],
        [
            "276",
            "汚い",
            "kitanai (きたない)",
            "Adjective, い-adjective",
            "dirty"
        ],
        [
            "277",
            "切手",
            "kitte (きって)",
            "Noun",
            "stamp (postage)"
        ],
        [
            "278",
            "こっち",
            "kocchi",
            "Noun",
            "this person or way"
        ],
        [
            "279",
            "こちら",
            "kochira",
            "Noun",
            "this way; this direction​"
        ],
        [
            "280",
            "子供",
            "kodomo (こども)",
            "Noun",
            "child"
        ],
        [
            "281",
            "声",
            "koe (こえ)",
            "Noun",
            "voice"
        ],
        [
            "282",
            "ここ",
            "koko",
            "Pronoun",
            "here; this place"
        ],
        [
            "283",
            "九日",
            "kokonoka (ここのか)",
            "Noun",
            "ninth day of the month / 9 days"
        ],
        [
            "284",
            "九つ",
            "kokonotsu (ここのつ)",
            "Noun, Numeric",
            "nine; 9"
        ],
        [
            "285",
            "困る",
            "komaru (こまる)",
            "Verb, Godan verb, Intransitive verb",
            "to be troubled"
        ],
        [
            "286",
            "今晩",
            "konban (こんばん)",
            "Noun",
            "tonight; this evening"
        ],
        [
            "287",
            "今月",
            "kongetsu (こんげつ)",
            "Noun",
            "this month"
        ],
        [
            "288",
            "こんな",
            "konna",
            "Pre-noun adjectival",
            "such; like this​"
        ],
        [
            "289",
            "この",
            "kono",
            "Pre-noun adjectival",
            "this​"
        ],
        [
            "290",
            "今週",
            "konshuu (こんしゅう)",
            "Noun",
            "this week"
        ],
        [
            "291",
            "コーヒー",
            "koohii",
            "Noun, Katakana",
            "Coffee"
        ],
        [
            "292",
            "コート",
            "kooto",
            "Noun, Katakana",
            "coat"
        ],
        [
            "293",
            "コピー",
            "kopii",
            "Noun, Suru verb, Katakana",
            "copy; photocopy"
        ],
        [
            "294",
            "コップ",
            "koppu",
            "Noun, Katakana",
            "glass (drinking vessel); tumbler​"
        ],
        [
            "295",
            "これ",
            "kore",
            "Pronoun",
            "this"
        ],
        [
            "296",
            "答える",
            "kotaeru (こたえる)",
            "Verb, Ichidan verb, Intransitive verb",
            "to answer"
        ],
        [
            "297",
            "言葉",
            "kotoba (ことば)",
            "Noun",
            "word; words"
        ],
        [
            "298",
            "今年",
            "kotoshi (ことし)",
            "Noun",
            "this year"
        ],
        [
            "299",
            "交番",
            "kouban (こうばん)",
            "Noun",
            "police box"
        ],
        [
            "300",
            "紅茶",
            "koucha (こうちゃ)",
            "Noun",
            "black tea"
        ],
        [
            "301",
            "公園",
            "kouen (こうえん)",
            "Noun",
            "park"
        ],
        [
            "302",
            "交差点",
            "kousaten (こうさてん)",
            "Noun",
            "intersection"
        ],
        [
            "303",
            "口",
            "kuchi (くち)",
            "Noun",
            "mouth, opening"
        ],
        [
            "304",
            "果物",
            "kudamono (くだもの)",
            "Noun",
            "fruit"
        ],
        [
            "305",
            "下さい",
            "kudasai (ください)",
            "Expression",
            "please"
        ],
        [
            "306",
            "曇り",
            "kumori (くもり)",
            "Noun",
            "cloudiness; cloudy weather"
        ],
        [
            "307",
            "曇る",
            "kumoru (くもる)",
            "Verb, Godan verb, Intransitive verb",
            "to become cloudy, to become dim"
        ],
        [
            "308",
            "国",
            "kuni (くに)",
            "Noun",
            "country; state; region"
        ],
        [
            "309",
            "暗い",
            "kurai (くらい)",
            "Adjective, い-adjective",
            "dark; gloomy; murky"
        ],
        [
            "310",
            "クラス",
            "kurasu",
            "Noun, Katakana",
            "class"
        ],
        [
            "311",
            "黒",
            "kuro (くろ)",
            "Noun",
            "black"
        ],
        [
            "312",
            "黒い",
            "kuroi (くろい)",
            "い-adjective",
            "black"
        ],
        [
            "313",
            "来る",
            "kuru (くる)",
            "Verb, Intransitive verb",
            "to come"
        ],
        [
            "314",
            "車",
            "kuruma (くるま)",
            "Noun",
            "car; automobile; vehicle"
        ],
        [
            "315",
            "薬",
            "kusuri (くすり)",
            "Noun",
            "medicine"
        ],
        [
            "316",
            "靴",
            "kutsu (くつ)",
            "Noun",
            "shoes"
        ],
        [
            "317",
            "靴下",
            "kutsushita (くつした)",
            "Noun",
            "socks"
        ],
        [
            "318",
            "去年",
            "kyonen (きょねん)",
            "Noun",
            "last year"
        ],
        [
            "319",
            "今日",
            "kyou (きょう)",
            "Noun, Temporal noun",
            "today; this day"
        ],
        [
            "320",
            "兄弟",
            "kyoudai (きょうだい)",
            "Noun",
            "siblings; brothers and sisters​; mate"
        ],
        [
            "321",
            "教室",
            "kyoushitsu (きょうしつ)",
            "Noun",
            "classroom"
        ],
        [
            "322",
            "九",
            "kyuu (きゅう)",
            "Noun, Numeric",
            "nine; 9"
        ],
        [
            "323",
            "マッチ",
            "macchi",
            "Noun, Katakana",
            "match"
        ],
        [
            "324",
            "町",
            "machi (まち)",
            "Noun",
            "town; block; neighborhood"
        ],
        [
            "325",
            "窓",
            "mado (まど)",
            "Noun",
            "window"
        ],
        [
            "326",
            "前",
            "mae (まえ)",
            "Noun, Suffix",
            "previous; before; in front; ago"
        ],
        [
            "327",
            "曲がる",
            "magaru (まがる)",
            "Verb, Godan verb, Intransitive verb",
            "to turn, to bend"
        ],
        [
            "328",
            "毎朝",
            "maiasa (まいあさ)",
            "Noun",
            "every morning"
        ],
        [
            "329",
            "毎晩",
            "maiban (まいばん)",
            "Noun",
            "every night"
        ],
        [
            "330",
            "毎日",
            "mainichi (まいにち)",
            "Noun",
            "every day"
        ],
        [
            "331",
            "毎週",
            "maishuu (まいしゅう)",
            "Noun",
            "every week"
        ],
        [
            "332",
            "毎年",
            "maitoshi / mainen (まいとし / まいねん)",
            "Noun",
            "every year; yearly; annually"
        ],
        [
            "333",
            "毎月",
            "maitsuki (まいつき)",
            "Noun",
            "every month; monthly"
        ],
        [
            "334",
            "万",
            "man (まん)",
            "Noun, Numeric",
            "10,000; ten thousand"
        ],
        [
            "335",
            "万年筆",
            "mannenhitsu (まんねんひつ)",
            "Noun",
            "fountain pen"
        ],
        [
            "336",
            "丸い",
            "marui (まるい)",
            "Adjective, い-adjective",
            "round,circular"
        ],
        [
            "337",
            "真っ直ぐ",
            "massugu (まっすぐ)",
            "Noun, Adjective, な-adjective, Adverb",
            "straight ahead,direct"
        ],
        [
            "338",
            "待つ",
            "matsu (まつ)",
            "Verb, Godan verb, Intransitive verb, Transitive verb",
            "to wait​"
        ],
        [
            "339",
            "不味い",
            "mazui (まずい)",
            "Adjective, い-adjective",
            "unpleasant"
        ],
        [
            "340",
            "目",
            "me (め)",
            "Noun",
            "eye"
        ],
        [
            "341",
            "メートル",
            "meetoru",
            "Noun, Katakana",
            "metre; meter"
        ],
        [
            "342",
            "眼鏡",
            "megane (めがね)",
            "Noun",
            "glasses"
        ],
        [
            "343",
            "道",
            "michi (みち)",
            "Noun",
            "road; street"
        ],
        [
            "344",
            "緑",
            "midori (みどり)",
            "Noun",
            "green"
        ],
        [
            "345",
            "磨く",
            "migaku (みがく)",
            "Verb, Godan verb, Intransitive verb",
            "to polish; to shine; to brush (e.g. teeth)"
        ],
        [
            "346",
            "右",
            "migi (みぎ)",
            "Noun",
            "right; right hand side"
        ],
        [
            "347",
            "短い",
            "mijikai (みじかい)",
            "Adjective, い-adjective",
            "short"
        ],
        [
            "348",
            "三日",
            "mikka (みっか)",
            "Noun",
            "the third day of the month / 3 days"
        ],
        [
            "349",
            "耳",
            "mimi (みみ)",
            "Noun",
            "ear; hearing"
        ],
        [
            "350",
            "南",
            "minami (みなみ)",
            "Noun",
            "south"
        ],
        [
            "351",
            "皆さん",
            "minasan (みなさん)",
            "Noun",
            "everyone"
        ],
        [
            "352",
            "みんな",
            "minna",
            "Noun, Adverb",
            "all; everyone; everybody"
        ],
        [
            "353",
            "見る",
            "miru (みる)",
            "Verb, Ichidan verb, Transitive verb",
            "to see; to look; to watch; to view; to observe"
        ],
        [
            "354",
            "店",
            "mise (みせ)",
            "Noun",
            "store; shop; establishment; restaurant"
        ],
        [
            "355",
            "見せる",
            "miseru (みせる)",
            "Verb, Ichidan verb, Transitive verb",
            "to show; to display"
        ],
        [
            "356",
            "三つ",
            "mittsu (みっつ)",
            "Noun, Numeric",
            "three; 3"
        ],
        [
            "357",
            "水",
            "mizu (みず)",
            "Noun",
            "water; fluid; liquid​"
        ],
        [
            "358",
            "木曜日",
            "mokuyoubi (もくようび)",
            "Noun",
            "Thursday"
        ],
        [
            "359",
            "門",
            "mon (もん)",
            "Noun",
            "gate"
        ],
        [
            "360",
            "問題",
            "mondai (もんだい)",
            "Noun",
            "problem; question (e.g. on a test)"
        ],
        [
            "361",
            "物",
            "mono (もの)",
            "Noun",
            "thing"
        ],
        [
            "362",
            "持つ",
            "motsu (もつ)",
            "Verb, Godan verb",
            "to hold"
        ],
        [
            "363",
            "もっと",
            "motto",
            "Adverb",
            "more; longer; further"
        ],
        [
            "364",
            "もう一度",
            "mouichido (もういちど)",
            "Expression",
            "once more; again"
        ],
        [
            "365",
            "六日",
            "muika (むいか)",
            "Noun",
            "sixth day of the month / 6 days"
        ],
        [
            "366",
            "向こう",
            "mukou (むこう)",
            "Noun",
            "over there"
        ],
        [
            "367",
            "村",
            "mura (むら)",
            "Noun",
            "village"
        ],
        [
            "368",
            "六つ",
            "muttsu (むっつ)",
            "Noun, Numeric",
            "six; 6"
        ],
        [
            "369",
            "難しい",
            "muzukashii (むずかしい)",
            "Adjective, い-adjective",
            "difficult"
        ],
        [
            "370",
            "長い",
            "nagai (ながい)",
            "Adjective, い-adjective",
            "long (distance)​; long (time); lengthy."
        ],
        [
            "371",
            "ナイフ",
            "naifu",
            "Noun, Katakana",
            "knife"
        ],
        [
            "372",
            "中",
            "naka (なか)",
            "Noun",
            "inside; in; within; center"
        ],
        [
            "373",
            "鳴く",
            "naku (なく)",
            "Verb, Godan verb, Intransitive verb",
            "animal noise. to chirp"
        ],
        [
            "374",
            "無くす",
            "nakusu (なくす)",
            "Verb, Godan verb, Transitive verb",
            "to lose (something)"
        ],
        [
            "375",
            "名前",
            "namae (なまえ)",
            "Noun",
            "name; full name; given name"
        ],
        [
            "376",
            "七つ",
            "nanatsu (ななつ)",
            "Noun, Numeric",
            "seven; 7"
        ],
        [
            "377",
            "何",
            "nani (なに)",
            "Noun, Pronoun, Prefix",
            "what"
        ],
        [
            "378",
            "七日",
            "nanoka (なのか)",
            "Noun",
            "seventh day of the month / 7 days"
        ],
        [
            "379",
            "並べる",
            "naraberu (ならべる)",
            "Verb, Ichidan verb, Transitive verb",
            "to line up,to set up"
        ],
        [
            "380",
            "並ぶ",
            "narabu (ならぶ)",
            "Verb, Godan verb, Intransitive verb",
            "to line up,to stand in a line"
        ],
        [
            "381",
            "習う",
            "narau (ならう)",
            "Verb, Godan verb, Transitive verb",
            "to be taught; to learn (from a teacher)"
        ],
        [
            "382",
            "夏",
            "natsu (なつ)",
            "Noun",
            "summer"
        ],
        [
            "383",
            "夏休み",
            "natsuyasumi (なつやすみ)",
            "Noun",
            "summer vacation; summer holiday"
        ],
        [
            "384",
            "何故",
            "naze (なぜ)",
            "Adverb",
            "why; how"
        ],
        [
            "385",
            "猫",
            "neko (ねこ)",
            "Noun",
            "cat"
        ],
        [
            "386",
            "ネクタイ",
            "nekutai",
            "Noun, Katakana",
            "tie; necktie"
        ],
        [
            "387",
            "寝る",
            "neru (ねる)",
            "Verb, Ichidan verb, Intransitive verb",
            "to sleep; to go to bed; to lie down"
        ],
        [
            "388",
            "二",
            "ni (に)",
            "Noun, Numeric",
            "two; 2"
        ],
        [
            "389",
            "日曜日",
            "nichiyoubi (にちようび)",
            "Noun",
            "Sunday"
        ],
        [
            "390",
            "賑やか",
            "nigiyaka (にぎやか)",
            "Adjective, な-adjective",
            "bustling,busy"
        ],
        [
            "391",
            "日記",
            "nikki (にっき)",
            "Noun",
            "diary; journal"
        ],
        [
            "392",
            "肉",
            "niku (にく)",
            "Noun",
            "meat"
        ],
        [
            "393",
            "荷物",
            "nimotsu (にもつ)",
            "Noun",
            "luggage; baggage"
        ],
        [
            "394",
            "西",
            "nishi (にし)",
            "Noun",
            "west"
        ],
        [
            "395",
            "庭",
            "niwa (にわ)",
            "Noun",
            "garden"
        ],
        [
            "396",
            "登る",
            "noboru (のぼる)",
            "Verb, Godan verb, Intransitive verb",
            "to climb"
        ],
        [
            "397",
            "飲み物",
            "nomimono (のみもの)",
            "Noun",
            "drink; beverage"
        ],
        [
            "398",
            "飲む",
            "nomu (のむ)",
            "Verb, Godan verb, Transitive verb",
            "to drink"
        ],
        [
            "399",
            "ノート",
            "nooto",
            "Noun, Katakana",
            "notebook"
        ],
        [
            "400",
            "乗る",
            "noru (のる)",
            "Verb, Godan verb, Intransitive verb",
            "to get on (train, plane, bus, ship, etc.)"
        ],
        [
            "401",
            "脱ぐ",
            "nugu (ぬぐ)",
            "Verb, Godan verb, Transitive verb",
            "to take off clothes"
        ],
        [
            "402",
            "温い",
            "nurui (ぬるい)",
            "Adjective, い-adjective",
            "luke warm"
        ],
        [
            "403",
            "ニュース",
            "nyuusu",
            "Noun, Katakana",
            "news"
        ],
        [
            "404",
            "おばあさん",
            "obaasan",
            "Noun",
            "grandmother"
        ],
        [
            "405",
            "伯母さん",
            "obasan (おばさん)",
            "Noun",
            "aunt; old lady"
        ],
        [
            "406",
            "お弁当",
            "obentou (おべんとう)",
            "Noun",
            "lunch box; Japanese box lunch"
        ],
        [
            "407",
            "覚える",
            "oboeru (おぼえる)",
            "Verb, Ichidan verb, Transitive verb",
            "to remember"
        ],
        [
            "408",
            "お茶",
            "ocha (おちゃ)",
            "Noun",
            "tea"
        ],
        [
            "409",
            "お風呂",
            "ofuro (おふろ)",
            "Noun",
            "bath"
        ],
        [
            "410",
            "美味しい",
            "oishii (おいしい)",
            "い-adjective",
            "delicious"
        ],
        [
            "411",
            "伯父さん",
            "ojisan (おじさん)",
            "Noun",
            "uncle; old man; mister"
        ],
        [
            "412",
            "お母さん",
            "okaasan (おかあさん)",
            "Noun",
            "mother; mom; mum; ma"
        ],
        [
            "413",
            "お金",
            "okane (おかね)",
            "Noun",
            "money"
        ],
        [
            "414",
            "お菓子",
            "okashi (おかし)",
            "Noun",
            "confections; sweets; candy"
        ],
        [
            "415",
            "起きる",
            "okiru (おきる)",
            "Verb, Ichidan verb, Intransitive verb",
            "to get up; to wake up"
        ],
        [
            "416",
            "置く",
            "oku (おく)",
            "Verb, Godan verb, Transitive verb",
            "to put; to place​"
        ],
        [
            "417",
            "奥さん",
            "okusan (おくさん)",
            "Noun",
            "wife; your wife; his wife"
        ],
        [
            "418",
            "お巡りさん",
            "omawari san (おまわりさん)",
            "Noun",
            "police officer (friendly term for policeman)"
        ],
        [
            "419",
            "重い",
            "omoi (おもい)",
            "い-adjective",
            "heavy"
        ],
        [
            "420",
            "面白い",
            "omoshiroi (おもしろい)",
            "Adjective, い-adjective",
            "interesting"
        ],
        [
            "421",
            "同じ",
            "onaji (おなじ)",
            "Noun, Adverb",
            "same"
        ],
        [
            "422",
            "お腹",
            "onaka (おなか)",
            "Noun",
            "stomach"
        ],
        [
            "423",
            "お姉さん",
            "oneesan (おねえさん)",
            "Noun",
            "elder sister; young lady; miss; ma'am"
        ],
        [
            "424",
            "音楽",
            "ongaku (おんがく)",
            "Noun",
            "music"
        ],
        [
            "425",
            "お兄さん",
            "oniisan (おにいさん)",
            "Noun",
            "older brother; elder brother; young man; buddy"
        ],
        [
            "426",
            "女",
            "onna (おんな)",
            "Noun, Prefix",
            "woman; female sex"
        ],
        [
            "427",
            "女の子",
            "onnanoko (おんなのこ)",
            "Noun, Expression",
            "girl; daughter; young women"
        ],
        [
            "428",
            "多い",
            "ooi (おおい)",
            "Adjective, い-adjective",
            "many; numerous; a lot; large quantity; frequent"
        ],
        [
            "429",
            "大きい",
            "ookii (おおきい)",
            "Adjective, い-adjective",
            "big; large; great; important"
        ],
        [
            "430",
            "大きな",
            "ookina (おおきな)",
            "Pre-noun adjectival",
            "big; large; great​"
        ],
        [
            "431",
            "大勢",
            "oozei (おおぜい)",
            "Noun",
            "crowd of people; great number of people"
        ],
        [
            "432",
            "降りる",
            "oriru (おりる)",
            "Verb, Ichidan verb, Intransitive verb",
            "to get off"
        ],
        [
            "433",
            "お酒",
            "osake (おさけ)",
            "Noun",
            "alcohol"
        ],
        [
            "434",
            "お皿",
            "osara (おさら)",
            "Noun",
            "plate, dish"
        ],
        [
            "435",
            "教える",
            "oshieru (おしえる)",
            "Verb, Ichidan verb, Transitive verb",
            "to teach"
        ],
        [
            "436",
            "遅い",
            "osoi (おそい)",
            "Adjective, い-adjective",
            "slow; time-consuming; late"
        ],
        [
            "437",
            "押す",
            "osu (おす)",
            "Verb, Godan verb, Transitive verb",
            "to push; to press​"
        ],
        [
            "438",
            "お手洗い",
            "otearai (おてあらい)",
            "Noun",
            "toilet; restroom; lavatory; bathroom"
        ],
        [
            "439",
            "男",
            "otoko (おとこ)",
            "Noun",
            "man; male"
        ],
        [
            "440",
            "男の子",
            "otokonoko (おとこのこ)",
            "Noun, Expression",
            "boy; male child; baby boy"
        ],
        [
            "441",
            "大人",
            "otona (おとな)",
            "Noun",
            "adult"
        ],
        [
            "442",
            "一昨日",
            "ototoi (おととい)",
            "Noun",
            "day before yesterday"
        ],
        [
            "443",
            "一昨年",
            "ototoshi (おととし)",
            "Noun",
            "year before last"
        ],
        [
            "444",
            "お父さん",
            "otousan (おとうさん)",
            "Noun",
            "father; dad; papa; pa; pop; daddy"
        ],
        [
            "445",
            "弟",
            "otouto (おとうと)",
            "Noun",
            "younger brother"
        ],
        [
            "446",
            "終る",
            "owaru (おわる)",
            "Verb, Godan verb, Intransitive verb",
            "to finish; to end"
        ],
        [
            "447",
            "泳ぐ",
            "oyogu (およぐ)",
            "Verb, Godan verb, Intransitive verb",
            "to swim"
        ],
        [
            "448",
            "パーティー",
            "paatii",
            "Noun, Katakana",
            "party"
        ],
        [
            "449",
            "パン",
            "pan",
            "Noun, Katakana",
            "bread"
        ],
        [
            "450",
            "ページ",
            "peeji",
            "Noun, Katakana",
            "page"
        ],
        [
            "451",
            "ペン",
            "pen",
            "Noun, Katakana",
            "pen"
        ],
        [
            "452",
            "ペット",
            "petto",
            "Noun, Katakana",
            "pet"
        ],
        [
            "453",
            "ポケット",
            "poketto",
            "Noun",
            "pocket"
        ],
        [
            "454",
            "ポスト",
            "posuto",
            "Noun, Katakana",
            "post"
        ],
        [
            "455",
            "プール",
            "puuru",
            "Noun, Katakana",
            "swimming pool"
        ],
        [
            "456",
            "来月",
            "raigetsu (らいげつ)",
            "Noun",
            "next month"
        ],
        [
            "457",
            "来年",
            "rainen (らいねん)",
            "Noun",
            "next year"
        ],
        [
            "458",
            "来週",
            "raishuu (らいしゅう)",
            "Noun",
            "next week"
        ],
        [
            "459",
            "ラジオ",
            "rajiio",
            "Noun, Katakana",
            "radio"
        ],
        [
            "460",
            "零",
            "rei (れい)",
            "Noun",
            "zero"
        ],
        [
            "461",
            "冷蔵庫",
            "reizouko (れいぞうこ)",
            "Noun",
            "refrigerator"
        ],
        [
            "462",
            "レコード",
            "rekoodo",
            "Noun, Katakana",
            "record"
        ],
        [
            "463",
            "練習",
            "renshuu (れんしゅう)",
            "Noun, Verb, Suru verb",
            "practice; practicing"
        ],
        [
            "464",
            "レストラン",
            "resutoran",
            "Noun, Katakana",
            "restaurant"
        ],
        [
            "465",
            "立派",
            "rippa (りっぱ)",
            "Adjective, な-adjective",
            "splendid"
        ],
        [
            "466",
            "六",
            "roku (ろく)",
            "Noun, Numeric",
            "six; 6"
        ],
        [
            "467",
            "廊下",
            "rouka (ろうか)",
            "Noun",
            "corridor; hallway; passageway"
        ],
        [
            "468",
            "旅行",
            "ryokou (りょこう)",
            "Noun, Verb, Suru verb",
            "travel; trip; journey; excursion; tour"
        ],
        [
            "469",
            "料理",
            "ryouri (りょうり)",
            "Noun, Suru verb",
            "cuisine"
        ],
        [
            "470",
            "両親",
            "ryoushin (りょうしん)",
            "Noun",
            "parents; both parents"
        ],
        [
            "471",
            "留学生",
            "ryuugakusei (りゅうがくせい)",
            "Noun",
            "overseas student; exchange student"
        ],
        [
            "472",
            "さあ",
            "saa",
            "Conjunction",
            "well…"
        ],
        [
            "473",
            "財布",
            "saifu (さいふ)",
            "Noun",
            "purse; wallet"
        ],
        [
            "474",
            "魚",
            "sakana (さかな)",
            "Noun",
            "fish"
        ],
        [
            "475",
            "先",
            "saki (さき)",
            "Noun, Prefix, Suffix",
            "previous; prior; first; earlier"
        ],
        [
            "476",
            "咲く",
            "saku (さく)",
            "Verb, Godan verb, Intransitive verb",
            "to bloom"
        ],
        [
            "477",
            "作文",
            "sakubun (さくぶん)",
            "Noun",
            "writing; composition"
        ],
        [
            "478",
            "寒い",
            "samui (さむい)",
            "Adjective, い-adjective",
            "cold"
        ],
        [
            "479",
            "三",
            "san (さん)",
            "Noun, Numeric",
            "three; 3"
        ],
        [
            "480",
            "散歩",
            "sanpo (さんぽ)",
            "Noun, Verb, Suru verb",
            "walk; stroll"
        ],
        [
            "481",
            "再来年",
            "sarainen (さらいねん)",
            "Noun",
            "year after next"
        ],
        [
            "482",
            "差す",
            "sasu (さす)",
            "Verb, Godan verb, Transitive verb",
            "to stretch out hands, to raise an umbrella"
        ],
        [
            "483",
            "砂糖",
            "satou (さとう)",
            "Noun",
            "sugar"
        ],
        [
            "484",
            "背",
            "se (せ)",
            "Noun",
            "height; stature; back; spine"
        ],
        [
            "485",
            "背広",
            "sebiro (せびろ)",
            "Noun",
            "business suit"
        ],
        [
            "486",
            "セーター",
            "seetaa",
            "Noun, Katakana",
            "sweater; jumper"
        ],
        [
            "487",
            "生徒",
            "seito (せいと)",
            "Noun",
            "pupil; student"
        ],
        [
            "488",
            "石鹼",
            "sekken (せっけん)",
            "Noun",
            "soap"
        ],
        [
            "489",
            "狭い",
            "semai (せまい)",
            "い-adjective",
            "narrow"
        ],
        [
            "490",
            "千",
            "sen (せん)",
            "Noun, Numeric",
            "1,000; thousand"
        ],
        [
            "491",
            "先月",
            "sengetsu (せんげつ)",
            "Noun",
            "last month"
        ],
        [
            "492",
            "先生",
            "sensei (せんせい)",
            "Noun, Suffix",
            "teacher; instructor; master"
        ],
        [
            "493",
            "先週",
            "senshuu (せんしゅう)",
            "Noun",
            "last week"
        ],
        [
            "494",
            "洗濯",
            "sentaku (せんたく)",
            "Noun, Suru verb",
            "washing; laundry"
        ],
        [
            "495",
            "写真",
            "shashin (しゃしん)",
            "Noun",
            "photograph; photo"
        ],
        [
            "496",
            "シャツ",
            "shatsu",
            "Noun, Katakana",
            "shirt"
        ],
        [
            "497",
            "シャワー",
            "shawaa",
            "Noun, Katakana",
            "shower"
        ],
        [
            "498",
            "四",
            "shi (し)",
            "Noun, Numeric",
            "four; 4"
        ],
        [
            "499",
            "七",
            "shichi (しち)",
            "Noun, Numeric",
            "seven; 7"
        ],
        [
            "500",
            "仕事",
            "shigoto (しごと)",
            "Noun, Verb, Suru verb",
            "work; job; business"
        ],
        [
            "501",
            "閉まる",
            "shimaru (しまる)",
            "Verb, Godan verb, Intransitive verb",
            "to close, to be closed"
        ],
        [
            "502",
            "締める",
            "shimeru (しめる)",
            "Verb, Ichidan verb, Transitive verb",
            "to tie; to fasten; to tighten​"
        ],
        [
            "503",
            "閉める",
            "shimeru (しめる)",
            "Verb, Ichidan verb, Transitive verb",
            "to close; to shut"
        ],
        [
            "504",
            "新聞",
            "shinbun (しんぶん)",
            "Noun",
            "newspaper"
        ],
        [
            "505",
            "死ぬ",
            "shinu (しぬ)",
            "Verb, Godan verb, Intransitive verb",
            "to die"
        ],
        [
            "506",
            "塩",
            "shio (しお)",
            "Noun",
            "salt"
        ],
        [
            "507",
            "白",
            "shiro (しろ)",
            "Noun",
            "white; innocence; innocent person"
        ],
        [
            "508",
            "白い",
            "shiroi (しろい)",
            "Adjective, い-adjective",
            "white"
        ],
        [
            "509",
            "知る",
            "shiru (しる)",
            "Verb, Godan verb, Transitive verb",
            "to know"
        ],
        [
            "510",
            "下",
            "shita (した)",
            "Noun",
            "below; down; under; bottom"
        ],
        [
            "511",
            "質問",
            "shitsumon (しつもん)",
            "Noun, Verb, Suru verb",
            "question; inquiry"
        ],
        [
            "512",
            "静か",
            "shizuka (しずか)",
            "Adjective, な-adjective",
            "quiet"
        ],
        [
            "513",
            "食堂",
            "shokudou (しょくどう)",
            "Noun",
            "cafeteria; dining room"
        ],
        [
            "514",
            "醬油",
            "shouyu (しょうゆ)",
            "Noun",
            "soy sauce"
        ],
        [
            "515",
            "宿題",
            "shukudai (しゅくだい)",
            "Noun",
            "homework; assignment; pending issue"
        ],
        [
            "516",
            "そば",
            "soba",
            "Noun",
            "near; beside"
        ],
        [
            "517",
            "そっち",
            "socchi",
            "Pronoun",
            "that way; ​over there"
        ],
        [
            "518",
            "そちら",
            "sochira",
            "Pronoun",
            "that way (distant from speaker, close to listener); you; your family"
        ],
        [
            "519",
            "そこ",
            "soko",
            "Pronoun",
            "that place​; there"
        ],
        [
            "520",
            "その",
            "sono",
            "Pre-noun adjectival",
            "that"
        ],
        [
            "521",
            "空",
            "sora (そら)",
            "Noun",
            "sky; the air"
        ],
        [
            "522",
            "それ",
            "sore",
            "Pronoun",
            "that"
        ],
        [
            "523",
            "それでは",
            "sore dewa",
            "Expression",
            "in that situation"
        ],
        [
            "524",
            "外",
            "soto (そと)",
            "Noun",
            "outside; exterior;"
        ],
        [
            "525",
            "掃除",
            "souji (そうじ)",
            "Noun, Suru verb",
            "to clean, to sweep"
        ],
        [
            "526",
            "直ぐに",
            "sugu ni (すぐに)",
            "Adverb",
            "immediately; right away; instantly​"
        ],
        [
            "527",
            "水曜日",
            "suiyoubi (すいようび)",
            "Noun",
            "Wednesday"
        ],
        [
            "528",
            "スカート",
            "sukaato",
            "Noun, Katakana",
            "skirt"
        ],
        [
            "529",
            "好き",
            "suki (すき)",
            "Noun, Adjective, な-adjective",
            "like"
        ],
        [
            "530",
            "少し",
            "sukoshi (すこし)",
            "Noun, Adverb",
            "a little (bit); small quantity; few; short distance"
        ],
        [
            "531",
            "少ない",
            "sukunai (すくない)",
            "い-adjective",
            "few; a little; scarce; insufficient; seldom"
        ],
        [
            "532",
            "住む",
            "sumu (すむ)",
            "Verb, Godan verb, Intransitive verb",
            "to live in; to reside; to inhabit; to dwell; to abide"
        ],
        [
            "533",
            "スポーツ",
            "supootsu",
            "Noun, Katakana",
            "sport; sports"
        ],
        [
            "534",
            "スプーン",
            "supuun",
            "Noun, Katakana",
            "spoon"
        ],
        [
            "535",
            "スリッパ",
            "surippa",
            "Noun, Katakana",
            "slipper; slippers"
        ],
        [
            "536",
            "ストーブ",
            "sutoobu",
            "Noun, Katakana",
            "heater; stove"
        ],
        [
            "537",
            "吸う",
            "suu (すう)",
            "Verb, Godan verb, Transitive verb",
            "to smoke, to suck"
        ],
        [
            "538",
            "座る",
            "suwaru (すわる)",
            "Verb, Godan verb, Intransitive verb",
            "to sit"
        ],
        [
            "539",
            "涼しい",
            "suzushii (すずしい)",
            "Adjective, い-adjective",
            "refreshing, cool"
        ],
        [
            "540",
            "たばこ",
            "tabako",
            "Noun",
            "tobacco; cigarette"
        ],
        [
            "541",
            "食べ物",
            "tabemono (たべもの)",
            "Noun",
            "food"
        ],
        [
            "542",
            "食べる",
            "taberu (たべる)",
            "Verb, Ichidan verb, Intransitive verb",
            "to eat"
        ],
        [
            "543",
            "多分",
            "tabun (たぶん)",
            "Adjective, な-adjective, Adverb",
            "perhaps; probably"
        ],
        [
            "544",
            "大変",
            "taihen (たいへん)",
            "Noun, Adjective, な-adjective, Adverb",
            "very; greatly; terribly; serious; difficult"
        ],
        [
            "545",
            "大切",
            "taisetsu (たいせつ)",
            "Noun, Adjective, な-adjective",
            "important; necessary; indispensable; beloved"
        ],
        [
            "546",
            "大使館",
            "taishikan (たいしかん)",
            "Noun",
            "embassy"
        ],
        [
            "547",
            "高い",
            "takai (たかい)",
            "Adjective, い-adjective",
            "high; tall; expensive; above average"
        ],
        [
            "548",
            "沢山",
            "takusan (たくさん)",
            "Noun, Adverbial Noun, Adjective, な-adjective",
            "many"
        ],
        [
            "549",
            "タクシー",
            "takushii",
            "Noun, Katakana",
            "taxi"
        ],
        [
            "550",
            "卵",
            "tamago (たまご)",
            "Noun",
            "eggs; egg"
        ],
        [
            "551",
            "誕生日",
            "tanjoubi (たんじょうび)",
            "Noun",
            "birthday"
        ],
        [
            "552",
            "頼む",
            "tanomu (たのむ)",
            "Verb, Godan verb, Transitive verb",
            "to ask"
        ],
        [
            "553",
            "楽しい",
            "tanoshii (たのしい)",
            "Adjective, い-adjective",
            "enjoyable; fun"
        ],
        [
            "554",
            "縦",
            "tate (たて)",
            "Noun",
            "length,height"
        ],
        [
            "555",
            "建物",
            "tatemono (たてもの)",
            "Noun",
            "building"
        ],
        [
            "556",
            "立つ",
            "tatsu (たつ)",
            "Verb, Godan verb, Intransitive verb",
            "to stand; to stand up​"
        ],
        [
            "557",
            "手",
            "te (て)",
            "Noun",
            "hand; arm"
        ],
        [
            "558",
            "テーブル",
            "teeburu",
            "Noun, Katakana",
            "table"
        ],
        [
            "559",
            "テープ",
            "teepu",
            "Noun, Katakana",
            "tape"
        ],
        [
            "560",
            "テープレコーダー",
            "teepu rekoodaa",
            "Noun, Katakana",
            "tape recorder"
        ],
        [
            "561",
            "手紙",
            "tegami (てがみ)",
            "Noun",
            "Letter (message)​"
        ],
        [
            "562",
            "天気",
            "tenki (てんき)",
            "Noun",
            "weather; the elements"
        ],
        [
            "563",
            "テレビ",
            "terebi",
            "Noun, Katakana",
            "television; TV​"
        ],
        [
            "564",
            "テスト",
            "tesuto",
            "Noun, Suru verb, Katakana",
            "examination; quiz; test"
        ],
        [
            "565",
            "戸",
            "to (と)",
            "Noun",
            "Japanese style door"
        ],
        [
            "566",
            "飛ぶ",
            "tobu (とぶ)",
            "Verb, Godan verb, Intransitive verb",
            "to fly; to hop"
        ],
        [
            "567",
            "トイレ",
            "toire",
            "Noun, Katakana",
            "toilet"
        ],
        [
            "568",
            "時計",
            "tokei (とけい)",
            "Noun",
            "watch; clock; timepiece"
        ],
        [
            "569",
            "時",
            "toki (とき)",
            "Noun",
            "time; moment; occasion; chance"
        ],
        [
            "570",
            "時々",
            "tokidoki (ときどき)",
            "Noun, Adverb",
            "sometimes; at times"
        ],
        [
            "571",
            "所",
            "tokoro (ところ)",
            "Noun, Suffix",
            "place"
        ],
        [
            "572",
            "止まる",
            "tomaru (とまる)",
            "Verb, Godan verb, Intransitive verb",
            "to stop; to come to a halt"
        ],
        [
            "573",
            "友達",
            "tomodachi (ともだち)",
            "Noun",
            "friend; companion"
        ],
        [
            "574",
            "隣",
            "tonari (となり)",
            "Noun",
            "next door to"
        ],
        [
            "575",
            "遠い",
            "tooi (とおい)",
            "Adjective, い-adjective",
            "far"
        ],
        [
            "576",
            "十日",
            "tooka (とおか)",
            "Noun",
            "tenth day of the month / 10 days"
        ],
        [
            "577",
            "鳥",
            "tori (とり)",
            "Noun",
            "bird"
        ],
        [
            "578",
            "鶏肉",
            "toriniku (とりにく)",
            "Noun",
            "chicken meat"
        ],
        [
            "579",
            "撮る",
            "toru (とる)",
            "Verb, Godan verb, Transitive verb",
            "to take a photo or record a film"
        ],
        [
            "580",
            "取る",
            "toru (とる)",
            "Verb, Godan verb, Transitive verb",
            "to take; to pick up; to harvest; to earn; to win; to choose"
        ],
        [
            "581",
            "年",
            "toshi (とし)",
            "Noun",
            "year; age"
        ],
        [
            "582",
            "図書館",
            "toshokan (としょかん)",
            "Noun",
            "library"
        ],
        [
            "583",
            "次",
            "tsugi (つぎ)",
            "Noun",
            "next"
        ],
        [
            "584",
            "一日",
            "tsuitachi (ついたち)",
            "Noun",
            "first day of the month"
        ],
        [
            "585",
            "疲れる",
            "tsukareru (つかれる)",
            "Verb, Ichidan verb, Intransitive verb",
            "to get tired"
        ],
        [
            "586",
            "使う",
            "tsukau (つかう)",
            "Verb, Godan verb, Transitive verb",
            "to use"
        ],
        [
            "587",
            "つける",
            "tsukeru",
            "Verb, Ichidan verb, Transitive verb",
            "to turn on"
        ],
        [
            "588",
            "着く",
            "tsuku (つく)",
            "Verb, Godan verb, Intransitive verb",
            "to arrive at"
        ],
        [
            "589",
            "机",
            "tsukue (つくえ)",
            "Noun",
            "desk"
        ],
        [
            "590",
            "作る",
            "tsukuru (つくる)",
            "Verb, Godan verb, Transitive verb",
            "to make"
        ],
        [
            "591",
            "詰まらない",
            "tsumaranai (つまらない)",
            "Adjective, い-adjective",
            "boring"
        ],
        [
            "592",
            "冷たい",
            "tsumetai (つめたい)",
            "い-adjective",
            "cold to the touch"
        ],
        [
            "593",
            "勤める",
            "tsutomeru (つとめる)",
            "Verb, Ichidan verb, Transitive verb",
            "to work for someone"
        ],
        [
            "594",
            "強い",
            "tsuyoi (つよい)",
            "Adjective, い-adjective",
            "powerful"
        ],
        [
            "595",
            "上",
            "ue (うえ)",
            "Noun, Suffix",
            "above; up; over; top; surface"
        ],
        [
            "596",
            "生まれる",
            "umareru (うまれる)",
            "Verb, Ichidan verb, Intransitive verb",
            "to be born"
        ],
        [
            "597",
            "海",
            "umi (うみ)",
            "Noun",
            "sea"
        ],
        [
            "598",
            "売る",
            "uru (うる)",
            "Verb, Godan verb, Transitive verb",
            "to sell"
        ],
        [
            "599",
            "煩い",
            "urusai (うるさい)",
            "い-adjective",
            "noisy, annoying"
        ],
        [
            "600",
            "後ろ",
            "ushiro (うしろ)",
            "Noun",
            "back; behind; rear"
        ],
        [
            "601",
            "薄い",
            "usui (うすい)",
            "Adjective, い-adjective",
            "thin; weak"
        ],
        [
            "602",
            "歌",
            "uta (うた)",
            "Noun",
            "song"
        ],
        [
            "603",
            "歌う",
            "utau (うたう)",
            "Verb, Godan verb, Intransitive verb, Transitive verb",
            "to sing"
        ],
        [
            "604",
            "上着",
            "uwagi (うわぎ)",
            "Noun",
            "coat; tunic; jacket; outer garment"
        ],
        [
            "605",
            "ワイシャツ",
            "wai shatsu",
            "Noun, Katakana, Wasei",
            "shirt"
        ],
        [
            "606",
            "若い",
            "wakai (わかい)",
            "Adjective, い-adjective",
            "young"
        ],
        [
            "607",
            "分かる",
            "wakaru (わかる)",
            "Verb, Godan verb, Intransitive verb",
            "to understand; to comprehend; to grasp; to see; to get; to follow"
        ],
        [
            "608",
            "悪い",
            "warui (わるい)",
            "Adjective, い-adjective",
            "bad; poor; undesirable"
        ],
        [
            "609",
            "忘れる",
            "wasureru (わすれる)",
            "Verb, Ichidan verb, Transitive verb",
            "to forget"
        ],
        [
            "610",
            "渡る",
            "wataru (わたる)",
            "Verb, Godan verb, Intransitive verb",
            "to go across"
        ],
        [
            "611",
            "私",
            "watashi (わたし)",
            "Pronoun",
            "I; myself"
        ],
        [
            "612",
            "渡す",
            "watasu (わたす)",
            "Verb, Godan verb, Transitive verb",
            "to hand over"
        ],
        [
            "613",
            "山",
            "yama (やま)",
            "Noun",
            "mountain; hill"
        ],
        [
            "614",
            "八百屋",
            "yaoya (やおや)",
            "Noun",
            "greengrocer; fruit and vegetable shop; versatile"
        ],
        [
            "615",
            "やる",
            "yaru",
            "Verb, Godan verb, Transitive verb",
            "to do"
        ],
        [
            "616",
            "野菜",
            "yasai (やさい)",
            "Noun",
            "vegetable"
        ],
        [
            "617",
            "易しい",
            "yasashii (やさしい)",
            "Adjective, い-adjective",
            "easy, simple"
        ],
        [
            "618",
            "安い",
            "yasui (やすい)",
            "Adjective, い-adjective",
            "cheap; inexpensive"
        ],
        [
            "619",
            "休み",
            "yasumi (やすみ)",
            "Noun",
            "rest; vacation; holiday"
        ],
        [
            "620",
            "休む",
            "yasumu (やすむ)",
            "Verb, Godan verb, Intransitive verb",
            "to be absent; to take a day off; to rest"
        ],
        [
            "621",
            "八つ",
            "yattsu (やっつ)",
            "Noun, Numeric",
            "eight: 8"
        ],
        [
            "622",
            "呼ぶ",
            "yobu (よぶ)",
            "Verb, Godan verb, Transitive verb",
            "to call out, to invite"
        ],
        [
            "623",
            "良い",
            "yoi/ii (よい/いい)",
            "Adjective, い-adjective",
            "good"
        ],
        [
            "624",
            "四日",
            "yokka (よっか)",
            "Noun",
            "fourth day of the month / 4 days"
        ],
        [
            "625",
            "横",
            "yoko (よこ)",
            "Noun",
            "beside,side,width"
        ],
        [
            "626",
            "よく",
            "yoku",
            "Adverb",
            "often, well"
        ],
        [
            "627",
            "読む",
            "yomu (よむ)",
            "Verb, Godan verb, Transitive verb",
            "to read; to guess; to predict; to read (someone's thoughts)"
        ],
        [
            "628",
            "夜",
            "yoru (よる)",
            "Noun",
            "evening; night"
        ],
        [
            "629",
            "四つ",
            "yotsu (よつ)",
            "Noun, Numeric",
            "four; 4"
        ],
        [
            "630",
            "洋服",
            "youfuku (ようふく)",
            "Noun",
            "western clothes"
        ],
        [
            "631",
            "八日",
            "youka (ようか)",
            "Noun",
            "eighth day of the month / 8 days"
        ],
        [
            "632",
            "弱い",
            "yowai (よわい)",
            "Adjective, い-adjective",
            "weak"
        ],
        [
            "633",
            "雪",
            "yuki (ゆき)",
            "Noun",
            "snow"
        ],
        [
            "634",
            "ゆっくり",
            "yukkuri",
            "Adverb",
            "slowly"
        ],
        [
            "635",
            "昨夜",
            "yuube (ゆうべ)",
            "Noun",
            "last night"
        ],
        [
            "636",
            "郵便局",
            "yuubinkyoku (ゆうびんきょく)",
            "Noun",
            "post office"
        ],
        [
            "637",
            "夕方",
            "yuugata (ゆうがた)",
            "Noun",
            "evening; dusk"
        ],
        [
            "638",
            "夕飯",
            "yuuhan (ゆうはん)",
            "Noun",
            "evening meal"
        ],
        [
            "639",
            "有名",
            "yuumei (ゆうめい)",
            "Noun, Adjective, な-adjective",
            "famous"
        ],
        [
            "640",
            "雑誌",
            "zasshi (ざっし)",
            "Noun",
            "magazine"
        ],
        [
            "641",
            "全部",
            "zenbu (ぜんぶ)",
            "Noun, Adverbial Noun",
            "all"
        ],
        [
            "642",
            "ゼロ",
            "zero",
            "Noun",
            "zero"
        ],
        [
            "643",
            "ズボン",
            "zubon",
            "Noun, Katakana",
            "trousers; pants"
        ]
    ];
}