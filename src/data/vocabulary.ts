import { Entry } from "../interface/entry";

// N5
export function getN5Vocabulary(): Array<Entry> {
    return [
        {
            "id": "n5_vocab_1",
            "japanese": "浴びる",
            "romanization": "abiru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to bathe, to shower",
            "alwaysShowHint": true,
            "kana": "あびる"
        },
        {
            "id": "n5_vocab_2",
            "japanese": "危ない",
            "romanization": "abunai",
            "category": "Adjective, い-adjective",
            "translation": "dangerous",
            "alwaysShowHint": true,
            "kana": "あぶない"
        },
        {
            "id": "n5_vocab_3",
            "japanese": "あっち",
            "romanization": "acchi",
            "category": "Pronoun",
            "translation": "over there",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_4",
            "japanese": "あちら",
            "romanization": "achira",
            "category": "Pronoun",
            "translation": "there",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_5",
            "japanese": "上げる",
            "romanization": "ageru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to raise; to elevate",
            "alwaysShowHint": true,
            "kana": "あげる"
        },
        {
            "id": "n5_vocab_6",
            "japanese": "赤",
            "romanization": "aka",
            "category": "Noun",
            "translation": "red; crimson; scarlet​",
            "alwaysShowHint": true,
            "kana": "あか"
        },
        {
            "id": "n5_vocab_7",
            "japanese": "赤い",
            "romanization": "akai",
            "category": "Adjective, い-adjective",
            "translation": "red; crimson; scarlet​",
            "alwaysShowHint": true,
            "kana": "あかい"
        },
        {
            "id": "n5_vocab_8",
            "japanese": "明るい",
            "romanization": "akarui",
            "category": "Adjective, い-adjective",
            "translation": "bright; light",
            "alwaysShowHint": true,
            "kana": "あかるい"
        },
        {
            "id": "n5_vocab_9",
            "japanese": "開ける",
            "romanization": "akeru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to open (a door, etc.); to unwrap (e.g. parcel, package); to unlock",
            "alwaysShowHint": true,
            "kana": "あける"
        },
        {
            "id": "n5_vocab_10",
            "japanese": "秋",
            "romanization": "aki",
            "category": "Noun",
            "translation": "autumn; fall",
            "alwaysShowHint": true,
            "kana": "あき"
        },
        {
            "id": "n5_vocab_11",
            "japanese": "開く",
            "romanization": "aku",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to open (e.g. doors, business, etc)",
            "alwaysShowHint": true,
            "kana": "あく"
        },
        {
            "id": "n5_vocab_12",
            "japanese": "甘い",
            "romanization": "amai",
            "category": "Adjective, い-adjective",
            "translation": "sweet; sweet-tasting; sugary; naive; indulgent",
            "alwaysShowHint": true,
            "kana": "あまい"
        },
        {
            "id": "n5_vocab_13",
            "japanese": "飴",
            "romanization": "ame",
            "category": "Noun",
            "translation": "candy",
            "alwaysShowHint": true,
            "kana": "あめ"
        },
        {
            "id": "n5_vocab_14",
            "japanese": "雨",
            "romanization": "ame",
            "category": "Noun",
            "translation": "rain",
            "alwaysShowHint": true,
            "kana": "あめ"
        },
        {
            "id": "n5_vocab_15",
            "japanese": "あなた",
            "romanization": "anata",
            "category": "Pronoun",
            "translation": "you",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_16",
            "japanese": "姉",
            "romanization": "ane",
            "category": "Noun",
            "translation": "older sister; elder sister​",
            "alwaysShowHint": true,
            "kana": "あね"
        },
        {
            "id": "n5_vocab_17",
            "japanese": "兄",
            "romanization": "ani",
            "category": "Noun",
            "translation": "elder brother; older brother​",
            "alwaysShowHint": true,
            "kana": "あに"
        },
        {
            "id": "n5_vocab_18",
            "japanese": "あの",
            "romanization": "ano",
            "category": "Pre-noun adjectival",
            "translation": "that",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_19",
            "japanese": "青",
            "romanization": "ao",
            "category": "Noun",
            "translation": "blue; azure",
            "alwaysShowHint": true,
            "kana": "あお"
        },
        {
            "id": "n5_vocab_20",
            "japanese": "青い",
            "romanization": "aoi",
            "category": "Adjective, い-adjective",
            "translation": "blue; azure",
            "alwaysShowHint": true,
            "kana": "あおい"
        },
        {
            "id": "n5_vocab_21",
            "japanese": "アパート",
            "romanization": "apaato",
            "category": "Noun, Katakana",
            "translation": "apartment",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_22",
            "japanese": "洗う",
            "romanization": "arau",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to wash",
            "alwaysShowHint": true,
            "kana": "あらう"
        },
        {
            "id": "n5_vocab_23",
            "japanese": "あれ",
            "romanization": "are",
            "category": "Pronoun",
            "translation": "that",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_24",
            "japanese": "ある",
            "romanization": "aru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to be, to have",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_25",
            "japanese": "歩く",
            "romanization": "aruku",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to walk",
            "alwaysShowHint": true,
            "kana": "あるく"
        },
        {
            "id": "n5_vocab_26",
            "japanese": "朝",
            "romanization": "asa",
            "category": "Noun",
            "translation": "morning",
            "alwaysShowHint": true,
            "kana": "あさ"
        },
        {
            "id": "n5_vocab_27",
            "japanese": "朝ご飯",
            "romanization": "asagohan",
            "category": "Noun",
            "translation": "breakfast",
            "alwaysShowHint": true,
            "kana": "あさごはん"
        },
        {
            "id": "n5_vocab_28",
            "japanese": "明後日",
            "romanization": "asatte",
            "category": "Noun",
            "translation": "day after tomorrow",
            "alwaysShowHint": true,
            "kana": "あさって"
        },
        {
            "id": "n5_vocab_29",
            "japanese": "足",
            "romanization": "ashi",
            "category": "Noun",
            "translation": "foot; leg; paw; arm",
            "alwaysShowHint": true,
            "kana": "あし"
        },
        {
            "id": "n5_vocab_30",
            "japanese": "明日",
            "romanization": "ashita",
            "category": "Noun, Temporal noun",
            "translation": "tomorrow",
            "alwaysShowHint": true,
            "kana": "あした"
        },
        {
            "id": "n5_vocab_31",
            "japanese": "遊ぶ",
            "romanization": "asobu",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to play; to enjoy oneself",
            "alwaysShowHint": true,
            "kana": "あそぶ"
        },
        {
            "id": "n5_vocab_32",
            "japanese": "あそこ",
            "romanization": "asoko",
            "category": "Pronoun",
            "translation": "over there",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_33",
            "japanese": "頭",
            "romanization": "atama",
            "category": "Noun",
            "translation": "head",
            "alwaysShowHint": true,
            "kana": "あたま"
        },
        {
            "id": "n5_vocab_34",
            "japanese": "新しい",
            "romanization": "atarashii",
            "category": "Adjective, い-adjective",
            "translation": "new; novel; fresh; recent; latest",
            "alwaysShowHint": true,
            "kana": "あたらしい"
        },
        {
            "id": "n5_vocab_35",
            "japanese": "暖かい",
            "romanization": "atatakai",
            "category": "Adjective, い-adjective",
            "translation": "warm",
            "alwaysShowHint": true,
            "kana": "あたたかい"
        },
        {
            "id": "n5_vocab_36",
            "japanese": "後",
            "romanization": "ato",
            "category": "Noun",
            "translation": "behind; after; remainder; left; also",
            "alwaysShowHint": true,
            "kana": "あと"
        },
        {
            "id": "n5_vocab_37",
            "japanese": "厚い",
            "romanization": "atsui",
            "category": "Adjective, い-adjective",
            "translation": "thick",
            "alwaysShowHint": true,
            "kana": "あつい"
        },
        {
            "id": "n5_vocab_38",
            "japanese": "暑い",
            "romanization": "atsui",
            "category": "Adjective, い-adjective",
            "translation": "hot; sultry",
            "alwaysShowHint": true,
            "kana": "あつい"
        },
        {
            "id": "n5_vocab_39",
            "japanese": "熱い",
            "romanization": "atsui",
            "category": "Adjective, い-adjective",
            "translation": "hot",
            "alwaysShowHint": true,
            "kana": "あつい"
        },
        {
            "id": "n5_vocab_40",
            "japanese": "会う",
            "romanization": "au",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to meet; to encounter; to see",
            "alwaysShowHint": true,
            "kana": "あう"
        },
        {
            "id": "n5_vocab_41",
            "japanese": "晩ご飯",
            "romanization": "bangohan",
            "category": "Noun",
            "translation": "dinner; evening meal",
            "alwaysShowHint": true,
            "kana": "ばんごはん"
        },
        {
            "id": "n5_vocab_42",
            "japanese": "番号",
            "romanization": "bangou",
            "category": "Noun",
            "translation": "number",
            "alwaysShowHint": true,
            "kana": "ばんごう"
        },
        {
            "id": "n5_vocab_43",
            "japanese": "バス",
            "romanization": "basu",
            "category": "Noun, Katakana",
            "translation": "bus",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_44",
            "japanese": "バター",
            "romanization": "bataa",
            "category": "Noun, Katakana",
            "translation": "butter​",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_45",
            "japanese": "ベッド",
            "romanization": "beddo",
            "category": "Noun, Katakana",
            "translation": "bed",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_46",
            "japanese": "勉強",
            "romanization": "benkyou",
            "category": "Noun, Suru verb",
            "translation": "to study",
            "alwaysShowHint": true,
            "kana": "べんきょう"
        },
        {
            "id": "n5_vocab_47",
            "japanese": "便利",
            "romanization": "benri",
            "category": "な-adjective",
            "translation": "convenient; handy; useful",
            "alwaysShowHint": true,
            "kana": "べんり"
        },
        {
            "id": "n5_vocab_48",
            "japanese": "ボールペン",
            "romanization": "boorupen",
            "category": "Noun, Katakana",
            "translation": "ball-point pen",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_49",
            "japanese": "ボタン",
            "romanization": "botan",
            "category": "Noun, Katakana",
            "translation": "button",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_50",
            "japanese": "帽子",
            "romanization": "boushi",
            "category": "Noun",
            "translation": "hat; cap",
            "alwaysShowHint": true,
            "kana": "ぼうし"
        },
        {
            "id": "n5_vocab_51",
            "japanese": "文章",
            "romanization": "bunshou",
            "category": "Noun",
            "translation": "sentence",
            "alwaysShowHint": true,
            "kana": "ぶんしょう"
        },
        {
            "id": "n5_vocab_52",
            "japanese": "豚肉",
            "romanization": "butaniku",
            "category": "Noun",
            "translation": "pork",
            "alwaysShowHint": true,
            "kana": "ぶたにく"
        },
        {
            "id": "n5_vocab_53",
            "japanese": "病院",
            "romanization": "byouin",
            "category": "Noun",
            "translation": "hospital",
            "alwaysShowHint": true,
            "kana": "びょういん"
        },
        {
            "id": "n5_vocab_54",
            "japanese": "病気",
            "romanization": "byouki",
            "category": "Noun",
            "translation": "illness; disease; sickness",
            "alwaysShowHint": true,
            "kana": "びょうき"
        },
        {
            "id": "n5_vocab_55",
            "japanese": "茶色",
            "romanization": "chairo",
            "category": "Noun",
            "translation": "brown",
            "alwaysShowHint": true,
            "kana": "ちゃいろ"
        },
        {
            "id": "n5_vocab_56",
            "japanese": "茶碗",
            "romanization": "chawan",
            "category": "Noun",
            "translation": "rice bowl; tea cup; teacup",
            "alwaysShowHint": true,
            "kana": "ちゃわん"
        },
        {
            "id": "n5_vocab_57",
            "japanese": "父",
            "romanization": "chichi",
            "category": "Noun",
            "translation": "father",
            "alwaysShowHint": true,
            "kana": "ちち"
        },
        {
            "id": "n5_vocab_58",
            "japanese": "違う",
            "romanization": "chigau",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to differ",
            "alwaysShowHint": true,
            "kana": "ちがう"
        },
        {
            "id": "n5_vocab_59",
            "japanese": "小さい",
            "romanization": "chiisai",
            "category": "Adjective, い-adjective",
            "translation": "small; little; tiny",
            "alwaysShowHint": true,
            "kana": "ちいさい"
        },
        {
            "id": "n5_vocab_60",
            "japanese": "小さな",
            "romanization": "chiisana",
            "category": "Pre-noun adjectival",
            "translation": "small; little; tiny​",
            "alwaysShowHint": true,
            "kana": "ちいさな"
        },
        {
            "id": "n5_vocab_61",
            "japanese": "近い",
            "romanization": "chikai",
            "category": "い-adjective",
            "translation": "near; close",
            "alwaysShowHint": true,
            "kana": "ちかい"
        },
        {
            "id": "n5_vocab_62",
            "japanese": "地下鉄",
            "romanization": "chikatetsu",
            "category": "Noun",
            "translation": "subway; underground train",
            "alwaysShowHint": true,
            "kana": "ちかてつ"
        },
        {
            "id": "n5_vocab_63",
            "japanese": "地図",
            "romanization": "chizu",
            "category": "Noun",
            "translation": "map",
            "alwaysShowHint": true,
            "kana": "ちず"
        },
        {
            "id": "n5_vocab_64",
            "japanese": "ちょっと",
            "romanization": "chotto",
            "category": "Adverb",
            "translation": "a little",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_65",
            "japanese": "丁度",
            "romanization": "choudo",
            "category": "Adverb",
            "translation": "exactly",
            "alwaysShowHint": true,
            "kana": "ちょうど"
        },
        {
            "id": "n5_vocab_66",
            "japanese": "台所",
            "romanization": "daidokoro",
            "category": "Noun",
            "translation": "kitchen",
            "alwaysShowHint": true,
            "kana": "だいどころ"
        },
        {
            "id": "n5_vocab_67",
            "japanese": "大学",
            "romanization": "daigaku",
            "category": "Noun",
            "translation": "university; college",
            "alwaysShowHint": true,
            "kana": "だいがく"
        },
        {
            "id": "n5_vocab_68",
            "japanese": "大丈夫",
            "romanization": "daijoubu",
            "category": "Adjective, な-adjective, Adverb",
            "translation": "OK; okay; alright; problem free",
            "alwaysShowHint": true,
            "kana": "だいじょうぶ"
        },
        {
            "id": "n5_vocab_69",
            "japanese": "大好き",
            "romanization": "daisuki",
            "category": "Adjective, な-adjective",
            "translation": "love; like; like very much",
            "alwaysShowHint": true,
            "kana": "だいすき"
        },
        {
            "id": "n5_vocab_70",
            "japanese": "だんだん",
            "romanization": "dandan",
            "category": "Adverb",
            "translation": "gradually",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_71",
            "japanese": "誰",
            "romanization": "dare",
            "category": "Pronoun",
            "translation": "who",
            "alwaysShowHint": true,
            "kana": "だれ"
        },
        {
            "id": "n5_vocab_72",
            "japanese": "誰か",
            "romanization": "dareka",
            "category": "Pronoun",
            "translation": "someone; somebody",
            "alwaysShowHint": true,
            "kana": "だれか"
        },
        {
            "id": "n5_vocab_73",
            "japanese": "出す",
            "romanization": "dasu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to take out; to get out; to put out; to reveal",
            "alwaysShowHint": true,
            "kana": "だす"
        },
        {
            "id": "n5_vocab_74",
            "japanese": "出口",
            "romanization": "deguchi",
            "category": "Noun",
            "translation": "exit; gateway; way out",
            "alwaysShowHint": true,
            "kana": "でぐち"
        },
        {
            "id": "n5_vocab_75",
            "japanese": "出かける",
            "romanization": "dekakeru",
            "category": "Verb, Ichidan verb, Intransitive verb",
            "translation": "to go out; to leave; to depart",
            "alwaysShowHint": true,
            "kana": "でかける"
        },
        {
            "id": "n5_vocab_76",
            "japanese": "電気",
            "romanization": "denki",
            "category": "Noun",
            "translation": "electricity",
            "alwaysShowHint": true,
            "kana": "でんき"
        },
        {
            "id": "n5_vocab_77",
            "japanese": "電車",
            "romanization": "densha",
            "category": "Noun",
            "translation": "train; electric train",
            "alwaysShowHint": true,
            "kana": "でんしゃ"
        },
        {
            "id": "n5_vocab_78",
            "japanese": "電話",
            "romanization": "denwa",
            "category": "Noun, Verb, Suru verb",
            "translation": "telephone (call / device)l; phone call",
            "alwaysShowHint": true,
            "kana": "でんわ"
        },
        {
            "id": "n5_vocab_79",
            "japanese": "デパート",
            "romanization": "depaato",
            "category": "Noun, Katakana",
            "translation": "department store",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_80",
            "japanese": "出る",
            "romanization": "deru",
            "category": "Verb, Ichidan verb, Intransitive verb",
            "translation": "to leave; to exit; to appear; to go out",
            "alwaysShowHint": true,
            "kana": "でる"
        },
        {
            "id": "n5_vocab_81",
            "japanese": "ドア",
            "romanization": "doa",
            "category": "Noun, Katakana",
            "translation": "door",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_82",
            "japanese": "どっち",
            "romanization": "docchi",
            "category": "Pronoun",
            "translation": "which; which one",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_83",
            "japanese": "どちら",
            "romanization": "dochira",
            "category": "Pronoun",
            "translation": "which of two",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_84",
            "japanese": "どこ",
            "romanization": "doko",
            "category": "Pronoun",
            "translation": "where; what place​",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_85",
            "japanese": "どなた",
            "romanization": "donata",
            "category": "Noun",
            "translation": "who",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_86",
            "japanese": "どの",
            "romanization": "dono",
            "category": "Pre-noun adjectival",
            "translation": "which",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_87",
            "japanese": "どれ",
            "romanization": "dore",
            "category": "Pronoun",
            "translation": "which (of three or more)​",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_88",
            "japanese": "どう",
            "romanization": "dou",
            "category": "Adverb",
            "translation": "how; in what way; how about​",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_89",
            "japanese": "動物",
            "romanization": "doubutsu",
            "category": "Noun",
            "translation": "animal",
            "alwaysShowHint": true,
            "kana": "どうぶつ"
        },
        {
            "id": "n5_vocab_90",
            "japanese": "どうも",
            "romanization": "doumo",
            "category": "Adverb",
            "translation": "thank you; thanks",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_91",
            "japanese": "どうぞ",
            "romanization": "douzo",
            "category": "Adverb",
            "translation": "please",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_92",
            "japanese": "土曜日",
            "romanization": "doyoubi",
            "category": "Noun",
            "translation": "Saturday",
            "alwaysShowHint": true,
            "kana": "どようび"
        },
        {
            "id": "n5_vocab_93",
            "japanese": "絵",
            "romanization": "e",
            "category": "Noun",
            "translation": "picture",
            "alwaysShowHint": true,
            "kana": "え"
        },
        {
            "id": "n5_vocab_94",
            "japanese": "ええ",
            "romanization": "ee",
            "category": "Noun",
            "translation": "yes; that is correct; right",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_95",
            "japanese": "映画",
            "romanization": "eiga",
            "category": "Noun",
            "translation": "movie; film",
            "alwaysShowHint": true,
            "kana": "えいが"
        },
        {
            "id": "n5_vocab_96",
            "japanese": "映画館",
            "romanization": "eigakan",
            "category": "Noun",
            "translation": "movie theater; cinema",
            "alwaysShowHint": true,
            "kana": "えいがかん"
        },
        {
            "id": "n5_vocab_97",
            "japanese": "英語",
            "romanization": "eigo",
            "category": "Noun",
            "translation": "English language",
            "alwaysShowHint": true,
            "kana": "えいご"
        },
        {
            "id": "n5_vocab_98",
            "japanese": "駅",
            "romanization": "eki",
            "category": "Noun",
            "translation": "station",
            "alwaysShowHint": true,
            "kana": "えき"
        },
        {
            "id": "n5_vocab_99",
            "japanese": "鉛筆",
            "romanization": "enpitsu",
            "category": "Noun",
            "translation": "pencil",
            "alwaysShowHint": true,
            "kana": "えんぴつ"
        },
        {
            "id": "n5_vocab_100",
            "japanese": "エレベーター",
            "romanization": "erebeetaa",
            "category": "Noun, Katakana",
            "translation": "elevator",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_101",
            "japanese": "フィルム",
            "romanization": "firumu",
            "category": "Noun, Katakana",
            "translation": "film",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_102",
            "japanese": "フォーク",
            "romanization": "fooku",
            "category": "Noun, Katakana",
            "translation": "fork",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_103",
            "japanese": "吹く",
            "romanization": "fuku",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to blow (of the wind)",
            "alwaysShowHint": true,
            "kana": "ふく"
        },
        {
            "id": "n5_vocab_104",
            "japanese": "服",
            "romanization": "fuku",
            "category": "Noun",
            "translation": "clothes",
            "alwaysShowHint": true,
            "kana": "ふく"
        },
        {
            "id": "n5_vocab_105",
            "japanese": "降る",
            "romanization": "furu",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to fall",
            "alwaysShowHint": true,
            "kana": "ふる"
        },
        {
            "id": "n5_vocab_106",
            "japanese": "古い",
            "romanization": "furui",
            "category": "い-adjective",
            "translation": "old (not used for people)",
            "alwaysShowHint": true,
            "kana": "ふるい"
        },
        {
            "id": "n5_vocab_107",
            "japanese": "二人",
            "romanization": "futari",
            "category": "Noun",
            "translation": "two people; pair; couple",
            "alwaysShowHint": true,
            "kana": "ふたり"
        },
        {
            "id": "n5_vocab_108",
            "japanese": "二つ",
            "romanization": "futatsu",
            "category": "Noun, Numeric",
            "translation": "two; 2",
            "alwaysShowHint": true,
            "kana": "ふたつ"
        },
        {
            "id": "n5_vocab_109",
            "japanese": "太い",
            "romanization": "futoi",
            "category": "Adjective, い-adjective",
            "translation": "fat; thick",
            "alwaysShowHint": true,
            "kana": "ふとい"
        },
        {
            "id": "n5_vocab_110",
            "japanese": "二日",
            "romanization": "futsuka",
            "category": "Noun",
            "translation": "the second day of the month / 2 days",
            "alwaysShowHint": true,
            "kana": "ふつか"
        },
        {
            "id": "n5_vocab_111",
            "japanese": "封筒",
            "romanization": "fuutou",
            "category": "Noun",
            "translation": "envelope",
            "alwaysShowHint": true,
            "kana": "ふうとう"
        },
        {
            "id": "n5_vocab_112",
            "japanese": "冬",
            "romanization": "fuyu",
            "category": "Noun",
            "translation": "winter",
            "alwaysShowHint": true,
            "kana": "ふゆ"
        },
        {
            "id": "n5_vocab_113",
            "japanese": "外国",
            "romanization": "gaikoku",
            "category": "Noun",
            "translation": "foreign country",
            "alwaysShowHint": true,
            "kana": "がいこく"
        },
        {
            "id": "n5_vocab_114",
            "japanese": "外国人",
            "romanization": "gaikokujin",
            "category": "Noun",
            "translation": "foreigner; foreign citizen; foreign national; alien; non-Japanese",
            "alwaysShowHint": true,
            "kana": "がいこくじん"
        },
        {
            "id": "n5_vocab_115",
            "japanese": "学校",
            "romanization": "gakkou",
            "category": "Noun",
            "translation": "school",
            "alwaysShowHint": true,
            "kana": "がっこう"
        },
        {
            "id": "n5_vocab_116",
            "japanese": "学生",
            "romanization": "gakusei",
            "category": "Noun",
            "translation": "student",
            "alwaysShowHint": true,
            "kana": "がくせい"
        },
        {
            "id": "n5_vocab_117",
            "japanese": "玄関",
            "romanization": "genkan",
            "category": "Noun",
            "translation": "entrance",
            "alwaysShowHint": true,
            "kana": "げんかん"
        },
        {
            "id": "n5_vocab_118",
            "japanese": "元気",
            "romanization": "genki",
            "category": "Noun, Adjective, な-adjective",
            "translation": "lively; full of spirit; energetic; healthy",
            "alwaysShowHint": true,
            "kana": "げんき"
        },
        {
            "id": "n5_vocab_119",
            "japanese": "月曜日",
            "romanization": "getsuyoubi",
            "category": "Noun",
            "translation": "Monday",
            "alwaysShowHint": true,
            "kana": "げつようび"
        },
        {
            "id": "n5_vocab_120",
            "japanese": "銀行",
            "romanization": "ginkou",
            "category": "Noun",
            "translation": "bank",
            "alwaysShowHint": true,
            "kana": "ぎんこう"
        },
        {
            "id": "n5_vocab_121",
            "japanese": "ギター",
            "romanization": "gitaa",
            "category": "Noun, Katakana",
            "translation": "guitar",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_122",
            "japanese": "五",
            "romanization": "go",
            "category": "Noun, Numeric",
            "translation": "five; 5",
            "alwaysShowHint": true,
            "kana": "ご"
        },
        {
            "id": "n5_vocab_123",
            "japanese": "午後",
            "romanization": "gogo",
            "category": "Noun",
            "translation": "afternoon; p.m.",
            "alwaysShowHint": true,
            "kana": "ごご"
        },
        {
            "id": "n5_vocab_124",
            "japanese": "ご飯",
            "romanization": "gohan",
            "category": "Noun",
            "translation": "cooked rice, meal",
            "alwaysShowHint": true,
            "kana": "ごはん"
        },
        {
            "id": "n5_vocab_125",
            "japanese": "午前",
            "romanization": "gozen",
            "category": "Noun",
            "translation": "morning; a.m.",
            "alwaysShowHint": true,
            "kana": "ごぜん"
        },
        {
            "id": "n5_vocab_126",
            "japanese": "グラム",
            "romanization": "guramu",
            "category": "Noun, Katakana",
            "translation": "gram",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_127",
            "japanese": "牛肉",
            "romanization": "gyuuniku",
            "category": "Noun",
            "translation": "beef",
            "alwaysShowHint": true,
            "kana": "ぎゅうにく"
        },
        {
            "id": "n5_vocab_128",
            "japanese": "牛乳",
            "romanization": "gyuunyuu",
            "category": "Noun",
            "translation": "(cow's) milk",
            "alwaysShowHint": true,
            "kana": "ぎゅうにゅう"
        },
        {
            "id": "n5_vocab_129",
            "japanese": "歯",
            "romanization": "ha",
            "category": "Noun",
            "translation": "tooth",
            "alwaysShowHint": true,
            "kana": "は"
        },
        {
            "id": "n5_vocab_130",
            "japanese": "八",
            "romanization": "hachi",
            "category": "Noun, Numeric",
            "translation": "eight: 8",
            "alwaysShowHint": true,
            "kana": "はち"
        },
        {
            "id": "n5_vocab_131",
            "japanese": "葉書",
            "romanization": "hagaki",
            "category": "Noun",
            "translation": "postcard",
            "alwaysShowHint": true,
            "kana": "はがき"
        },
        {
            "id": "n5_vocab_132",
            "japanese": "母",
            "romanization": "haha",
            "category": "Noun",
            "translation": "mother",
            "alwaysShowHint": true,
            "kana": "はは"
        },
        {
            "id": "n5_vocab_133",
            "japanese": "はい",
            "romanization": "hai",
            "category": "Noun",
            "translation": "yes; that is correct​",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_134",
            "japanese": "入る",
            "romanization": "hairu",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to enter; to go into",
            "alwaysShowHint": true,
            "kana": "はいる"
        },
        {
            "id": "n5_vocab_135",
            "japanese": "灰皿",
            "romanization": "haizara",
            "category": "Noun",
            "translation": "ashtray",
            "alwaysShowHint": true,
            "kana": "はいざら"
        },
        {
            "id": "n5_vocab_136",
            "japanese": "始まる",
            "romanization": "hajimaru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to begin",
            "alwaysShowHint": true,
            "kana": "はじまる"
        },
        {
            "id": "n5_vocab_137",
            "japanese": "初めて",
            "romanization": "hajimete",
            "category": "Adverb",
            "translation": "for the first time",
            "alwaysShowHint": true,
            "kana": "はじめて"
        },
        {
            "id": "n5_vocab_138",
            "japanese": "箱",
            "romanization": "hako",
            "category": "Noun",
            "translation": "box; crate",
            "alwaysShowHint": true,
            "kana": "はこ"
        },
        {
            "id": "n5_vocab_139",
            "japanese": "履く",
            "romanization": "haku",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to wear, to put on trousers",
            "alwaysShowHint": true,
            "kana": "はく"
        },
        {
            "id": "n5_vocab_140",
            "japanese": "半",
            "romanization": "han",
            "category": "Noun",
            "translation": "half; semi-; half-past",
            "alwaysShowHint": true,
            "kana": "はん"
        },
        {
            "id": "n5_vocab_141",
            "japanese": "花",
            "romanization": "hana",
            "category": "Noun",
            "translation": "flower",
            "alwaysShowHint": true,
            "kana": "はな"
        },
        {
            "id": "n5_vocab_142",
            "japanese": "鼻",
            "romanization": "hana",
            "category": "Noun",
            "translation": "nose",
            "alwaysShowHint": true,
            "kana": "はな"
        },
        {
            "id": "n5_vocab_143",
            "japanese": "話",
            "romanization": "hanashi",
            "category": "Noun",
            "translation": "talk; speech; chat; conversation​",
            "alwaysShowHint": true,
            "kana": "はなし"
        },
        {
            "id": "n5_vocab_144",
            "japanese": "話す",
            "romanization": "hanasu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to speak; to talk; to converse",
            "alwaysShowHint": true,
            "kana": "はなす"
        },
        {
            "id": "n5_vocab_145",
            "japanese": "半分",
            "romanization": "hanbun",
            "category": "Noun",
            "translation": "half",
            "alwaysShowHint": true,
            "kana": "はんぶん"
        },
        {
            "id": "n5_vocab_146",
            "japanese": "ハンカチ",
            "romanization": "hankachi",
            "category": "Noun, Katakana",
            "translation": "handkerchief​",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_147",
            "japanese": "晴れ",
            "romanization": "hare",
            "category": "Noun",
            "translation": "clear weather",
            "alwaysShowHint": true,
            "kana": "はれ"
        },
        {
            "id": "n5_vocab_148",
            "japanese": "晴れる",
            "romanization": "hareru",
            "category": "Verb, Ichidan verb, Intransitive verb",
            "translation": "to be sunny",
            "alwaysShowHint": true,
            "kana": "はれる"
        },
        {
            "id": "n5_vocab_149",
            "japanese": "貼る",
            "romanization": "haru",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to stick; to paste",
            "alwaysShowHint": true,
            "kana": "はる"
        },
        {
            "id": "n5_vocab_150",
            "japanese": "春",
            "romanization": "haru",
            "category": "Noun",
            "translation": "spring; springtime",
            "alwaysShowHint": true,
            "kana": "はる"
        },
        {
            "id": "n5_vocab_151",
            "japanese": "箸",
            "romanization": "hashi",
            "category": "Noun",
            "translation": "chopsticks",
            "alwaysShowHint": true,
            "kana": "はし"
        },
        {
            "id": "n5_vocab_152",
            "japanese": "橋",
            "romanization": "hashi",
            "category": "Noun",
            "translation": "bridge",
            "alwaysShowHint": true,
            "kana": "はし"
        },
        {
            "id": "n5_vocab_153",
            "japanese": "走る",
            "romanization": "hashiru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to run",
            "alwaysShowHint": true,
            "kana": "はしる"
        },
        {
            "id": "n5_vocab_154",
            "japanese": "二十歳",
            "romanization": "hatachi",
            "category": "Noun",
            "translation": "20 years old; twenty years old",
            "alwaysShowHint": true,
            "kana": "はたち"
        },
        {
            "id": "n5_vocab_155",
            "japanese": "働く",
            "romanization": "hataraku",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to work",
            "alwaysShowHint": true,
            "kana": "はたらく"
        },
        {
            "id": "n5_vocab_156",
            "japanese": "二十日",
            "romanization": "hatsuka",
            "category": "Noun",
            "translation": "twentieth day of the month / 20 days",
            "alwaysShowHint": true,
            "kana": "はつか"
        },
        {
            "id": "n5_vocab_157",
            "japanese": "早い",
            "romanization": "hayai",
            "category": "Adjective, い-adjective",
            "translation": "fast; early",
            "alwaysShowHint": true,
            "kana": "はやい"
        },
        {
            "id": "n5_vocab_158",
            "japanese": "速い",
            "romanization": "hayai",
            "category": "Adjective, い-adjective",
            "translation": "fast; quick; hasty; brisk",
            "alwaysShowHint": true,
            "kana": "はやい"
        },
        {
            "id": "n5_vocab_159",
            "japanese": "辺",
            "romanization": "hen",
            "category": "Noun",
            "translation": "area",
            "alwaysShowHint": true,
            "kana": "へん"
        },
        {
            "id": "n5_vocab_160",
            "japanese": "下手",
            "romanization": "heta",
            "category": "Noun, Adjective, な-adjective",
            "translation": "unskillful; poor; awkward​",
            "alwaysShowHint": true,
            "kana": "へた"
        },
        {
            "id": "n5_vocab_161",
            "japanese": "部屋",
            "romanization": "heya",
            "category": "Noun",
            "translation": "room",
            "alwaysShowHint": true,
            "kana": "へや"
        },
        {
            "id": "n5_vocab_162",
            "japanese": "左",
            "romanization": "hidari",
            "category": "Noun",
            "translation": "left; left hand side",
            "alwaysShowHint": true,
            "kana": "ひだり"
        },
        {
            "id": "n5_vocab_163",
            "japanese": "東",
            "romanization": "higashi",
            "category": "Noun",
            "translation": "east",
            "alwaysShowHint": true,
            "kana": "ひがし"
        },
        {
            "id": "n5_vocab_164",
            "japanese": "飛行機",
            "romanization": "hikouki",
            "category": "Noun",
            "translation": "airplane; aircraft",
            "alwaysShowHint": true,
            "kana": "ひこうき"
        },
        {
            "id": "n5_vocab_165",
            "japanese": "引く",
            "romanization": "hiku",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to pull",
            "alwaysShowHint": true,
            "kana": "ひく"
        },
        {
            "id": "n5_vocab_166",
            "japanese": "弾く",
            "romanization": "hiku",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to play",
            "alwaysShowHint": true,
            "kana": "ひく"
        },
        {
            "id": "n5_vocab_167",
            "japanese": "低い",
            "romanization": "hikui",
            "category": "い-adjective",
            "translation": "short,low",
            "alwaysShowHint": true,
            "kana": "ひくい"
        },
        {
            "id": "n5_vocab_168",
            "japanese": "暇",
            "romanization": "hima",
            "category": "Noun, な-adjective",
            "translation": "free time",
            "alwaysShowHint": true,
            "kana": "ひま"
        },
        {
            "id": "n5_vocab_169",
            "japanese": "広い",
            "romanization": "hiroi",
            "category": "い-adjective",
            "translation": "spacious; vast; wide",
            "alwaysShowHint": true,
            "kana": "ひろい"
        },
        {
            "id": "n5_vocab_170",
            "japanese": "昼",
            "romanization": "hiru",
            "category": "Noun",
            "translation": "noon; midday; daytime; lunch",
            "alwaysShowHint": true,
            "kana": "ひる"
        },
        {
            "id": "n5_vocab_171",
            "japanese": "昼ご飯",
            "romanization": "hirugohan",
            "category": "Noun",
            "translation": "lunch",
            "alwaysShowHint": true,
            "kana": "ひるごはん"
        },
        {
            "id": "n5_vocab_172",
            "japanese": "人",
            "romanization": "hito",
            "category": "Noun",
            "translation": "person; human",
            "alwaysShowHint": true,
            "kana": "ひと"
        },
        {
            "id": "n5_vocab_173",
            "japanese": "一人",
            "romanization": "hitori",
            "category": "Noun, Adverb",
            "translation": "one person​; alone; single",
            "alwaysShowHint": true,
            "kana": "ひとり"
        },
        {
            "id": "n5_vocab_174",
            "japanese": "一つ",
            "romanization": "hitotsu",
            "category": "Noun, Numeric",
            "translation": "one thing; only",
            "alwaysShowHint": true,
            "kana": "ひとつ"
        },
        {
            "id": "n5_vocab_175",
            "japanese": "ほか",
            "romanization": "hoka",
            "category": "Noun",
            "translation": "other (place, thing, person); the rest",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_176",
            "japanese": "本",
            "romanization": "hon",
            "category": "Noun",
            "translation": "book; volume; script",
            "alwaysShowHint": true,
            "kana": "ほん"
        },
        {
            "id": "n5_vocab_177",
            "japanese": "本棚",
            "romanization": "hondana",
            "category": "Noun",
            "translation": "bookshelf; bookcase",
            "alwaysShowHint": true,
            "kana": "ほんだな"
        },
        {
            "id": "n5_vocab_178",
            "japanese": "本当",
            "romanization": "hontou",
            "category": "Noun, Adjective, な-adjective",
            "translation": "truth; reality; actuality; fact",
            "alwaysShowHint": true,
            "kana": "ほんとう"
        },
        {
            "id": "n5_vocab_179",
            "japanese": "欲しい",
            "romanization": "hoshii",
            "category": "Adjective, い-adjective",
            "translation": "want",
            "alwaysShowHint": true,
            "kana": "ほしい"
        },
        {
            "id": "n5_vocab_180",
            "japanese": "細い",
            "romanization": "hosoi",
            "category": "Adjective, い-adjective",
            "translation": "thin; slender",
            "alwaysShowHint": true,
            "kana": "ほそい"
        },
        {
            "id": "n5_vocab_181",
            "japanese": "ホテル",
            "romanization": "hoteru",
            "category": "Noun, Katakana",
            "translation": "hotel",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_182",
            "japanese": "百",
            "romanization": "hyaku",
            "category": "Noun, Numeric",
            "translation": "100; hundred",
            "alwaysShowHint": true,
            "kana": "ひゃく"
        },
        {
            "id": "n5_vocab_183",
            "japanese": "一",
            "romanization": "ichi",
            "category": "Noun, Prefix, Suffix, Numeric",
            "translation": "one; best; first; foremost; start",
            "alwaysShowHint": true,
            "kana": "いち"
        },
        {
            "id": "n5_vocab_184",
            "japanese": "一番",
            "romanization": "ichiban",
            "category": "Noun, Adverb",
            "translation": "number one; first; 1st, first place​; best; most​",
            "alwaysShowHint": true,
            "kana": "いちばん"
        },
        {
            "id": "n5_vocab_185",
            "japanese": "一日",
            "romanization": "ichinichi",
            "category": "Noun, Adverbial Noun",
            "translation": "one day, all day",
            "alwaysShowHint": true,
            "kana": "いちにち"
        },
        {
            "id": "n5_vocab_186",
            "japanese": "家",
            "romanization": "ie",
            "category": "Noun",
            "translation": "house, residence, family",
            "alwaysShowHint": true,
            "kana": "いえ"
        },
        {
            "id": "n5_vocab_187",
            "japanese": "いかが",
            "romanization": "ikaga",
            "category": "Adverb",
            "translation": "how; in what way; how about​",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_188",
            "japanese": "池",
            "romanization": "ike",
            "category": "Noun",
            "translation": "pond",
            "alwaysShowHint": true,
            "kana": "いけ"
        },
        {
            "id": "n5_vocab_189",
            "japanese": "行く",
            "romanization": "iku",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to go; to move",
            "alwaysShowHint": true,
            "kana": "いく"
        },
        {
            "id": "n5_vocab_190",
            "japanese": "いくら",
            "romanization": "ikura",
            "category": "Noun, Adverb",
            "translation": "how much?; how many?​",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_191",
            "japanese": "いくつ",
            "romanization": "ikutsu",
            "category": "Adverb",
            "translation": "how many?,how old?",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_192",
            "japanese": "今",
            "romanization": "ima",
            "category": "Noun, Adverbial Noun",
            "translation": "now; the present time; soon",
            "alwaysShowHint": true,
            "kana": "いま"
        },
        {
            "id": "n5_vocab_193",
            "japanese": "意味",
            "romanization": "imi",
            "category": "Noun",
            "translation": "meaning; significance; sense",
            "alwaysShowHint": true,
            "kana": "いみ"
        },
        {
            "id": "n5_vocab_194",
            "japanese": "妹",
            "romanization": "imouto",
            "category": "Noun",
            "translation": "younger sister",
            "alwaysShowHint": true,
            "kana": "いもうと"
        },
        {
            "id": "n5_vocab_195",
            "japanese": "犬",
            "romanization": "inu",
            "category": "Noun",
            "translation": "dog",
            "alwaysShowHint": true,
            "kana": "いぬ"
        },
        {
            "id": "n5_vocab_196",
            "japanese": "入れる",
            "romanization": "ireru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to put in; to let in; to take in; to bring in; to insert; to install",
            "alwaysShowHint": true,
            "kana": "いれる"
        },
        {
            "id": "n5_vocab_197",
            "japanese": "入口",
            "romanization": "iriguchi",
            "category": "Noun",
            "translation": "entrance; entry; gate",
            "alwaysShowHint": true,
            "kana": "いりぐち"
        },
        {
            "id": "n5_vocab_198",
            "japanese": "色",
            "romanization": "iro",
            "category": "Noun",
            "translation": "colour; color",
            "alwaysShowHint": true,
            "kana": "いろ"
        },
        {
            "id": "n5_vocab_199",
            "japanese": "色々",
            "romanization": "iroiro",
            "category": "Noun, Adjective, な-adjective, Adverb",
            "translation": "various",
            "alwaysShowHint": true,
            "kana": "いろいろ"
        },
        {
            "id": "n5_vocab_200",
            "japanese": "居る",
            "romanization": "iru",
            "category": "Verb, Ichidan verb, Intransitive verb",
            "translation": "to be, to have",
            "alwaysShowHint": true,
            "kana": "いる"
        },
        {
            "id": "n5_vocab_201",
            "japanese": "要る",
            "romanization": "iru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to be needed",
            "alwaysShowHint": true,
            "kana": "いる"
        },
        {
            "id": "n5_vocab_202",
            "japanese": "医者",
            "romanization": "isha",
            "category": "Noun",
            "translation": "(medical) doctor; physician",
            "alwaysShowHint": true,
            "kana": "いしゃ"
        },
        {
            "id": "n5_vocab_203",
            "japanese": "忙しい",
            "romanization": "isogashii",
            "category": "Adjective, い-adjective",
            "translation": "busy",
            "alwaysShowHint": true,
            "kana": "いそがしい"
        },
        {
            "id": "n5_vocab_204",
            "japanese": "一緒",
            "romanization": "issho",
            "category": "Noun",
            "translation": "together; at the same time; same; identical",
            "alwaysShowHint": true,
            "kana": "いっしょ"
        },
        {
            "id": "n5_vocab_205",
            "japanese": "椅子",
            "romanization": "isu",
            "category": "Noun",
            "translation": "chair",
            "alwaysShowHint": true,
            "kana": "いす"
        },
        {
            "id": "n5_vocab_206",
            "japanese": "痛い",
            "romanization": "itai",
            "category": "Adjective, い-adjective",
            "translation": "painful; sore​",
            "alwaysShowHint": true,
            "kana": "いたい"
        },
        {
            "id": "n5_vocab_207",
            "japanese": "いつ",
            "romanization": "itsu",
            "category": "Pronoun",
            "translation": "when",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_208",
            "japanese": "五日",
            "romanization": "itsuka",
            "category": "Noun",
            "translation": "the fifth day of the month / 5 days",
            "alwaysShowHint": true,
            "kana": "いつか"
        },
        {
            "id": "n5_vocab_209",
            "japanese": "五つ",
            "romanization": "itsutsu",
            "category": "Noun, Numeric",
            "translation": "five; 5",
            "alwaysShowHint": true,
            "kana": "いつつ"
        },
        {
            "id": "n5_vocab_210",
            "japanese": "言う",
            "romanization": "iu",
            "category": "Verb, Godan verb",
            "translation": "to say; to call",
            "alwaysShowHint": true,
            "kana": "いう"
        },
        {
            "id": "n5_vocab_211",
            "japanese": "嫌",
            "romanization": "iya",
            "category": "Noun, Adjective, な-adjective",
            "translation": "unpleasant",
            "alwaysShowHint": true,
            "kana": "いや"
        },
        {
            "id": "n5_vocab_212",
            "japanese": "じゃあ",
            "romanization": "jaa",
            "category": "Conjunction",
            "translation": "then; well; so; well then",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_213",
            "japanese": "字引",
            "romanization": "jibiki",
            "category": "Noun",
            "translation": "dictionary",
            "alwaysShowHint": true,
            "kana": "じびき"
        },
        {
            "id": "n5_vocab_214",
            "japanese": "自分",
            "romanization": "jibun",
            "category": "Pronoun",
            "translation": "myself; yourself; oneself; himself; herself; i; me",
            "alwaysShowHint": true,
            "kana": "じぶん"
        },
        {
            "id": "n5_vocab_215",
            "japanese": "自動車",
            "romanization": "jidousha",
            "category": "Noun",
            "translation": "automobile; motorcar; motor vehicle; car",
            "alwaysShowHint": true,
            "kana": "じどうしゃ"
        },
        {
            "id": "n5_vocab_216",
            "japanese": "時間",
            "romanization": "jikan",
            "category": "Noun",
            "translation": "time; hour(s)",
            "alwaysShowHint": true,
            "kana": "じかん"
        },
        {
            "id": "n5_vocab_217",
            "japanese": "辞書",
            "romanization": "jisho",
            "category": "Noun",
            "translation": "dictionary",
            "alwaysShowHint": true,
            "kana": "じしょ"
        },
        {
            "id": "n5_vocab_218",
            "japanese": "自転車",
            "romanization": "jitensha",
            "category": "Noun",
            "translation": "bicycle",
            "alwaysShowHint": true,
            "kana": "じてんしゃ"
        },
        {
            "id": "n5_vocab_219",
            "japanese": "丈夫",
            "romanization": "joubu",
            "category": "Adjective, な-adjective",
            "translation": "strong, durable",
            "alwaysShowHint": true,
            "kana": "じょうぶ"
        },
        {
            "id": "n5_vocab_220",
            "japanese": "上手",
            "romanization": "jouzu",
            "category": "Noun, Adjective, な-adjective",
            "translation": "skillful; skilled; proficient; good (at)",
            "alwaysShowHint": true,
            "kana": "じょうず"
        },
        {
            "id": "n5_vocab_221",
            "japanese": "授業",
            "romanization": "jugyou",
            "category": "Noun, Suru verb",
            "translation": "lesson; class work",
            "alwaysShowHint": true,
            "kana": "じゅぎょう"
        },
        {
            "id": "n5_vocab_222",
            "japanese": "十",
            "romanization": "juu",
            "category": "Noun, Numeric",
            "translation": "ten; 10",
            "alwaysShowHint": true,
            "kana": "じゅう"
        },
        {
            "id": "n5_vocab_223",
            "japanese": "かばん",
            "romanization": "kaban",
            "category": "Noun",
            "translation": "bag; basket​",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_224",
            "japanese": "花瓶",
            "romanization": "kabin",
            "category": "Noun",
            "translation": "a vase",
            "alwaysShowHint": true,
            "kana": "かびん"
        },
        {
            "id": "n5_vocab_225",
            "japanese": "角",
            "romanization": "kado",
            "category": "Noun",
            "translation": "a corner; angle​",
            "alwaysShowHint": true,
            "kana": "かど"
        },
        {
            "id": "n5_vocab_226",
            "japanese": "帰る",
            "romanization": "kaeru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to go back​",
            "alwaysShowHint": true,
            "kana": "かえる"
        },
        {
            "id": "n5_vocab_227",
            "japanese": "返す",
            "romanization": "kaesu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to return something",
            "alwaysShowHint": true,
            "kana": "かえす"
        },
        {
            "id": "n5_vocab_228",
            "japanese": "鍵",
            "romanization": "kagi",
            "category": "Noun",
            "translation": "key",
            "alwaysShowHint": true,
            "kana": "かぎ"
        },
        {
            "id": "n5_vocab_229",
            "japanese": "階段",
            "romanization": "kaidan",
            "category": "Noun",
            "translation": "stairs; stairway; staircase",
            "alwaysShowHint": true,
            "kana": "かいだん"
        },
        {
            "id": "n5_vocab_230",
            "japanese": "買い物",
            "romanization": "kaimono",
            "category": "Noun",
            "translation": "shopping; purchased goods",
            "alwaysShowHint": true,
            "kana": "かいもの"
        },
        {
            "id": "n5_vocab_231",
            "japanese": "会社",
            "romanization": "kaisha",
            "category": "Noun",
            "translation": "company; corporation",
            "alwaysShowHint": true,
            "kana": "かいしゃ"
        },
        {
            "id": "n5_vocab_232",
            "japanese": "掛かる",
            "romanization": "kakaru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to take (a resource, e.g. time or money)",
            "alwaysShowHint": true,
            "kana": "かかる"
        },
        {
            "id": "n5_vocab_233",
            "japanese": "掛ける",
            "romanization": "kakeru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to hang up; to make (a call)​;",
            "alwaysShowHint": true,
            "kana": "かける"
        },
        {
            "id": "n5_vocab_234",
            "japanese": "書く",
            "romanization": "kaku",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to write; to compose; to pen; to draw",
            "alwaysShowHint": true,
            "kana": "かく"
        },
        {
            "id": "n5_vocab_235",
            "japanese": "カメラ",
            "romanization": "kamera",
            "category": "Noun, Katakana",
            "translation": "camera",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_236",
            "japanese": "紙",
            "romanization": "kami",
            "category": "Noun",
            "translation": "paper",
            "alwaysShowHint": true,
            "kana": "かみ"
        },
        {
            "id": "n5_vocab_237",
            "japanese": "漢字",
            "romanization": "kanji",
            "category": "Noun",
            "translation": "kanji",
            "alwaysShowHint": true,
            "kana": "かんじ"
        },
        {
            "id": "n5_vocab_238",
            "japanese": "カップ",
            "romanization": "kappu",
            "category": "Noun, Katakana",
            "translation": "cup",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_239",
            "japanese": "体",
            "romanization": "karada",
            "category": "Noun",
            "translation": "body",
            "alwaysShowHint": true,
            "kana": "からだ"
        },
        {
            "id": "n5_vocab_240",
            "japanese": "辛い",
            "romanization": "karai",
            "category": "い-adjective",
            "translation": "spicy",
            "alwaysShowHint": true,
            "kana": "からい"
        },
        {
            "id": "n5_vocab_241",
            "japanese": "カレー",
            "romanization": "karee",
            "category": "Noun, Katakana",
            "translation": "curry",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_242",
            "japanese": "カレンダー",
            "romanization": "karendaa",
            "category": "Noun, Katakana",
            "translation": "calendar",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_243",
            "japanese": "借りる",
            "romanization": "kariru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to borrow",
            "alwaysShowHint": true,
            "kana": "かりる"
        },
        {
            "id": "n5_vocab_244",
            "japanese": "軽い",
            "romanization": "karui",
            "category": "Adjective, い-adjective",
            "translation": "light",
            "alwaysShowHint": true,
            "kana": "かるい"
        },
        {
            "id": "n5_vocab_245",
            "japanese": "傘",
            "romanization": "kasa",
            "category": "Noun",
            "translation": "umbrella",
            "alwaysShowHint": true,
            "kana": "かさ"
        },
        {
            "id": "n5_vocab_246",
            "japanese": "貸す",
            "romanization": "kasu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to lend; to loan",
            "alwaysShowHint": true,
            "kana": "かす"
        },
        {
            "id": "n5_vocab_247",
            "japanese": "方",
            "romanization": "kata",
            "category": "Noun",
            "translation": "way of doing something",
            "alwaysShowHint": true,
            "kana": "かた"
        },
        {
            "id": "n5_vocab_248",
            "japanese": "家庭",
            "romanization": "katei",
            "category": "Noun",
            "translation": "household",
            "alwaysShowHint": true,
            "kana": "かてい"
        },
        {
            "id": "n5_vocab_249",
            "japanese": "買う",
            "romanization": "kau",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to buy; to purchase",
            "alwaysShowHint": true,
            "kana": "かう"
        },
        {
            "id": "n5_vocab_250",
            "japanese": "川",
            "romanization": "kawa",
            "category": "Noun, Suffix",
            "translation": "river; stream​",
            "alwaysShowHint": true,
            "kana": "かわ"
        },
        {
            "id": "n5_vocab_251",
            "japanese": "可愛い",
            "romanization": "kawaii",
            "category": "Adjective, い-adjective",
            "translation": "cute",
            "alwaysShowHint": true,
            "kana": "かわいい"
        },
        {
            "id": "n5_vocab_252",
            "japanese": "火曜日",
            "romanization": "kayoubi",
            "category": "Noun",
            "translation": "Tuesday",
            "alwaysShowHint": true,
            "kana": "かようび"
        },
        {
            "id": "n5_vocab_253",
            "japanese": "風邪",
            "romanization": "kaze",
            "category": "Noun",
            "translation": "a cold",
            "alwaysShowHint": true,
            "kana": "かぜ"
        },
        {
            "id": "n5_vocab_254",
            "japanese": "風",
            "romanization": "kaze",
            "category": "Noun",
            "translation": "wind",
            "alwaysShowHint": true,
            "kana": "かぜ"
        },
        {
            "id": "n5_vocab_255",
            "japanese": "家族",
            "romanization": "kazoku",
            "category": "Noun",
            "translation": "family; members of a family",
            "alwaysShowHint": true,
            "kana": "かぞく"
        },
        {
            "id": "n5_vocab_256",
            "japanese": "警官",
            "romanization": "keikan",
            "category": "Noun",
            "translation": "policeman; police officer",
            "alwaysShowHint": true,
            "kana": "けいかん"
        },
        {
            "id": "n5_vocab_257",
            "japanese": "結婚",
            "romanization": "kekkon",
            "category": "Noun, Suru verb",
            "translation": "marriage",
            "alwaysShowHint": true,
            "kana": "けっこん"
        },
        {
            "id": "n5_vocab_258",
            "japanese": "結構",
            "romanization": "kekkou",
            "category": "Noun, Adverbial Noun, Adjective, な-adjective, Adverb",
            "translation": "splendid, enough",
            "alwaysShowHint": true,
            "kana": "けっこう"
        },
        {
            "id": "n5_vocab_259",
            "japanese": "今朝",
            "romanization": "kesa",
            "category": "Noun, Temporal noun",
            "translation": "this morning",
            "alwaysShowHint": true,
            "kana": "けさ"
        },
        {
            "id": "n5_vocab_260",
            "japanese": "消す",
            "romanization": "kesu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to erase, to turn off power",
            "alwaysShowHint": true,
            "kana": "けす"
        },
        {
            "id": "n5_vocab_261",
            "japanese": "木",
            "romanization": "ki",
            "category": "Noun",
            "translation": "tree; shrub; bush; wood; timber",
            "alwaysShowHint": true,
            "kana": "き"
        },
        {
            "id": "n5_vocab_262",
            "japanese": "消える",
            "romanization": "kieru",
            "category": "Verb, Ichidan verb, Intransitive verb",
            "translation": "to disappear",
            "alwaysShowHint": true,
            "kana": "きえる"
        },
        {
            "id": "n5_vocab_263",
            "japanese": "黄色い",
            "romanization": "kiiroi",
            "category": "Adjective, い-adjective",
            "translation": "yellow",
            "alwaysShowHint": true,
            "kana": "きいろい"
        },
        {
            "id": "n5_vocab_264",
            "japanese": "聞く",
            "romanization": "kiku",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to hear; to listen (to music); to ask; to learn of",
            "alwaysShowHint": true,
            "kana": "きく"
        },
        {
            "id": "n5_vocab_265",
            "japanese": "昨日",
            "romanization": "kinou",
            "category": "Noun",
            "translation": "yesterday",
            "alwaysShowHint": true,
            "kana": "きのう"
        },
        {
            "id": "n5_vocab_266",
            "japanese": "金曜日",
            "romanization": "kinyoubi",
            "category": "Noun",
            "translation": "Friday",
            "alwaysShowHint": true,
            "kana": "きんようび"
        },
        {
            "id": "n5_vocab_267",
            "japanese": "切符",
            "romanization": "kippu",
            "category": "Noun",
            "translation": "ticket",
            "alwaysShowHint": true,
            "kana": "きっぷ"
        },
        {
            "id": "n5_vocab_268",
            "japanese": "嫌い",
            "romanization": "kirai",
            "category": "Noun, Adjective, な-adjective",
            "translation": "hate",
            "alwaysShowHint": true,
            "kana": "きらい"
        },
        {
            "id": "n5_vocab_269",
            "japanese": "綺麗",
            "romanization": "kirei",
            "category": "Adjective, な-adjective",
            "translation": "pretty; lovely; beautiful",
            "alwaysShowHint": true,
            "kana": "きれい"
        },
        {
            "id": "n5_vocab_270",
            "japanese": "キログラム",
            "romanization": "kiro guramu",
            "category": "Noun, Katakana",
            "translation": "kilogram",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_271",
            "japanese": "キロメートル",
            "romanization": "kiro meetoru",
            "category": "Noun, Katakana",
            "translation": "kilometer",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_272",
            "japanese": "切る",
            "romanization": "kiru",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to cut",
            "alwaysShowHint": true,
            "kana": "きる"
        },
        {
            "id": "n5_vocab_273",
            "japanese": "着る",
            "romanization": "kiru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to wear",
            "alwaysShowHint": true,
            "kana": "きる"
        },
        {
            "id": "n5_vocab_274",
            "japanese": "喫茶店",
            "romanization": "kissaten",
            "category": "Noun",
            "translation": "coffee shop; tearoom; cafe",
            "alwaysShowHint": true,
            "kana": "きっさてん"
        },
        {
            "id": "n5_vocab_275",
            "japanese": "北",
            "romanization": "kita",
            "category": "Noun",
            "translation": "north",
            "alwaysShowHint": true,
            "kana": "きた"
        },
        {
            "id": "n5_vocab_276",
            "japanese": "汚い",
            "romanization": "kitanai",
            "category": "Adjective, い-adjective",
            "translation": "dirty",
            "alwaysShowHint": true,
            "kana": "きたない"
        },
        {
            "id": "n5_vocab_277",
            "japanese": "切手",
            "romanization": "kitte",
            "category": "Noun",
            "translation": "stamp (postage)",
            "alwaysShowHint": true,
            "kana": "きって"
        },
        {
            "id": "n5_vocab_278",
            "japanese": "こっち",
            "romanization": "kocchi",
            "category": "Noun",
            "translation": "this person or way",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_279",
            "japanese": "こちら",
            "romanization": "kochira",
            "category": "Noun",
            "translation": "this way; this direction​",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_280",
            "japanese": "子供",
            "romanization": "kodomo",
            "category": "Noun",
            "translation": "child",
            "alwaysShowHint": true,
            "kana": "こども"
        },
        {
            "id": "n5_vocab_281",
            "japanese": "声",
            "romanization": "koe",
            "category": "Noun",
            "translation": "voice",
            "alwaysShowHint": true,
            "kana": "こえ"
        },
        {
            "id": "n5_vocab_282",
            "japanese": "ここ",
            "romanization": "koko",
            "category": "Pronoun",
            "translation": "here; this place",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_283",
            "japanese": "九日",
            "romanization": "kokonoka",
            "category": "Noun",
            "translation": "ninth day of the month / 9 days",
            "alwaysShowHint": true,
            "kana": "ここのか"
        },
        {
            "id": "n5_vocab_284",
            "japanese": "九つ",
            "romanization": "kokonotsu",
            "category": "Noun, Numeric",
            "translation": "nine; 9",
            "alwaysShowHint": true,
            "kana": "ここのつ"
        },
        {
            "id": "n5_vocab_285",
            "japanese": "困る",
            "romanization": "komaru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to be troubled",
            "alwaysShowHint": true,
            "kana": "こまる"
        },
        {
            "id": "n5_vocab_286",
            "japanese": "今晩",
            "romanization": "konban",
            "category": "Noun",
            "translation": "tonight; this evening",
            "alwaysShowHint": true,
            "kana": "こんばん"
        },
        {
            "id": "n5_vocab_287",
            "japanese": "今月",
            "romanization": "kongetsu",
            "category": "Noun",
            "translation": "this month",
            "alwaysShowHint": true,
            "kana": "こんげつ"
        },
        {
            "id": "n5_vocab_288",
            "japanese": "こんな",
            "romanization": "konna",
            "category": "Pre-noun adjectival",
            "translation": "such; like this​",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_289",
            "japanese": "この",
            "romanization": "kono",
            "category": "Pre-noun adjectival",
            "translation": "this​",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_290",
            "japanese": "今週",
            "romanization": "konshuu",
            "category": "Noun",
            "translation": "this week",
            "alwaysShowHint": true,
            "kana": "こんしゅう"
        },
        {
            "id": "n5_vocab_291",
            "japanese": "コーヒー",
            "romanization": "koohii",
            "category": "Noun, Katakana",
            "translation": "Coffee",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_292",
            "japanese": "コート",
            "romanization": "kooto",
            "category": "Noun, Katakana",
            "translation": "coat",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_293",
            "japanese": "コピー",
            "romanization": "kopii",
            "category": "Noun, Suru verb, Katakana",
            "translation": "copy; photocopy",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_294",
            "japanese": "コップ",
            "romanization": "koppu",
            "category": "Noun, Katakana",
            "translation": "glass (drinking vessel); tumbler​",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_295",
            "japanese": "これ",
            "romanization": "kore",
            "category": "Pronoun",
            "translation": "this",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_296",
            "japanese": "答える",
            "romanization": "kotaeru",
            "category": "Verb, Ichidan verb, Intransitive verb",
            "translation": "to answer",
            "alwaysShowHint": true,
            "kana": "こたえる"
        },
        {
            "id": "n5_vocab_297",
            "japanese": "言葉",
            "romanization": "kotoba",
            "category": "Noun",
            "translation": "word; words",
            "alwaysShowHint": true,
            "kana": "ことば"
        },
        {
            "id": "n5_vocab_298",
            "japanese": "今年",
            "romanization": "kotoshi",
            "category": "Noun",
            "translation": "this year",
            "alwaysShowHint": true,
            "kana": "ことし"
        },
        {
            "id": "n5_vocab_299",
            "japanese": "交番",
            "romanization": "kouban",
            "category": "Noun",
            "translation": "police box",
            "alwaysShowHint": true,
            "kana": "こうばん"
        },
        {
            "id": "n5_vocab_300",
            "japanese": "紅茶",
            "romanization": "koucha",
            "category": "Noun",
            "translation": "black tea",
            "alwaysShowHint": true,
            "kana": "こうちゃ"
        },
        {
            "id": "n5_vocab_301",
            "japanese": "公園",
            "romanization": "kouen",
            "category": "Noun",
            "translation": "park",
            "alwaysShowHint": true,
            "kana": "こうえん"
        },
        {
            "id": "n5_vocab_302",
            "japanese": "交差点",
            "romanization": "kousaten",
            "category": "Noun",
            "translation": "intersection",
            "alwaysShowHint": true,
            "kana": "こうさてん"
        },
        {
            "id": "n5_vocab_303",
            "japanese": "口",
            "romanization": "kuchi",
            "category": "Noun",
            "translation": "mouth, opening",
            "alwaysShowHint": true,
            "kana": "くち"
        },
        {
            "id": "n5_vocab_304",
            "japanese": "果物",
            "romanization": "kudamono",
            "category": "Noun",
            "translation": "fruit",
            "alwaysShowHint": true,
            "kana": "くだもの"
        },
        {
            "id": "n5_vocab_305",
            "japanese": "下さい",
            "romanization": "kudasai",
            "category": "Expression",
            "translation": "please",
            "alwaysShowHint": true,
            "kana": "ください"
        },
        {
            "id": "n5_vocab_306",
            "japanese": "曇り",
            "romanization": "kumori",
            "category": "Noun",
            "translation": "cloudiness; cloudy weather",
            "alwaysShowHint": true,
            "kana": "くもり"
        },
        {
            "id": "n5_vocab_307",
            "japanese": "曇る",
            "romanization": "kumoru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to become cloudy, to become dim",
            "alwaysShowHint": true,
            "kana": "くもる"
        },
        {
            "id": "n5_vocab_308",
            "japanese": "国",
            "romanization": "kuni",
            "category": "Noun",
            "translation": "country; state; region",
            "alwaysShowHint": true,
            "kana": "くに"
        },
        {
            "id": "n5_vocab_309",
            "japanese": "暗い",
            "romanization": "kurai",
            "category": "Adjective, い-adjective",
            "translation": "dark; gloomy; murky",
            "alwaysShowHint": true,
            "kana": "くらい"
        },
        {
            "id": "n5_vocab_310",
            "japanese": "クラス",
            "romanization": "kurasu",
            "category": "Noun, Katakana",
            "translation": "class",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_311",
            "japanese": "黒",
            "romanization": "kuro",
            "category": "Noun",
            "translation": "black",
            "alwaysShowHint": true,
            "kana": "くろ"
        },
        {
            "id": "n5_vocab_312",
            "japanese": "黒い",
            "romanization": "kuroi",
            "category": "い-adjective",
            "translation": "black",
            "alwaysShowHint": true,
            "kana": "くろい"
        },
        {
            "id": "n5_vocab_313",
            "japanese": "来る",
            "romanization": "kuru",
            "category": "Verb, Intransitive verb",
            "translation": "to come",
            "alwaysShowHint": true,
            "kana": "くる"
        },
        {
            "id": "n5_vocab_314",
            "japanese": "車",
            "romanization": "kuruma",
            "category": "Noun",
            "translation": "car; automobile; vehicle",
            "alwaysShowHint": true,
            "kana": "くるま"
        },
        {
            "id": "n5_vocab_315",
            "japanese": "薬",
            "romanization": "kusuri",
            "category": "Noun",
            "translation": "medicine",
            "alwaysShowHint": true,
            "kana": "くすり"
        },
        {
            "id": "n5_vocab_316",
            "japanese": "靴",
            "romanization": "kutsu",
            "category": "Noun",
            "translation": "shoes",
            "alwaysShowHint": true,
            "kana": "くつ"
        },
        {
            "id": "n5_vocab_317",
            "japanese": "靴下",
            "romanization": "kutsushita",
            "category": "Noun",
            "translation": "socks",
            "alwaysShowHint": true,
            "kana": "くつした"
        },
        {
            "id": "n5_vocab_318",
            "japanese": "去年",
            "romanization": "kyonen",
            "category": "Noun",
            "translation": "last year",
            "alwaysShowHint": true,
            "kana": "きょねん"
        },
        {
            "id": "n5_vocab_319",
            "japanese": "今日",
            "romanization": "kyou",
            "category": "Noun, Temporal noun",
            "translation": "today; this day",
            "alwaysShowHint": true,
            "kana": "きょう"
        },
        {
            "id": "n5_vocab_320",
            "japanese": "兄弟",
            "romanization": "kyoudai",
            "category": "Noun",
            "translation": "siblings; brothers and sisters​; mate",
            "alwaysShowHint": true,
            "kana": "きょうだい"
        },
        {
            "id": "n5_vocab_321",
            "japanese": "教室",
            "romanization": "kyoushitsu",
            "category": "Noun",
            "translation": "classroom",
            "alwaysShowHint": true,
            "kana": "きょうしつ"
        },
        {
            "id": "n5_vocab_322",
            "japanese": "九",
            "romanization": "kyuu",
            "category": "Noun, Numeric",
            "translation": "nine; 9",
            "alwaysShowHint": true,
            "kana": "きゅう"
        },
        {
            "id": "n5_vocab_323",
            "japanese": "マッチ",
            "romanization": "macchi",
            "category": "Noun, Katakana",
            "translation": "match",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_324",
            "japanese": "町",
            "romanization": "machi",
            "category": "Noun",
            "translation": "town; block; neighborhood",
            "alwaysShowHint": true,
            "kana": "まち"
        },
        {
            "id": "n5_vocab_325",
            "japanese": "窓",
            "romanization": "mado",
            "category": "Noun",
            "translation": "window",
            "alwaysShowHint": true,
            "kana": "まど"
        },
        {
            "id": "n5_vocab_326",
            "japanese": "前",
            "romanization": "mae",
            "category": "Noun, Suffix",
            "translation": "previous; before; in front; ago",
            "alwaysShowHint": true,
            "kana": "まえ"
        },
        {
            "id": "n5_vocab_327",
            "japanese": "曲がる",
            "romanization": "magaru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to turn, to bend",
            "alwaysShowHint": true,
            "kana": "まがる"
        },
        {
            "id": "n5_vocab_328",
            "japanese": "毎朝",
            "romanization": "maiasa",
            "category": "Noun",
            "translation": "every morning",
            "alwaysShowHint": true,
            "kana": "まいあさ"
        },
        {
            "id": "n5_vocab_329",
            "japanese": "毎晩",
            "romanization": "maiban",
            "category": "Noun",
            "translation": "every night",
            "alwaysShowHint": true,
            "kana": "まいばん"
        },
        {
            "id": "n5_vocab_330",
            "japanese": "毎日",
            "romanization": "mainichi",
            "category": "Noun",
            "translation": "every day",
            "alwaysShowHint": true,
            "kana": "まいにち"
        },
        {
            "id": "n5_vocab_331",
            "japanese": "毎週",
            "romanization": "maishuu",
            "category": "Noun",
            "translation": "every week",
            "alwaysShowHint": true,
            "kana": "まいしゅう"
        },
        {
            "id": "n5_vocab_332",
            "japanese": "毎年",
            "romanization": "maitoshi / mainen",
            "category": "Noun",
            "translation": "every year; yearly; annually",
            "alwaysShowHint": true,
            "kana": "まいとし  まいねん"
        },
        {
            "id": "n5_vocab_333",
            "japanese": "毎月",
            "romanization": "maitsuki",
            "category": "Noun",
            "translation": "every month; monthly",
            "alwaysShowHint": true,
            "kana": "まいつき"
        },
        {
            "id": "n5_vocab_334",
            "japanese": "万",
            "romanization": "man",
            "category": "Noun, Numeric",
            "translation": "10,000; ten thousand",
            "alwaysShowHint": true,
            "kana": "まん"
        },
        {
            "id": "n5_vocab_335",
            "japanese": "万年筆",
            "romanization": "mannenhitsu",
            "category": "Noun",
            "translation": "fountain pen",
            "alwaysShowHint": true,
            "kana": "まんねんひつ"
        },
        {
            "id": "n5_vocab_336",
            "japanese": "丸い",
            "romanization": "marui",
            "category": "Adjective, い-adjective",
            "translation": "round,circular",
            "alwaysShowHint": true,
            "kana": "まるい"
        },
        {
            "id": "n5_vocab_337",
            "japanese": "真っ直ぐ",
            "romanization": "massugu",
            "category": "Noun, Adjective, な-adjective, Adverb",
            "translation": "straight ahead,direct",
            "alwaysShowHint": true,
            "kana": "まっすぐ"
        },
        {
            "id": "n5_vocab_338",
            "japanese": "待つ",
            "romanization": "matsu",
            "category": "Verb, Godan verb, Intransitive verb, Transitive verb",
            "translation": "to wait​",
            "alwaysShowHint": true,
            "kana": "まつ"
        },
        {
            "id": "n5_vocab_339",
            "japanese": "不味い",
            "romanization": "mazui",
            "category": "Adjective, い-adjective",
            "translation": "unpleasant",
            "alwaysShowHint": true,
            "kana": "まずい"
        },
        {
            "id": "n5_vocab_340",
            "japanese": "目",
            "romanization": "me",
            "category": "Noun",
            "translation": "eye",
            "alwaysShowHint": true,
            "kana": "め"
        },
        {
            "id": "n5_vocab_341",
            "japanese": "メートル",
            "romanization": "meetoru",
            "category": "Noun, Katakana",
            "translation": "metre; meter",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_342",
            "japanese": "眼鏡",
            "romanization": "megane",
            "category": "Noun",
            "translation": "glasses",
            "alwaysShowHint": true,
            "kana": "めがね"
        },
        {
            "id": "n5_vocab_343",
            "japanese": "道",
            "romanization": "michi",
            "category": "Noun",
            "translation": "road; street",
            "alwaysShowHint": true,
            "kana": "みち"
        },
        {
            "id": "n5_vocab_344",
            "japanese": "緑",
            "romanization": "midori",
            "category": "Noun",
            "translation": "green",
            "alwaysShowHint": true,
            "kana": "みどり"
        },
        {
            "id": "n5_vocab_345",
            "japanese": "磨く",
            "romanization": "migaku",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to polish; to shine; to brush (e.g. teeth)",
            "alwaysShowHint": true,
            "kana": "みがく"
        },
        {
            "id": "n5_vocab_346",
            "japanese": "右",
            "romanization": "migi",
            "category": "Noun",
            "translation": "right; right hand side",
            "alwaysShowHint": true,
            "kana": "みぎ"
        },
        {
            "id": "n5_vocab_347",
            "japanese": "短い",
            "romanization": "mijikai",
            "category": "Adjective, い-adjective",
            "translation": "short",
            "alwaysShowHint": true,
            "kana": "みじかい"
        },
        {
            "id": "n5_vocab_348",
            "japanese": "三日",
            "romanization": "mikka",
            "category": "Noun",
            "translation": "the third day of the month / 3 days",
            "alwaysShowHint": true,
            "kana": "みっか"
        },
        {
            "id": "n5_vocab_349",
            "japanese": "耳",
            "romanization": "mimi",
            "category": "Noun",
            "translation": "ear; hearing",
            "alwaysShowHint": true,
            "kana": "みみ"
        },
        {
            "id": "n5_vocab_350",
            "japanese": "南",
            "romanization": "minami",
            "category": "Noun",
            "translation": "south",
            "alwaysShowHint": true,
            "kana": "みなみ"
        },
        {
            "id": "n5_vocab_351",
            "japanese": "皆さん",
            "romanization": "minasan",
            "category": "Noun",
            "translation": "everyone",
            "alwaysShowHint": true,
            "kana": "みなさん"
        },
        {
            "id": "n5_vocab_352",
            "japanese": "みんな",
            "romanization": "minna",
            "category": "Noun, Adverb",
            "translation": "all; everyone; everybody",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_353",
            "japanese": "見る",
            "romanization": "miru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to see; to look; to watch; to view; to observe",
            "alwaysShowHint": true,
            "kana": "みる"
        },
        {
            "id": "n5_vocab_354",
            "japanese": "店",
            "romanization": "mise",
            "category": "Noun",
            "translation": "store; shop; establishment; restaurant",
            "alwaysShowHint": true,
            "kana": "みせ"
        },
        {
            "id": "n5_vocab_355",
            "japanese": "見せる",
            "romanization": "miseru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to show; to display",
            "alwaysShowHint": true,
            "kana": "みせる"
        },
        {
            "id": "n5_vocab_356",
            "japanese": "三つ",
            "romanization": "mittsu",
            "category": "Noun, Numeric",
            "translation": "three; 3",
            "alwaysShowHint": true,
            "kana": "みっつ"
        },
        {
            "id": "n5_vocab_357",
            "japanese": "水",
            "romanization": "mizu",
            "category": "Noun",
            "translation": "water; fluid; liquid​",
            "alwaysShowHint": true,
            "kana": "みず"
        },
        {
            "id": "n5_vocab_358",
            "japanese": "木曜日",
            "romanization": "mokuyoubi",
            "category": "Noun",
            "translation": "Thursday",
            "alwaysShowHint": true,
            "kana": "もくようび"
        },
        {
            "id": "n5_vocab_359",
            "japanese": "門",
            "romanization": "mon",
            "category": "Noun",
            "translation": "gate",
            "alwaysShowHint": true,
            "kana": "もん"
        },
        {
            "id": "n5_vocab_360",
            "japanese": "問題",
            "romanization": "mondai",
            "category": "Noun",
            "translation": "problem; question (e.g. on a test)",
            "alwaysShowHint": true,
            "kana": "もんだい"
        },
        {
            "id": "n5_vocab_361",
            "japanese": "物",
            "romanization": "mono",
            "category": "Noun",
            "translation": "thing",
            "alwaysShowHint": true,
            "kana": "もの"
        },
        {
            "id": "n5_vocab_362",
            "japanese": "持つ",
            "romanization": "motsu",
            "category": "Verb, Godan verb",
            "translation": "to hold",
            "alwaysShowHint": true,
            "kana": "もつ"
        },
        {
            "id": "n5_vocab_363",
            "japanese": "もっと",
            "romanization": "motto",
            "category": "Adverb",
            "translation": "more; longer; further",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_364",
            "japanese": "もう一度",
            "romanization": "mouichido",
            "category": "Expression",
            "translation": "once more; again",
            "alwaysShowHint": true,
            "kana": "もういちど"
        },
        {
            "id": "n5_vocab_365",
            "japanese": "六日",
            "romanization": "muika",
            "category": "Noun",
            "translation": "sixth day of the month / 6 days",
            "alwaysShowHint": true,
            "kana": "むいか"
        },
        {
            "id": "n5_vocab_366",
            "japanese": "向こう",
            "romanization": "mukou",
            "category": "Noun",
            "translation": "over there",
            "alwaysShowHint": true,
            "kana": "むこう"
        },
        {
            "id": "n5_vocab_367",
            "japanese": "村",
            "romanization": "mura",
            "category": "Noun",
            "translation": "village",
            "alwaysShowHint": true,
            "kana": "むら"
        },
        {
            "id": "n5_vocab_368",
            "japanese": "六つ",
            "romanization": "muttsu",
            "category": "Noun, Numeric",
            "translation": "six; 6",
            "alwaysShowHint": true,
            "kana": "むっつ"
        },
        {
            "id": "n5_vocab_369",
            "japanese": "難しい",
            "romanization": "muzukashii",
            "category": "Adjective, い-adjective",
            "translation": "difficult",
            "alwaysShowHint": true,
            "kana": "むずかしい"
        },
        {
            "id": "n5_vocab_370",
            "japanese": "長い",
            "romanization": "nagai",
            "category": "Adjective, い-adjective",
            "translation": "long (distance)​; long (time); lengthy.",
            "alwaysShowHint": true,
            "kana": "ながい"
        },
        {
            "id": "n5_vocab_371",
            "japanese": "ナイフ",
            "romanization": "naifu",
            "category": "Noun, Katakana",
            "translation": "knife",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_372",
            "japanese": "中",
            "romanization": "naka",
            "category": "Noun",
            "translation": "inside; in; within; center",
            "alwaysShowHint": true,
            "kana": "なか"
        },
        {
            "id": "n5_vocab_373",
            "japanese": "鳴く",
            "romanization": "naku",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "animal noise. to chirp",
            "alwaysShowHint": true,
            "kana": "なく"
        },
        {
            "id": "n5_vocab_374",
            "japanese": "無くす",
            "romanization": "nakusu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to lose (something)",
            "alwaysShowHint": true,
            "kana": "なくす"
        },
        {
            "id": "n5_vocab_375",
            "japanese": "名前",
            "romanization": "namae",
            "category": "Noun",
            "translation": "name; full name; given name",
            "alwaysShowHint": true,
            "kana": "なまえ"
        },
        {
            "id": "n5_vocab_376",
            "japanese": "七つ",
            "romanization": "nanatsu",
            "category": "Noun, Numeric",
            "translation": "seven; 7",
            "alwaysShowHint": true,
            "kana": "ななつ"
        },
        {
            "id": "n5_vocab_377",
            "japanese": "何",
            "romanization": "nani",
            "category": "Noun, Pronoun, Prefix",
            "translation": "what",
            "alwaysShowHint": true,
            "kana": "なに"
        },
        {
            "id": "n5_vocab_378",
            "japanese": "七日",
            "romanization": "nanoka",
            "category": "Noun",
            "translation": "seventh day of the month / 7 days",
            "alwaysShowHint": true,
            "kana": "なのか"
        },
        {
            "id": "n5_vocab_379",
            "japanese": "並べる",
            "romanization": "naraberu",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to line up,to set up",
            "alwaysShowHint": true,
            "kana": "ならべる"
        },
        {
            "id": "n5_vocab_380",
            "japanese": "並ぶ",
            "romanization": "narabu",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to line up,to stand in a line",
            "alwaysShowHint": true,
            "kana": "ならぶ"
        },
        {
            "id": "n5_vocab_381",
            "japanese": "習う",
            "romanization": "narau",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to be taught; to learn (from a teacher)",
            "alwaysShowHint": true,
            "kana": "ならう"
        },
        {
            "id": "n5_vocab_382",
            "japanese": "夏",
            "romanization": "natsu",
            "category": "Noun",
            "translation": "summer",
            "alwaysShowHint": true,
            "kana": "なつ"
        },
        {
            "id": "n5_vocab_383",
            "japanese": "夏休み",
            "romanization": "natsuyasumi",
            "category": "Noun",
            "translation": "summer vacation; summer holiday",
            "alwaysShowHint": true,
            "kana": "なつやすみ"
        },
        {
            "id": "n5_vocab_384",
            "japanese": "何故",
            "romanization": "naze",
            "category": "Adverb",
            "translation": "why; how",
            "alwaysShowHint": true,
            "kana": "なぜ"
        },
        {
            "id": "n5_vocab_385",
            "japanese": "猫",
            "romanization": "neko",
            "category": "Noun",
            "translation": "cat",
            "alwaysShowHint": true,
            "kana": "ねこ"
        },
        {
            "id": "n5_vocab_386",
            "japanese": "ネクタイ",
            "romanization": "nekutai",
            "category": "Noun, Katakana",
            "translation": "tie; necktie",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_387",
            "japanese": "寝る",
            "romanization": "neru",
            "category": "Verb, Ichidan verb, Intransitive verb",
            "translation": "to sleep; to go to bed; to lie down",
            "alwaysShowHint": true,
            "kana": "ねる"
        },
        {
            "id": "n5_vocab_388",
            "japanese": "二",
            "romanization": "ni",
            "category": "Noun, Numeric",
            "translation": "two; 2",
            "alwaysShowHint": true,
            "kana": "に"
        },
        {
            "id": "n5_vocab_389",
            "japanese": "日曜日",
            "romanization": "nichiyoubi",
            "category": "Noun",
            "translation": "Sunday",
            "alwaysShowHint": true,
            "kana": "にちようび"
        },
        {
            "id": "n5_vocab_390",
            "japanese": "賑やか",
            "romanization": "nigiyaka",
            "category": "Adjective, な-adjective",
            "translation": "bustling,busy",
            "alwaysShowHint": true,
            "kana": "にぎやか"
        },
        {
            "id": "n5_vocab_391",
            "japanese": "日記",
            "romanization": "nikki",
            "category": "Noun",
            "translation": "diary; journal",
            "alwaysShowHint": true,
            "kana": "にっき"
        },
        {
            "id": "n5_vocab_392",
            "japanese": "肉",
            "romanization": "niku",
            "category": "Noun",
            "translation": "meat",
            "alwaysShowHint": true,
            "kana": "にく"
        },
        {
            "id": "n5_vocab_393",
            "japanese": "荷物",
            "romanization": "nimotsu",
            "category": "Noun",
            "translation": "luggage; baggage",
            "alwaysShowHint": true,
            "kana": "にもつ"
        },
        {
            "id": "n5_vocab_394",
            "japanese": "西",
            "romanization": "nishi",
            "category": "Noun",
            "translation": "west",
            "alwaysShowHint": true,
            "kana": "にし"
        },
        {
            "id": "n5_vocab_395",
            "japanese": "庭",
            "romanization": "niwa",
            "category": "Noun",
            "translation": "garden",
            "alwaysShowHint": true,
            "kana": "にわ"
        },
        {
            "id": "n5_vocab_396",
            "japanese": "登る",
            "romanization": "noboru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to climb",
            "alwaysShowHint": true,
            "kana": "のぼる"
        },
        {
            "id": "n5_vocab_397",
            "japanese": "飲み物",
            "romanization": "nomimono",
            "category": "Noun",
            "translation": "drink; beverage",
            "alwaysShowHint": true,
            "kana": "のみもの"
        },
        {
            "id": "n5_vocab_398",
            "japanese": "飲む",
            "romanization": "nomu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to drink",
            "alwaysShowHint": true,
            "kana": "のむ"
        },
        {
            "id": "n5_vocab_399",
            "japanese": "ノート",
            "romanization": "nooto",
            "category": "Noun, Katakana",
            "translation": "notebook",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_400",
            "japanese": "乗る",
            "romanization": "noru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to get on (train, plane, bus, ship, etc.)",
            "alwaysShowHint": true,
            "kana": "のる"
        },
        {
            "id": "n5_vocab_401",
            "japanese": "脱ぐ",
            "romanization": "nugu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to take off clothes",
            "alwaysShowHint": true,
            "kana": "ぬぐ"
        },
        {
            "id": "n5_vocab_402",
            "japanese": "温い",
            "romanization": "nurui",
            "category": "Adjective, い-adjective",
            "translation": "luke warm",
            "alwaysShowHint": true,
            "kana": "ぬるい"
        },
        {
            "id": "n5_vocab_403",
            "japanese": "ニュース",
            "romanization": "nyuusu",
            "category": "Noun, Katakana",
            "translation": "news",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_404",
            "japanese": "おばあさん",
            "romanization": "obaasan",
            "category": "Noun",
            "translation": "grandmother",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_405",
            "japanese": "伯母さん",
            "romanization": "obasan",
            "category": "Noun",
            "translation": "aunt; old lady",
            "alwaysShowHint": true,
            "kana": "おばさん"
        },
        {
            "id": "n5_vocab_406",
            "japanese": "お弁当",
            "romanization": "obentou",
            "category": "Noun",
            "translation": "lunch box; Japanese box lunch",
            "alwaysShowHint": true,
            "kana": "おべんとう"
        },
        {
            "id": "n5_vocab_407",
            "japanese": "覚える",
            "romanization": "oboeru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to remember",
            "alwaysShowHint": true,
            "kana": "おぼえる"
        },
        {
            "id": "n5_vocab_408",
            "japanese": "お茶",
            "romanization": "ocha",
            "category": "Noun",
            "translation": "tea",
            "alwaysShowHint": true,
            "kana": "おちゃ"
        },
        {
            "id": "n5_vocab_409",
            "japanese": "お風呂",
            "romanization": "ofuro",
            "category": "Noun",
            "translation": "bath",
            "alwaysShowHint": true,
            "kana": "おふろ"
        },
        {
            "id": "n5_vocab_410",
            "japanese": "美味しい",
            "romanization": "oishii",
            "category": "い-adjective",
            "translation": "delicious",
            "alwaysShowHint": true,
            "kana": "おいしい"
        },
        {
            "id": "n5_vocab_411",
            "japanese": "伯父さん",
            "romanization": "ojisan",
            "category": "Noun",
            "translation": "uncle; old man; mister",
            "alwaysShowHint": true,
            "kana": "おじさん"
        },
        {
            "id": "n5_vocab_412",
            "japanese": "お母さん",
            "romanization": "okaasan",
            "category": "Noun",
            "translation": "mother; mom; mum; ma",
            "alwaysShowHint": true,
            "kana": "おかあさん"
        },
        {
            "id": "n5_vocab_413",
            "japanese": "お金",
            "romanization": "okane",
            "category": "Noun",
            "translation": "money",
            "alwaysShowHint": true,
            "kana": "おかね"
        },
        {
            "id": "n5_vocab_414",
            "japanese": "お菓子",
            "romanization": "okashi",
            "category": "Noun",
            "translation": "confections; sweets; candy",
            "alwaysShowHint": true,
            "kana": "おかし"
        },
        {
            "id": "n5_vocab_415",
            "japanese": "起きる",
            "romanization": "okiru",
            "category": "Verb, Ichidan verb, Intransitive verb",
            "translation": "to get up; to wake up",
            "alwaysShowHint": true,
            "kana": "おきる"
        },
        {
            "id": "n5_vocab_416",
            "japanese": "置く",
            "romanization": "oku",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to put; to place​",
            "alwaysShowHint": true,
            "kana": "おく"
        },
        {
            "id": "n5_vocab_417",
            "japanese": "奥さん",
            "romanization": "okusan",
            "category": "Noun",
            "translation": "wife; your wife; his wife",
            "alwaysShowHint": true,
            "kana": "おくさん"
        },
        {
            "id": "n5_vocab_418",
            "japanese": "お巡りさん",
            "romanization": "omawari san",
            "category": "Noun",
            "translation": "police officer (friendly term for policeman)",
            "alwaysShowHint": true,
            "kana": "おまわりさん"
        },
        {
            "id": "n5_vocab_419",
            "japanese": "重い",
            "romanization": "omoi",
            "category": "い-adjective",
            "translation": "heavy",
            "alwaysShowHint": true,
            "kana": "おもい"
        },
        {
            "id": "n5_vocab_420",
            "japanese": "面白い",
            "romanization": "omoshiroi",
            "category": "Adjective, い-adjective",
            "translation": "interesting",
            "alwaysShowHint": true,
            "kana": "おもしろい"
        },
        {
            "id": "n5_vocab_421",
            "japanese": "同じ",
            "romanization": "onaji",
            "category": "Noun, Adverb",
            "translation": "same",
            "alwaysShowHint": true,
            "kana": "おなじ"
        },
        {
            "id": "n5_vocab_422",
            "japanese": "お腹",
            "romanization": "onaka",
            "category": "Noun",
            "translation": "stomach",
            "alwaysShowHint": true,
            "kana": "おなか"
        },
        {
            "id": "n5_vocab_423",
            "japanese": "お姉さん",
            "romanization": "oneesan",
            "category": "Noun",
            "translation": "elder sister; young lady; miss; ma'am",
            "alwaysShowHint": true,
            "kana": "おねえさん"
        },
        {
            "id": "n5_vocab_424",
            "japanese": "音楽",
            "romanization": "ongaku",
            "category": "Noun",
            "translation": "music",
            "alwaysShowHint": true,
            "kana": "おんがく"
        },
        {
            "id": "n5_vocab_425",
            "japanese": "お兄さん",
            "romanization": "oniisan",
            "category": "Noun",
            "translation": "older brother; elder brother; young man; buddy",
            "alwaysShowHint": true,
            "kana": "おにいさん"
        },
        {
            "id": "n5_vocab_426",
            "japanese": "女",
            "romanization": "onna",
            "category": "Noun, Prefix",
            "translation": "woman; female sex",
            "alwaysShowHint": true,
            "kana": "おんな"
        },
        {
            "id": "n5_vocab_427",
            "japanese": "女の子",
            "romanization": "onnanoko",
            "category": "Noun, Expression",
            "translation": "girl; daughter; young women",
            "alwaysShowHint": true,
            "kana": "おんなのこ"
        },
        {
            "id": "n5_vocab_428",
            "japanese": "多い",
            "romanization": "ooi",
            "category": "Adjective, い-adjective",
            "translation": "many; numerous; a lot; large quantity; frequent",
            "alwaysShowHint": true,
            "kana": "おおい"
        },
        {
            "id": "n5_vocab_429",
            "japanese": "大きい",
            "romanization": "ookii",
            "category": "Adjective, い-adjective",
            "translation": "big; large; great; important",
            "alwaysShowHint": true,
            "kana": "おおきい"
        },
        {
            "id": "n5_vocab_430",
            "japanese": "大きな",
            "romanization": "ookina",
            "category": "Pre-noun adjectival",
            "translation": "big; large; great​",
            "alwaysShowHint": true,
            "kana": "おおきな"
        },
        {
            "id": "n5_vocab_431",
            "japanese": "大勢",
            "romanization": "oozei",
            "category": "Noun",
            "translation": "crowd of people; great number of people",
            "alwaysShowHint": true,
            "kana": "おおぜい"
        },
        {
            "id": "n5_vocab_432",
            "japanese": "降りる",
            "romanization": "oriru",
            "category": "Verb, Ichidan verb, Intransitive verb",
            "translation": "to get off",
            "alwaysShowHint": true,
            "kana": "おりる"
        },
        {
            "id": "n5_vocab_433",
            "japanese": "お酒",
            "romanization": "osake",
            "category": "Noun",
            "translation": "alcohol",
            "alwaysShowHint": true,
            "kana": "おさけ"
        },
        {
            "id": "n5_vocab_434",
            "japanese": "お皿",
            "romanization": "osara",
            "category": "Noun",
            "translation": "plate, dish",
            "alwaysShowHint": true,
            "kana": "おさら"
        },
        {
            "id": "n5_vocab_435",
            "japanese": "教える",
            "romanization": "oshieru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to teach",
            "alwaysShowHint": true,
            "kana": "おしえる"
        },
        {
            "id": "n5_vocab_436",
            "japanese": "遅い",
            "romanization": "osoi",
            "category": "Adjective, い-adjective",
            "translation": "slow; time-consuming; late",
            "alwaysShowHint": true,
            "kana": "おそい"
        },
        {
            "id": "n5_vocab_437",
            "japanese": "押す",
            "romanization": "osu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to push; to press​",
            "alwaysShowHint": true,
            "kana": "おす"
        },
        {
            "id": "n5_vocab_438",
            "japanese": "お手洗い",
            "romanization": "otearai",
            "category": "Noun",
            "translation": "toilet; restroom; lavatory; bathroom",
            "alwaysShowHint": true,
            "kana": "おてあらい"
        },
        {
            "id": "n5_vocab_439",
            "japanese": "男",
            "romanization": "otoko",
            "category": "Noun",
            "translation": "man; male",
            "alwaysShowHint": true,
            "kana": "おとこ"
        },
        {
            "id": "n5_vocab_440",
            "japanese": "男の子",
            "romanization": "otokonoko",
            "category": "Noun, Expression",
            "translation": "boy; male child; baby boy",
            "alwaysShowHint": true,
            "kana": "おとこのこ"
        },
        {
            "id": "n5_vocab_441",
            "japanese": "大人",
            "romanization": "otona",
            "category": "Noun",
            "translation": "adult",
            "alwaysShowHint": true,
            "kana": "おとな"
        },
        {
            "id": "n5_vocab_442",
            "japanese": "一昨日",
            "romanization": "ototoi",
            "category": "Noun",
            "translation": "day before yesterday",
            "alwaysShowHint": true,
            "kana": "おととい"
        },
        {
            "id": "n5_vocab_443",
            "japanese": "一昨年",
            "romanization": "ototoshi",
            "category": "Noun",
            "translation": "year before last",
            "alwaysShowHint": true,
            "kana": "おととし"
        },
        {
            "id": "n5_vocab_444",
            "japanese": "お父さん",
            "romanization": "otousan",
            "category": "Noun",
            "translation": "father; dad; papa; pa; pop; daddy",
            "alwaysShowHint": true,
            "kana": "おとうさん"
        },
        {
            "id": "n5_vocab_445",
            "japanese": "弟",
            "romanization": "otouto",
            "category": "Noun",
            "translation": "younger brother",
            "alwaysShowHint": true,
            "kana": "おとうと"
        },
        {
            "id": "n5_vocab_446",
            "japanese": "終る",
            "romanization": "owaru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to finish; to end",
            "alwaysShowHint": true,
            "kana": "おわる"
        },
        {
            "id": "n5_vocab_447",
            "japanese": "泳ぐ",
            "romanization": "oyogu",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to swim",
            "alwaysShowHint": true,
            "kana": "およぐ"
        },
        {
            "id": "n5_vocab_448",
            "japanese": "パーティー",
            "romanization": "paatii",
            "category": "Noun, Katakana",
            "translation": "party",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_449",
            "japanese": "パン",
            "romanization": "pan",
            "category": "Noun, Katakana",
            "translation": "bread",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_450",
            "japanese": "ページ",
            "romanization": "peeji",
            "category": "Noun, Katakana",
            "translation": "page",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_451",
            "japanese": "ペン",
            "romanization": "pen",
            "category": "Noun, Katakana",
            "translation": "pen",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_452",
            "japanese": "ペット",
            "romanization": "petto",
            "category": "Noun, Katakana",
            "translation": "pet",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_453",
            "japanese": "ポケット",
            "romanization": "poketto",
            "category": "Noun",
            "translation": "pocket",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_454",
            "japanese": "ポスト",
            "romanization": "posuto",
            "category": "Noun, Katakana",
            "translation": "post",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_455",
            "japanese": "プール",
            "romanization": "puuru",
            "category": "Noun, Katakana",
            "translation": "swimming pool",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_456",
            "japanese": "来月",
            "romanization": "raigetsu",
            "category": "Noun",
            "translation": "next month",
            "alwaysShowHint": true,
            "kana": "らいげつ"
        },
        {
            "id": "n5_vocab_457",
            "japanese": "来年",
            "romanization": "rainen",
            "category": "Noun",
            "translation": "next year",
            "alwaysShowHint": true,
            "kana": "らいねん"
        },
        {
            "id": "n5_vocab_458",
            "japanese": "来週",
            "romanization": "raishuu",
            "category": "Noun",
            "translation": "next week",
            "alwaysShowHint": true,
            "kana": "らいしゅう"
        },
        {
            "id": "n5_vocab_459",
            "japanese": "ラジオ",
            "romanization": "rajiio",
            "category": "Noun, Katakana",
            "translation": "radio",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_460",
            "japanese": "零",
            "romanization": "rei",
            "category": "Noun",
            "translation": "zero",
            "alwaysShowHint": true,
            "kana": "れい"
        },
        {
            "id": "n5_vocab_461",
            "japanese": "冷蔵庫",
            "romanization": "reizouko",
            "category": "Noun",
            "translation": "refrigerator",
            "alwaysShowHint": true,
            "kana": "れいぞうこ"
        },
        {
            "id": "n5_vocab_462",
            "japanese": "レコード",
            "romanization": "rekoodo",
            "category": "Noun, Katakana",
            "translation": "record",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_463",
            "japanese": "練習",
            "romanization": "renshuu",
            "category": "Noun, Verb, Suru verb",
            "translation": "practice; practicing",
            "alwaysShowHint": true,
            "kana": "れんしゅう"
        },
        {
            "id": "n5_vocab_464",
            "japanese": "レストラン",
            "romanization": "resutoran",
            "category": "Noun, Katakana",
            "translation": "restaurant",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_465",
            "japanese": "立派",
            "romanization": "rippa",
            "category": "Adjective, な-adjective",
            "translation": "splendid",
            "alwaysShowHint": true,
            "kana": "りっぱ"
        },
        {
            "id": "n5_vocab_466",
            "japanese": "六",
            "romanization": "roku",
            "category": "Noun, Numeric",
            "translation": "six; 6",
            "alwaysShowHint": true,
            "kana": "ろく"
        },
        {
            "id": "n5_vocab_467",
            "japanese": "廊下",
            "romanization": "rouka",
            "category": "Noun",
            "translation": "corridor; hallway; passageway",
            "alwaysShowHint": true,
            "kana": "ろうか"
        },
        {
            "id": "n5_vocab_468",
            "japanese": "旅行",
            "romanization": "ryokou",
            "category": "Noun, Verb, Suru verb",
            "translation": "travel; trip; journey; excursion; tour",
            "alwaysShowHint": true,
            "kana": "りょこう"
        },
        {
            "id": "n5_vocab_469",
            "japanese": "料理",
            "romanization": "ryouri",
            "category": "Noun, Suru verb",
            "translation": "cuisine",
            "alwaysShowHint": true,
            "kana": "りょうり"
        },
        {
            "id": "n5_vocab_470",
            "japanese": "両親",
            "romanization": "ryoushin",
            "category": "Noun",
            "translation": "parents; both parents",
            "alwaysShowHint": true,
            "kana": "りょうしん"
        },
        {
            "id": "n5_vocab_471",
            "japanese": "留学生",
            "romanization": "ryuugakusei",
            "category": "Noun",
            "translation": "overseas student; exchange student",
            "alwaysShowHint": true,
            "kana": "りゅうがくせい"
        },
        {
            "id": "n5_vocab_472",
            "japanese": "さあ",
            "romanization": "saa",
            "category": "Conjunction",
            "translation": "well…",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_473",
            "japanese": "財布",
            "romanization": "saifu",
            "category": "Noun",
            "translation": "purse; wallet",
            "alwaysShowHint": true,
            "kana": "さいふ"
        },
        {
            "id": "n5_vocab_474",
            "japanese": "魚",
            "romanization": "sakana",
            "category": "Noun",
            "translation": "fish",
            "alwaysShowHint": true,
            "kana": "さかな"
        },
        {
            "id": "n5_vocab_475",
            "japanese": "先",
            "romanization": "saki",
            "category": "Noun, Prefix, Suffix",
            "translation": "previous; prior; first; earlier",
            "alwaysShowHint": true,
            "kana": "さき"
        },
        {
            "id": "n5_vocab_476",
            "japanese": "咲く",
            "romanization": "saku",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to bloom",
            "alwaysShowHint": true,
            "kana": "さく"
        },
        {
            "id": "n5_vocab_477",
            "japanese": "作文",
            "romanization": "sakubun",
            "category": "Noun",
            "translation": "writing; composition",
            "alwaysShowHint": true,
            "kana": "さくぶん"
        },
        {
            "id": "n5_vocab_478",
            "japanese": "寒い",
            "romanization": "samui",
            "category": "Adjective, い-adjective",
            "translation": "cold",
            "alwaysShowHint": true,
            "kana": "さむい"
        },
        {
            "id": "n5_vocab_479",
            "japanese": "三",
            "romanization": "san",
            "category": "Noun, Numeric",
            "translation": "three; 3",
            "alwaysShowHint": true,
            "kana": "さん"
        },
        {
            "id": "n5_vocab_480",
            "japanese": "散歩",
            "romanization": "sanpo",
            "category": "Noun, Verb, Suru verb",
            "translation": "walk; stroll",
            "alwaysShowHint": true,
            "kana": "さんぽ"
        },
        {
            "id": "n5_vocab_481",
            "japanese": "再来年",
            "romanization": "sarainen",
            "category": "Noun",
            "translation": "year after next",
            "alwaysShowHint": true,
            "kana": "さらいねん"
        },
        {
            "id": "n5_vocab_482",
            "japanese": "差す",
            "romanization": "sasu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to stretch out hands, to raise an umbrella",
            "alwaysShowHint": true,
            "kana": "さす"
        },
        {
            "id": "n5_vocab_483",
            "japanese": "砂糖",
            "romanization": "satou",
            "category": "Noun",
            "translation": "sugar",
            "alwaysShowHint": true,
            "kana": "さとう"
        },
        {
            "id": "n5_vocab_484",
            "japanese": "背",
            "romanization": "se",
            "category": "Noun",
            "translation": "height; stature; back; spine",
            "alwaysShowHint": true,
            "kana": "せ"
        },
        {
            "id": "n5_vocab_485",
            "japanese": "背広",
            "romanization": "sebiro",
            "category": "Noun",
            "translation": "business suit",
            "alwaysShowHint": true,
            "kana": "せびろ"
        },
        {
            "id": "n5_vocab_486",
            "japanese": "セーター",
            "romanization": "seetaa",
            "category": "Noun, Katakana",
            "translation": "sweater; jumper",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_487",
            "japanese": "生徒",
            "romanization": "seito",
            "category": "Noun",
            "translation": "pupil; student",
            "alwaysShowHint": true,
            "kana": "せいと"
        },
        {
            "id": "n5_vocab_488",
            "japanese": "石鹼",
            "romanization": "sekken",
            "category": "Noun",
            "translation": "soap",
            "alwaysShowHint": true,
            "kana": "せっけん"
        },
        {
            "id": "n5_vocab_489",
            "japanese": "狭い",
            "romanization": "semai",
            "category": "い-adjective",
            "translation": "narrow",
            "alwaysShowHint": true,
            "kana": "せまい"
        },
        {
            "id": "n5_vocab_490",
            "japanese": "千",
            "romanization": "sen",
            "category": "Noun, Numeric",
            "translation": "1,000; thousand",
            "alwaysShowHint": true,
            "kana": "せん"
        },
        {
            "id": "n5_vocab_491",
            "japanese": "先月",
            "romanization": "sengetsu",
            "category": "Noun",
            "translation": "last month",
            "alwaysShowHint": true,
            "kana": "せんげつ"
        },
        {
            "id": "n5_vocab_492",
            "japanese": "先生",
            "romanization": "sensei",
            "category": "Noun, Suffix",
            "translation": "teacher; instructor; master",
            "alwaysShowHint": true,
            "kana": "せんせい"
        },
        {
            "id": "n5_vocab_493",
            "japanese": "先週",
            "romanization": "senshuu",
            "category": "Noun",
            "translation": "last week",
            "alwaysShowHint": true,
            "kana": "せんしゅう"
        },
        {
            "id": "n5_vocab_494",
            "japanese": "洗濯",
            "romanization": "sentaku",
            "category": "Noun, Suru verb",
            "translation": "washing; laundry",
            "alwaysShowHint": true,
            "kana": "せんたく"
        },
        {
            "id": "n5_vocab_495",
            "japanese": "写真",
            "romanization": "shashin",
            "category": "Noun",
            "translation": "photograph; photo",
            "alwaysShowHint": true,
            "kana": "しゃしん"
        },
        {
            "id": "n5_vocab_496",
            "japanese": "シャツ",
            "romanization": "shatsu",
            "category": "Noun, Katakana",
            "translation": "shirt",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_497",
            "japanese": "シャワー",
            "romanization": "shawaa",
            "category": "Noun, Katakana",
            "translation": "shower",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_498",
            "japanese": "四",
            "romanization": "shi",
            "category": "Noun, Numeric",
            "translation": "four; 4",
            "alwaysShowHint": true,
            "kana": "し"
        },
        {
            "id": "n5_vocab_499",
            "japanese": "七",
            "romanization": "shichi",
            "category": "Noun, Numeric",
            "translation": "seven; 7",
            "alwaysShowHint": true,
            "kana": "しち"
        },
        {
            "id": "n5_vocab_500",
            "japanese": "仕事",
            "romanization": "shigoto",
            "category": "Noun, Verb, Suru verb",
            "translation": "work; job; business",
            "alwaysShowHint": true,
            "kana": "しごと"
        },
        {
            "id": "n5_vocab_501",
            "japanese": "閉まる",
            "romanization": "shimaru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to close, to be closed",
            "alwaysShowHint": true,
            "kana": "しまる"
        },
        {
            "id": "n5_vocab_502",
            "japanese": "締める",
            "romanization": "shimeru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to tie; to fasten; to tighten​",
            "alwaysShowHint": true,
            "kana": "しめる"
        },
        {
            "id": "n5_vocab_503",
            "japanese": "閉める",
            "romanization": "shimeru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to close; to shut",
            "alwaysShowHint": true,
            "kana": "しめる"
        },
        {
            "id": "n5_vocab_504",
            "japanese": "新聞",
            "romanization": "shinbun",
            "category": "Noun",
            "translation": "newspaper",
            "alwaysShowHint": true,
            "kana": "しんぶん"
        },
        {
            "id": "n5_vocab_505",
            "japanese": "死ぬ",
            "romanization": "shinu",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to die",
            "alwaysShowHint": true,
            "kana": "しぬ"
        },
        {
            "id": "n5_vocab_506",
            "japanese": "塩",
            "romanization": "shio",
            "category": "Noun",
            "translation": "salt",
            "alwaysShowHint": true,
            "kana": "しお"
        },
        {
            "id": "n5_vocab_507",
            "japanese": "白",
            "romanization": "shiro",
            "category": "Noun",
            "translation": "white; innocence; innocent person",
            "alwaysShowHint": true,
            "kana": "しろ"
        },
        {
            "id": "n5_vocab_508",
            "japanese": "白い",
            "romanization": "shiroi",
            "category": "Adjective, い-adjective",
            "translation": "white",
            "alwaysShowHint": true,
            "kana": "しろい"
        },
        {
            "id": "n5_vocab_509",
            "japanese": "知る",
            "romanization": "shiru",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to know",
            "alwaysShowHint": true,
            "kana": "しる"
        },
        {
            "id": "n5_vocab_510",
            "japanese": "下",
            "romanization": "shita",
            "category": "Noun",
            "translation": "below; down; under; bottom",
            "alwaysShowHint": true,
            "kana": "した"
        },
        {
            "id": "n5_vocab_511",
            "japanese": "質問",
            "romanization": "shitsumon",
            "category": "Noun, Verb, Suru verb",
            "translation": "question; inquiry",
            "alwaysShowHint": true,
            "kana": "しつもん"
        },
        {
            "id": "n5_vocab_512",
            "japanese": "静か",
            "romanization": "shizuka",
            "category": "Adjective, な-adjective",
            "translation": "quiet",
            "alwaysShowHint": true,
            "kana": "しずか"
        },
        {
            "id": "n5_vocab_513",
            "japanese": "食堂",
            "romanization": "shokudou",
            "category": "Noun",
            "translation": "cafeteria; dining room",
            "alwaysShowHint": true,
            "kana": "しょくどう"
        },
        {
            "id": "n5_vocab_514",
            "japanese": "醬油",
            "romanization": "shouyu",
            "category": "Noun",
            "translation": "soy sauce",
            "alwaysShowHint": true,
            "kana": "しょうゆ"
        },
        {
            "id": "n5_vocab_515",
            "japanese": "宿題",
            "romanization": "shukudai",
            "category": "Noun",
            "translation": "homework; assignment; pending issue",
            "alwaysShowHint": true,
            "kana": "しゅくだい"
        },
        {
            "id": "n5_vocab_516",
            "japanese": "そば",
            "romanization": "soba",
            "category": "Noun",
            "translation": "near; beside",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_517",
            "japanese": "そっち",
            "romanization": "socchi",
            "category": "Pronoun",
            "translation": "that way; ​over there",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_518",
            "japanese": "そちら",
            "romanization": "sochira",
            "category": "Pronoun",
            "translation": "that way (distant from speaker, close to listener); you; your family",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_519",
            "japanese": "そこ",
            "romanization": "soko",
            "category": "Pronoun",
            "translation": "that place​; there",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_520",
            "japanese": "その",
            "romanization": "sono",
            "category": "Pre-noun adjectival",
            "translation": "that",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_521",
            "japanese": "空",
            "romanization": "sora",
            "category": "Noun",
            "translation": "sky; the air",
            "alwaysShowHint": true,
            "kana": "そら"
        },
        {
            "id": "n5_vocab_522",
            "japanese": "それ",
            "romanization": "sore",
            "category": "Pronoun",
            "translation": "that",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_523",
            "japanese": "それでは",
            "romanization": "sore dewa",
            "category": "Expression",
            "translation": "in that situation",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_524",
            "japanese": "外",
            "romanization": "soto",
            "category": "Noun",
            "translation": "outside; exterior;",
            "alwaysShowHint": true,
            "kana": "そと"
        },
        {
            "id": "n5_vocab_525",
            "japanese": "掃除",
            "romanization": "souji",
            "category": "Noun, Suru verb",
            "translation": "to clean, to sweep",
            "alwaysShowHint": true,
            "kana": "そうじ"
        },
        {
            "id": "n5_vocab_526",
            "japanese": "直ぐに",
            "romanization": "sugu ni",
            "category": "Adverb",
            "translation": "immediately; right away; instantly​",
            "alwaysShowHint": true,
            "kana": "すぐに"
        },
        {
            "id": "n5_vocab_527",
            "japanese": "水曜日",
            "romanization": "suiyoubi",
            "category": "Noun",
            "translation": "Wednesday",
            "alwaysShowHint": true,
            "kana": "すいようび"
        },
        {
            "id": "n5_vocab_528",
            "japanese": "スカート",
            "romanization": "sukaato",
            "category": "Noun, Katakana",
            "translation": "skirt",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_529",
            "japanese": "好き",
            "romanization": "suki",
            "category": "Noun, Adjective, な-adjective",
            "translation": "like",
            "alwaysShowHint": true,
            "kana": "すき"
        },
        {
            "id": "n5_vocab_530",
            "japanese": "少し",
            "romanization": "sukoshi",
            "category": "Noun, Adverb",
            "translation": "a little (bit); small quantity; few; short distance",
            "alwaysShowHint": true,
            "kana": "すこし"
        },
        {
            "id": "n5_vocab_531",
            "japanese": "少ない",
            "romanization": "sukunai",
            "category": "い-adjective",
            "translation": "few; a little; scarce; insufficient; seldom",
            "alwaysShowHint": true,
            "kana": "すくない"
        },
        {
            "id": "n5_vocab_532",
            "japanese": "住む",
            "romanization": "sumu",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to live in; to reside; to inhabit; to dwell; to abide",
            "alwaysShowHint": true,
            "kana": "すむ"
        },
        {
            "id": "n5_vocab_533",
            "japanese": "スポーツ",
            "romanization": "supootsu",
            "category": "Noun, Katakana",
            "translation": "sport; sports",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_534",
            "japanese": "スプーン",
            "romanization": "supuun",
            "category": "Noun, Katakana",
            "translation": "spoon",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_535",
            "japanese": "スリッパ",
            "romanization": "surippa",
            "category": "Noun, Katakana",
            "translation": "slipper; slippers",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_536",
            "japanese": "ストーブ",
            "romanization": "sutoobu",
            "category": "Noun, Katakana",
            "translation": "heater; stove",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_537",
            "japanese": "吸う",
            "romanization": "suu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to smoke, to suck",
            "alwaysShowHint": true,
            "kana": "すう"
        },
        {
            "id": "n5_vocab_538",
            "japanese": "座る",
            "romanization": "suwaru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to sit",
            "alwaysShowHint": true,
            "kana": "すわる"
        },
        {
            "id": "n5_vocab_539",
            "japanese": "涼しい",
            "romanization": "suzushii",
            "category": "Adjective, い-adjective",
            "translation": "refreshing, cool",
            "alwaysShowHint": true,
            "kana": "すずしい"
        },
        {
            "id": "n5_vocab_540",
            "japanese": "たばこ",
            "romanization": "tabako",
            "category": "Noun",
            "translation": "tobacco; cigarette",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_541",
            "japanese": "食べ物",
            "romanization": "tabemono",
            "category": "Noun",
            "translation": "food",
            "alwaysShowHint": true,
            "kana": "たべもの"
        },
        {
            "id": "n5_vocab_542",
            "japanese": "食べる",
            "romanization": "taberu",
            "category": "Verb, Ichidan verb, Intransitive verb",
            "translation": "to eat",
            "alwaysShowHint": true,
            "kana": "たべる"
        },
        {
            "id": "n5_vocab_543",
            "japanese": "多分",
            "romanization": "tabun",
            "category": "Adjective, な-adjective, Adverb",
            "translation": "perhaps; probably",
            "alwaysShowHint": true,
            "kana": "たぶん"
        },
        {
            "id": "n5_vocab_544",
            "japanese": "大変",
            "romanization": "taihen",
            "category": "Noun, Adjective, な-adjective, Adverb",
            "translation": "very; greatly; terribly; serious; difficult",
            "alwaysShowHint": true,
            "kana": "たいへん"
        },
        {
            "id": "n5_vocab_545",
            "japanese": "大切",
            "romanization": "taisetsu",
            "category": "Noun, Adjective, な-adjective",
            "translation": "important; necessary; indispensable; beloved",
            "alwaysShowHint": true,
            "kana": "たいせつ"
        },
        {
            "id": "n5_vocab_546",
            "japanese": "大使館",
            "romanization": "taishikan",
            "category": "Noun",
            "translation": "embassy",
            "alwaysShowHint": true,
            "kana": "たいしかん"
        },
        {
            "id": "n5_vocab_547",
            "japanese": "高い",
            "romanization": "takai",
            "category": "Adjective, い-adjective",
            "translation": "high; tall; expensive; above average",
            "alwaysShowHint": true,
            "kana": "たかい"
        },
        {
            "id": "n5_vocab_548",
            "japanese": "沢山",
            "romanization": "takusan",
            "category": "Noun, Adverbial Noun, Adjective, な-adjective",
            "translation": "many",
            "alwaysShowHint": true,
            "kana": "たくさん"
        },
        {
            "id": "n5_vocab_549",
            "japanese": "タクシー",
            "romanization": "takushii",
            "category": "Noun, Katakana",
            "translation": "taxi",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_550",
            "japanese": "卵",
            "romanization": "tamago",
            "category": "Noun",
            "translation": "eggs; egg",
            "alwaysShowHint": true,
            "kana": "たまご"
        },
        {
            "id": "n5_vocab_551",
            "japanese": "誕生日",
            "romanization": "tanjoubi",
            "category": "Noun",
            "translation": "birthday",
            "alwaysShowHint": true,
            "kana": "たんじょうび"
        },
        {
            "id": "n5_vocab_552",
            "japanese": "頼む",
            "romanization": "tanomu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to ask",
            "alwaysShowHint": true,
            "kana": "たのむ"
        },
        {
            "id": "n5_vocab_553",
            "japanese": "楽しい",
            "romanization": "tanoshii",
            "category": "Adjective, い-adjective",
            "translation": "enjoyable; fun",
            "alwaysShowHint": true,
            "kana": "たのしい"
        },
        {
            "id": "n5_vocab_554",
            "japanese": "縦",
            "romanization": "tate",
            "category": "Noun",
            "translation": "length,height",
            "alwaysShowHint": true,
            "kana": "たて"
        },
        {
            "id": "n5_vocab_555",
            "japanese": "建物",
            "romanization": "tatemono",
            "category": "Noun",
            "translation": "building",
            "alwaysShowHint": true,
            "kana": "たてもの"
        },
        {
            "id": "n5_vocab_556",
            "japanese": "立つ",
            "romanization": "tatsu",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to stand; to stand up​",
            "alwaysShowHint": true,
            "kana": "たつ"
        },
        {
            "id": "n5_vocab_557",
            "japanese": "手",
            "romanization": "te",
            "category": "Noun",
            "translation": "hand; arm",
            "alwaysShowHint": true,
            "kana": "て"
        },
        {
            "id": "n5_vocab_558",
            "japanese": "テーブル",
            "romanization": "teeburu",
            "category": "Noun, Katakana",
            "translation": "table",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_559",
            "japanese": "テープ",
            "romanization": "teepu",
            "category": "Noun, Katakana",
            "translation": "tape",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_560",
            "japanese": "テープレコーダー",
            "romanization": "teepu rekoodaa",
            "category": "Noun, Katakana",
            "translation": "tape recorder",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_561",
            "japanese": "手紙",
            "romanization": "tegami",
            "category": "Noun",
            "translation": "Letter (message)​",
            "alwaysShowHint": true,
            "kana": "てがみ"
        },
        {
            "id": "n5_vocab_562",
            "japanese": "天気",
            "romanization": "tenki",
            "category": "Noun",
            "translation": "weather; the elements",
            "alwaysShowHint": true,
            "kana": "てんき"
        },
        {
            "id": "n5_vocab_563",
            "japanese": "テレビ",
            "romanization": "terebi",
            "category": "Noun, Katakana",
            "translation": "television; TV​",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_564",
            "japanese": "テスト",
            "romanization": "tesuto",
            "category": "Noun, Suru verb, Katakana",
            "translation": "examination; quiz; test",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_565",
            "japanese": "戸",
            "romanization": "to",
            "category": "Noun",
            "translation": "Japanese style door",
            "alwaysShowHint": true,
            "kana": "と"
        },
        {
            "id": "n5_vocab_566",
            "japanese": "飛ぶ",
            "romanization": "tobu",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to fly; to hop",
            "alwaysShowHint": true,
            "kana": "とぶ"
        },
        {
            "id": "n5_vocab_567",
            "japanese": "トイレ",
            "romanization": "toire",
            "category": "Noun, Katakana",
            "translation": "toilet",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_568",
            "japanese": "時計",
            "romanization": "tokei",
            "category": "Noun",
            "translation": "watch; clock; timepiece",
            "alwaysShowHint": true,
            "kana": "とけい"
        },
        {
            "id": "n5_vocab_569",
            "japanese": "時",
            "romanization": "toki",
            "category": "Noun",
            "translation": "time; moment; occasion; chance",
            "alwaysShowHint": true,
            "kana": "とき"
        },
        {
            "id": "n5_vocab_570",
            "japanese": "時々",
            "romanization": "tokidoki",
            "category": "Noun, Adverb",
            "translation": "sometimes; at times",
            "alwaysShowHint": true,
            "kana": "ときどき"
        },
        {
            "id": "n5_vocab_571",
            "japanese": "所",
            "romanization": "tokoro",
            "category": "Noun, Suffix",
            "translation": "place",
            "alwaysShowHint": true,
            "kana": "ところ"
        },
        {
            "id": "n5_vocab_572",
            "japanese": "止まる",
            "romanization": "tomaru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to stop; to come to a halt",
            "alwaysShowHint": true,
            "kana": "とまる"
        },
        {
            "id": "n5_vocab_573",
            "japanese": "友達",
            "romanization": "tomodachi",
            "category": "Noun",
            "translation": "friend; companion",
            "alwaysShowHint": true,
            "kana": "ともだち"
        },
        {
            "id": "n5_vocab_574",
            "japanese": "隣",
            "romanization": "tonari",
            "category": "Noun",
            "translation": "next door to",
            "alwaysShowHint": true,
            "kana": "となり"
        },
        {
            "id": "n5_vocab_575",
            "japanese": "遠い",
            "romanization": "tooi",
            "category": "Adjective, い-adjective",
            "translation": "far",
            "alwaysShowHint": true,
            "kana": "とおい"
        },
        {
            "id": "n5_vocab_576",
            "japanese": "十日",
            "romanization": "tooka",
            "category": "Noun",
            "translation": "tenth day of the month / 10 days",
            "alwaysShowHint": true,
            "kana": "とおか"
        },
        {
            "id": "n5_vocab_577",
            "japanese": "鳥",
            "romanization": "tori",
            "category": "Noun",
            "translation": "bird",
            "alwaysShowHint": true,
            "kana": "とり"
        },
        {
            "id": "n5_vocab_578",
            "japanese": "鶏肉",
            "romanization": "toriniku",
            "category": "Noun",
            "translation": "chicken meat",
            "alwaysShowHint": true,
            "kana": "とりにく"
        },
        {
            "id": "n5_vocab_579",
            "japanese": "撮る",
            "romanization": "toru",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to take a photo or record a film",
            "alwaysShowHint": true,
            "kana": "とる"
        },
        {
            "id": "n5_vocab_580",
            "japanese": "取る",
            "romanization": "toru",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to take; to pick up; to harvest; to earn; to win; to choose",
            "alwaysShowHint": true,
            "kana": "とる"
        },
        {
            "id": "n5_vocab_581",
            "japanese": "年",
            "romanization": "toshi",
            "category": "Noun",
            "translation": "year; age",
            "alwaysShowHint": true,
            "kana": "とし"
        },
        {
            "id": "n5_vocab_582",
            "japanese": "図書館",
            "romanization": "toshokan",
            "category": "Noun",
            "translation": "library",
            "alwaysShowHint": true,
            "kana": "としょかん"
        },
        {
            "id": "n5_vocab_583",
            "japanese": "次",
            "romanization": "tsugi",
            "category": "Noun",
            "translation": "next",
            "alwaysShowHint": true,
            "kana": "つぎ"
        },
        {
            "id": "n5_vocab_584",
            "japanese": "一日",
            "romanization": "tsuitachi",
            "category": "Noun",
            "translation": "first day of the month",
            "alwaysShowHint": true,
            "kana": "ついたち"
        },
        {
            "id": "n5_vocab_585",
            "japanese": "疲れる",
            "romanization": "tsukareru",
            "category": "Verb, Ichidan verb, Intransitive verb",
            "translation": "to get tired",
            "alwaysShowHint": true,
            "kana": "つかれる"
        },
        {
            "id": "n5_vocab_586",
            "japanese": "使う",
            "romanization": "tsukau",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to use",
            "alwaysShowHint": true,
            "kana": "つかう"
        },
        {
            "id": "n5_vocab_587",
            "japanese": "つける",
            "romanization": "tsukeru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to turn on",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_588",
            "japanese": "着く",
            "romanization": "tsuku",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to arrive at",
            "alwaysShowHint": true,
            "kana": "つく"
        },
        {
            "id": "n5_vocab_589",
            "japanese": "机",
            "romanization": "tsukue",
            "category": "Noun",
            "translation": "desk",
            "alwaysShowHint": true,
            "kana": "つくえ"
        },
        {
            "id": "n5_vocab_590",
            "japanese": "作る",
            "romanization": "tsukuru",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to make",
            "alwaysShowHint": true,
            "kana": "つくる"
        },
        {
            "id": "n5_vocab_591",
            "japanese": "詰まらない",
            "romanization": "tsumaranai",
            "category": "Adjective, い-adjective",
            "translation": "boring",
            "alwaysShowHint": true,
            "kana": "つまらない"
        },
        {
            "id": "n5_vocab_592",
            "japanese": "冷たい",
            "romanization": "tsumetai",
            "category": "い-adjective",
            "translation": "cold to the touch",
            "alwaysShowHint": true,
            "kana": "つめたい"
        },
        {
            "id": "n5_vocab_593",
            "japanese": "勤める",
            "romanization": "tsutomeru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to work for someone",
            "alwaysShowHint": true,
            "kana": "つとめる"
        },
        {
            "id": "n5_vocab_594",
            "japanese": "強い",
            "romanization": "tsuyoi",
            "category": "Adjective, い-adjective",
            "translation": "powerful",
            "alwaysShowHint": true,
            "kana": "つよい"
        },
        {
            "id": "n5_vocab_595",
            "japanese": "上",
            "romanization": "ue",
            "category": "Noun, Suffix",
            "translation": "above; up; over; top; surface",
            "alwaysShowHint": true,
            "kana": "うえ"
        },
        {
            "id": "n5_vocab_596",
            "japanese": "生まれる",
            "romanization": "umareru",
            "category": "Verb, Ichidan verb, Intransitive verb",
            "translation": "to be born",
            "alwaysShowHint": true,
            "kana": "うまれる"
        },
        {
            "id": "n5_vocab_597",
            "japanese": "海",
            "romanization": "umi",
            "category": "Noun",
            "translation": "sea",
            "alwaysShowHint": true,
            "kana": "うみ"
        },
        {
            "id": "n5_vocab_598",
            "japanese": "売る",
            "romanization": "uru",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to sell",
            "alwaysShowHint": true,
            "kana": "うる"
        },
        {
            "id": "n5_vocab_599",
            "japanese": "煩い",
            "romanization": "urusai",
            "category": "い-adjective",
            "translation": "noisy, annoying",
            "alwaysShowHint": true,
            "kana": "うるさい"
        },
        {
            "id": "n5_vocab_600",
            "japanese": "後ろ",
            "romanization": "ushiro",
            "category": "Noun",
            "translation": "back; behind; rear",
            "alwaysShowHint": true,
            "kana": "うしろ"
        },
        {
            "id": "n5_vocab_601",
            "japanese": "薄い",
            "romanization": "usui",
            "category": "Adjective, い-adjective",
            "translation": "thin; weak",
            "alwaysShowHint": true,
            "kana": "うすい"
        },
        {
            "id": "n5_vocab_602",
            "japanese": "歌",
            "romanization": "uta",
            "category": "Noun",
            "translation": "song",
            "alwaysShowHint": true,
            "kana": "うた"
        },
        {
            "id": "n5_vocab_603",
            "japanese": "歌う",
            "romanization": "utau",
            "category": "Verb, Godan verb, Intransitive verb, Transitive verb",
            "translation": "to sing",
            "alwaysShowHint": true,
            "kana": "うたう"
        },
        {
            "id": "n5_vocab_604",
            "japanese": "上着",
            "romanization": "uwagi",
            "category": "Noun",
            "translation": "coat; tunic; jacket; outer garment",
            "alwaysShowHint": true,
            "kana": "うわぎ"
        },
        {
            "id": "n5_vocab_605",
            "japanese": "ワイシャツ",
            "romanization": "wai shatsu",
            "category": "Noun, Katakana, Wasei",
            "translation": "shirt",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_606",
            "japanese": "若い",
            "romanization": "wakai",
            "category": "Adjective, い-adjective",
            "translation": "young",
            "alwaysShowHint": true,
            "kana": "わかい"
        },
        {
            "id": "n5_vocab_607",
            "japanese": "分かる",
            "romanization": "wakaru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to understand; to comprehend; to grasp; to see; to get; to follow",
            "alwaysShowHint": true,
            "kana": "わかる"
        },
        {
            "id": "n5_vocab_608",
            "japanese": "悪い",
            "romanization": "warui",
            "category": "Adjective, い-adjective",
            "translation": "bad; poor; undesirable",
            "alwaysShowHint": true,
            "kana": "わるい"
        },
        {
            "id": "n5_vocab_609",
            "japanese": "忘れる",
            "romanization": "wasureru",
            "category": "Verb, Ichidan verb, Transitive verb",
            "translation": "to forget",
            "alwaysShowHint": true,
            "kana": "わすれる"
        },
        {
            "id": "n5_vocab_610",
            "japanese": "渡る",
            "romanization": "wataru",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to go across",
            "alwaysShowHint": true,
            "kana": "わたる"
        },
        {
            "id": "n5_vocab_611",
            "japanese": "私",
            "romanization": "watashi",
            "category": "Pronoun",
            "translation": "I; myself",
            "alwaysShowHint": true,
            "kana": "わたし"
        },
        {
            "id": "n5_vocab_612",
            "japanese": "渡す",
            "romanization": "watasu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to hand over",
            "alwaysShowHint": true,
            "kana": "わたす"
        },
        {
            "id": "n5_vocab_613",
            "japanese": "山",
            "romanization": "yama",
            "category": "Noun",
            "translation": "mountain; hill",
            "alwaysShowHint": true,
            "kana": "やま"
        },
        {
            "id": "n5_vocab_614",
            "japanese": "八百屋",
            "romanization": "yaoya",
            "category": "Noun",
            "translation": "greengrocer; fruit and vegetable shop; versatile",
            "alwaysShowHint": true,
            "kana": "やおや"
        },
        {
            "id": "n5_vocab_615",
            "japanese": "やる",
            "romanization": "yaru",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to do",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_616",
            "japanese": "野菜",
            "romanization": "yasai",
            "category": "Noun",
            "translation": "vegetable",
            "alwaysShowHint": true,
            "kana": "やさい"
        },
        {
            "id": "n5_vocab_617",
            "japanese": "易しい",
            "romanization": "yasashii",
            "category": "Adjective, い-adjective",
            "translation": "easy, simple",
            "alwaysShowHint": true,
            "kana": "やさしい"
        },
        {
            "id": "n5_vocab_618",
            "japanese": "安い",
            "romanization": "yasui",
            "category": "Adjective, い-adjective",
            "translation": "cheap; inexpensive",
            "alwaysShowHint": true,
            "kana": "やすい"
        },
        {
            "id": "n5_vocab_619",
            "japanese": "休み",
            "romanization": "yasumi",
            "category": "Noun",
            "translation": "rest; vacation; holiday",
            "alwaysShowHint": true,
            "kana": "やすみ"
        },
        {
            "id": "n5_vocab_620",
            "japanese": "休む",
            "romanization": "yasumu",
            "category": "Verb, Godan verb, Intransitive verb",
            "translation": "to be absent; to take a day off; to rest",
            "alwaysShowHint": true,
            "kana": "やすむ"
        },
        {
            "id": "n5_vocab_621",
            "japanese": "八つ",
            "romanization": "yattsu",
            "category": "Noun, Numeric",
            "translation": "eight: 8",
            "alwaysShowHint": true,
            "kana": "やっつ"
        },
        {
            "id": "n5_vocab_622",
            "japanese": "呼ぶ",
            "romanization": "yobu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to call out, to invite",
            "alwaysShowHint": true,
            "kana": "よぶ"
        },
        {
            "id": "n5_vocab_623",
            "japanese": "良い",
            "romanization": "yoi/ii",
            "category": "Adjective, い-adjective",
            "translation": "good",
            "alwaysShowHint": true,
            "kana": "よいいい"
        },
        {
            "id": "n5_vocab_624",
            "japanese": "四日",
            "romanization": "yokka",
            "category": "Noun",
            "translation": "fourth day of the month / 4 days",
            "alwaysShowHint": true,
            "kana": "よっか"
        },
        {
            "id": "n5_vocab_625",
            "japanese": "横",
            "romanization": "yoko",
            "category": "Noun",
            "translation": "beside,side,width",
            "alwaysShowHint": true,
            "kana": "よこ"
        },
        {
            "id": "n5_vocab_626",
            "japanese": "よく",
            "romanization": "yoku",
            "category": "Adverb",
            "translation": "often, well",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_627",
            "japanese": "読む",
            "romanization": "yomu",
            "category": "Verb, Godan verb, Transitive verb",
            "translation": "to read; to guess; to predict; to read (someone's thoughts)",
            "alwaysShowHint": true,
            "kana": "よむ"
        },
        {
            "id": "n5_vocab_628",
            "japanese": "夜",
            "romanization": "yoru",
            "category": "Noun",
            "translation": "evening; night",
            "alwaysShowHint": true,
            "kana": "よる"
        },
        {
            "id": "n5_vocab_629",
            "japanese": "四つ",
            "romanization": "yotsu",
            "category": "Noun, Numeric",
            "translation": "four; 4",
            "alwaysShowHint": true,
            "kana": "よつ"
        },
        {
            "id": "n5_vocab_630",
            "japanese": "洋服",
            "romanization": "youfuku",
            "category": "Noun",
            "translation": "western clothes",
            "alwaysShowHint": true,
            "kana": "ようふく"
        },
        {
            "id": "n5_vocab_631",
            "japanese": "八日",
            "romanization": "youka",
            "category": "Noun",
            "translation": "eighth day of the month / 8 days",
            "alwaysShowHint": true,
            "kana": "ようか"
        },
        {
            "id": "n5_vocab_632",
            "japanese": "弱い",
            "romanization": "yowai",
            "category": "Adjective, い-adjective",
            "translation": "weak",
            "alwaysShowHint": true,
            "kana": "よわい"
        },
        {
            "id": "n5_vocab_633",
            "japanese": "雪",
            "romanization": "yuki",
            "category": "Noun",
            "translation": "snow",
            "alwaysShowHint": true,
            "kana": "ゆき"
        },
        {
            "id": "n5_vocab_634",
            "japanese": "ゆっくり",
            "romanization": "yukkuri",
            "category": "Adverb",
            "translation": "slowly",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_635",
            "japanese": "昨夜",
            "romanization": "yuube",
            "category": "Noun",
            "translation": "last night",
            "alwaysShowHint": true,
            "kana": "ゆうべ"
        },
        {
            "id": "n5_vocab_636",
            "japanese": "郵便局",
            "romanization": "yuubinkyoku",
            "category": "Noun",
            "translation": "post office",
            "alwaysShowHint": true,
            "kana": "ゆうびんきょく"
        },
        {
            "id": "n5_vocab_637",
            "japanese": "夕方",
            "romanization": "yuugata",
            "category": "Noun",
            "translation": "evening; dusk",
            "alwaysShowHint": true,
            "kana": "ゆうがた"
        },
        {
            "id": "n5_vocab_638",
            "japanese": "夕飯",
            "romanization": "yuuhan",
            "category": "Noun",
            "translation": "evening meal",
            "alwaysShowHint": true,
            "kana": "ゆうはん"
        },
        {
            "id": "n5_vocab_639",
            "japanese": "有名",
            "romanization": "yuumei",
            "category": "Noun, Adjective, な-adjective",
            "translation": "famous",
            "alwaysShowHint": true,
            "kana": "ゆうめい"
        },
        {
            "id": "n5_vocab_640",
            "japanese": "雑誌",
            "romanization": "zasshi",
            "category": "Noun",
            "translation": "magazine",
            "alwaysShowHint": true,
            "kana": "ざっし"
        },
        {
            "id": "n5_vocab_641",
            "japanese": "全部",
            "romanization": "zenbu",
            "category": "Noun, Adverbial Noun",
            "translation": "all",
            "alwaysShowHint": true,
            "kana": "ぜんぶ"
        },
        {
            "id": "n5_vocab_642",
            "japanese": "ゼロ",
            "romanization": "zero",
            "category": "Noun",
            "translation": "zero",
            "alwaysShowHint": true
        },
        {
            "id": "n5_vocab_643",
            "japanese": "ズボン",
            "romanization": "zubon",
            "category": "Noun, Katakana",
            "translation": "trousers; pants",
            "alwaysShowHint": true
        }
    ];
}