import { useEffect, useState } from 'react';
import { createUseStyles } from 'react-jss';
import WordInput from './component/WordInput';
import WordViewer from './component/WordViewer';
import { getConsolidatedEntries, isQueryNLevelVocab } from './util/entry';
import { Entry } from "./interface/entry";
import { getRandomElement } from './util/random';
import ScoreViewer from './component/ScoreViewer';
import { ScoreRecord } from './interface/scoreRecord';
import WordPrompt from './component/WordPrompt';
import { WORD_PROMPT_STATE } from './enum/wordPrompt';
import CheckBox from './component/shared/CheckBox';
import { getN5Vocabulary } from './data/vocabulary';
import { playJapanesePronounciation } from './util/speechSynthesis';

const useStyles = createUseStyles({
  main: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    width: '100%',
    overflowX: 'hidden',
  },
  options: {
    display: 'flex',
    paddingBottom: '10px',
    overflow: 'auto',
  },
  header: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    padding: '20px 10px 0px 10px',
  },
  body: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  footer: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
    padding: '20px',
  }
}, {
  name: 'App',
});

interface PreviousAttempt {
  key: number;
  state: WORD_PROMPT_STATE;
  entry?: Entry;
}

function App() {
  const { main, header, options, body, footer } = useStyles();

  // Kana entry options
  const [ isMonograph, setIsMonograph ] = useState(true);
  const [ includeHiragana, setIncludeHiragana ] = useState(true);
  const [ includeKatakana, setIncludeKatakana ] = useState(true);

  // N-Level vocab entry options
  const [ includeKana, setIncludeKana ] = useState(true);

  /**
   * If set to true, each entry will be shown ONLY ONCE per cycle (one cycle is a run through the whole entries)
   */
  const [ isUnique, setIsUnique ] = useState(true);
  /**
   * If set to true, switches to N-level vocabulary mode
   */
  const [ isNLevelVocab, setIsNLevelVocab ] = useState(isQueryNLevelVocab());

  const [ entries, setEntries ] = useState<Array<Entry> | undefined>(undefined);
  const [ activeEntry, setActiveEntry ] = useState<Entry | undefined>(undefined);

  const [ scoreRecord, setScoreRecord ] = useState<ScoreRecord>({ correct: 0, total: 0 });

  const [ previousAttempt, setPreviousAttempt ] = useState<PreviousAttempt>({
    key: Date.now(),
    state: WORD_PROMPT_STATE.NONE, 
    entry: undefined 
  });

  function handleSubmit(answer: string) {
    if (!activeEntry) {
      console.warn('ActiveEntry is undefined!');
    } else {
      const { japanese, romanization } = activeEntry;
      const isCorrect = answer.toLowerCase() === romanization;
      updateScore(isCorrect);
      setPreviousAttempt({
        key: Date.now(),
        state: isCorrect ? WORD_PROMPT_STATE.CORRECT : WORD_PROMPT_STATE.WRONG,
        entry: activeEntry,
      });

      if (isCorrect && isUnique) {
        removeFromEntries(activeEntry); // Remove active entry from entries list if UNIQUE is on, and the answer is correct
      } else {
        randomizeActiveEntry(); // Randomize from entries list if UNIQUE is not on
      }

      playJapanesePronounciation(japanese);
    }
  }

  function updateScore(isCorrect: boolean) {
    const { correct, total } = scoreRecord;

    setScoreRecord({
      correct: correct + (isCorrect ? 1 : 0),
      total: total + 1
    });
  }

  function resetEntries() {
    const newEntries = isNLevelVocab ?
      /**
       * Currently only has n5 vocabs, will add more later  
       */ 
      getN5Vocabulary() :
      /**
       * Only kanas
       */
      getConsolidatedEntries(includeHiragana, includeKatakana, isMonograph);

    setEntries(newEntries);
  }

  function removeFromEntries(entryToRemove: Entry) {
    if (entries) {
      const newEntries = entries.filter(function (entry) {
        return entry !== entryToRemove;
      });

      if (newEntries.length > 0) {
        setEntries(newEntries); // If there is still remaining entries, set the new entries list excluding the entryToRemove
      } else {
        resetEntries(); // If there is no remaining entries, reset the entries list
      }
    } else {
      console.warn('Failed to remove from entries list: entries is undefined!');
    }
  }

  function randomizeActiveEntry() {
    if (entries) {
      setActiveEntry(getRandomElement(entries));
    } else {
      console.warn('Failed to randomize active entry: entries is undefined!');
    }
  }

  // Reset entries list whenever a flag is updated
  useEffect(resetEntries, [
    /**
     * Kana exclusive flags
     */
    isMonograph, includeHiragana, includeKatakana,
    /**
     * General flags 
     */ 
    isUnique, isNLevelVocab
  ]);

  // Randomize a new active entry whenever the entries list is updated
  useEffect(randomizeActiveEntry, [entries])

  return (
    <div className={ main }>
      <div className={ header }>
        <div className={ options }>
          {
            !isNLevelVocab &&
            <>
              <CheckBox
                isChecked={ isMonograph }
                labelText={ 'Monograph' }
                onClick={ () => { setIsMonograph(!isMonograph); }}
              />
              <CheckBox
                isChecked={ includeHiragana }
                labelText={ 'Hiragana' }
                onClick={ () => { setIncludeHiragana(!includeHiragana); }}
              />
              <CheckBox
                isChecked={ includeKatakana }
                labelText={ 'Katakana' }
                onClick={ () => { setIncludeKatakana(!includeKatakana); }}
              />
            </>
          }
          {
            isNLevelVocab &&
            <>
              <CheckBox
                isChecked={ includeKana }
                labelText={ 'Show Kana' }
                onClick={ () => { setIncludeKana(!includeKana) }}
              />
            </>
          }
          <CheckBox
            isChecked={ isUnique }
            labelText={ 'Unique' }
            onClick={ () => { setIsUnique(!isUnique); }}
          />
          <CheckBox
            isChecked={ isNLevelVocab }
            labelText={ 'N-Level Vocabulary' }
            onClick={ () => { setIsNLevelVocab(!isNLevelVocab); }}
          />
        </div>
        {
          previousAttempt.entry &&
          <WordPrompt
            key={ previousAttempt.key }
            state={ previousAttempt.state }
            entry={ previousAttempt.entry } 
          />
        }
      </div>
      <div className={ body }>
        {
          activeEntry &&
          <>
            <WordViewer
              entry={ activeEntry }
              showKana={ includeKana }
            />
            <WordInput
              onSubmit={ handleSubmit }
            />
          </>
        }
      </div>
      <div className={ footer }>
        <ScoreViewer
          scoreRecord={ scoreRecord }
        />
      </div>
    </div>
  );
}

export default App;
