export interface Entry {
    id: string;
    japanese: string;
    romanization: string;
    /**
     * Kana reading for kanjis (only applies to kanji vocabs)
     */
    kana?: string;
    /**
     * Only consists of a single character (only applies to kana entries)
     */
    isMonograph?: boolean;
    /**
     * E.g. verb, noun, pronoun, etc.
     */
    category?: string;
    /**
     * Meaning of the word in english
     */
    translation?: string;
    /**
     * Always show hint even if the answer is correct.
     * 
     * Also shows the japanese and kana (if exist) on top of the romanization
     */
    alwaysShowHint?: boolean;
}