export interface ScoreRecord {
    correct: number;
    total: number;
}