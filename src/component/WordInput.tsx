import { createUseStyles } from 'react-jss';
import { FormEvent, useRef } from 'react';
import { getFontSizeByREMRatio } from '../util/font';
import { isMobileOrTablet } from '../util/mobile';

const useStyles = createUseStyles({
    main: {
        width: '75%',
    },
    input: {
        fontFamily: 'Gotham, Helvetica, Arial, sans-serif', //Set manually because input does not inherit from body
        fontSize: getFontSizeByREMRatio(3),
        width: '100%',
        fontWeight: '300',
        textAlign: 'center',
        outline: 'none',
        border: '0',
    },
    submit: {
        display: 'none',
    },
}, {
    name: 'WordInput',
});

interface WordInputProps {
    onSubmit: (answer: string) => void
}

function WordInput({ onSubmit }: WordInputProps) {
    const { main, input, submit } = useStyles();
    const inputElement = useRef<HTMLInputElement>(null);

    function handleSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault(); //Prevent form submit from reloading the page
        
        const answer = getInputValue();
        onSubmit(answer);
        resetInputValue();
    }

    function getInputValue() {
        const { current } = inputElement;
        return current?.value || '';
    }

    function resetInputValue() {
        const { current } = inputElement;
        if (current) {
            current.value = '';
        }
    }

    return (
        <form className={ main } onSubmit={ handleSubmit }>
            <input 
                className={ input }
                type='text' 
                autoFocus={ true }
                required={ true }
                onBlur={(event) => {
                    if (!isMobileOrTablet()) {
                        event.target.focus();
                    }
                }}
                ref={ inputElement }
            />
            <input
                className={ submit } 
                type='submit' 
            />
        </form>
    );
}

export default WordInput;