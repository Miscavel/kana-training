import { createUseStyles } from "react-jss";
import { WORD_PROMPT_STATE } from "../enum/wordPrompt";
import { Entry } from "../interface/entry";
import { getFontSizeByREMRatio } from "../util/font";
import SpeakerButton from "./shared/SpeakerButton";

const useStyles = createUseStyles({
    main: {
        letterSpacing: '0.1rem',
        fontWeight: '400',
        fontSize: getFontSizeByREMRatio(0.85),
        textTransform: 'uppercase',
        textAlign: 'center',
    },
    // Anims
    '@keyframes fadeIn': {
        from: {opacity: 0},
        to: {opacity: 1}
    },
    '@keyframes fadeOut': {
        from: {opacity: 1},
        to: {opacity: 0}
    },
    // Correct prompt
    correctGroup: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        animation: '$fadeIn 0.5s linear, $fadeOut 0.5s linear 10s 1 normal forwards',
    },
    correctText: {

    },
    correctPlus: {
        color: '#5c6bc2',
        fontWeight: '500',
        fontSize: getFontSizeByREMRatio(2),
    },
    correctHintDiv: {

    },
    correctHintTopDiv: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: getFontSizeByREMRatio(1.5),
    },
    correctHintBottomDiv: {
        fontSize: getFontSizeByREMRatio(1.5),
    },
    correctHintText: {
        
    },
    // Wrong prompt
    wrongGroup: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        animation: '$fadeIn 0.5s linear, $fadeOut 0.5s linear 10s 1 normal forwards',
    },
    wrongRedText: {
        color: '#FF4136',
        fontWeight: '500',
    },
    wrongText: {

    },
    wrongHintDiv: {
        fontWeight: '500',
    },
    wrongHintTopDiv: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: getFontSizeByREMRatio(2),
    },
    wrongHintBottomDiv: {
        fontSize: getFontSizeByREMRatio(2),
    },
    wrongHintText: {
        
    },
}, {
    name: 'WordPrompt',
});

interface WordPromptProps {
    state: WORD_PROMPT_STATE;
    entry: Entry;
}

function WordPrompt({ state, entry }: WordPromptProps) {
    const { 
        main, 
        correctGroup, correctText, correctPlus, correctHintDiv, correctHintTopDiv, correctHintBottomDiv, correctHintText,
        wrongGroup, wrongRedText, wrongText, wrongHintDiv, wrongHintTopDiv, wrongHintBottomDiv, wrongHintText,
    } = useStyles();

    const { japanese, kana, romanization, translation, alwaysShowHint } = entry;
    const topHint = alwaysShowHint ? `${romanization} - ${japanese} ${kana ? `(${kana})` : ''}` : romanization;

    return (
        <div className={ main }>
            {
                {
                    [WORD_PROMPT_STATE.NONE]: <></>,
                    [WORD_PROMPT_STATE.CORRECT]: 
                        <div className={ correctGroup }>
                            <div className={ correctText }>
                                GOOD!
                            </div>
                            <br></br>
                            <div className={ correctPlus }>
                                +1
                            </div>
                            {
                                alwaysShowHint &&
                                <>
                                    <br></br>
                                    <div className={ correctHintDiv }>
                                        <div className={ correctHintTopDiv }>
                                            <div className={ correctHintText }>
                                                { topHint }
                                            </div>
                                            <SpeakerButton
                                                japanese={ japanese }
                                            />
                                        </div>
                                        <br></br>
                                        <div className={ correctHintBottomDiv }>
                                            <div className={ correctHintText }>
                                                { translation }
                                            </div>
                                        </div>
                                    </div>
                                </>
                            }
                        </div>,
                    [WORD_PROMPT_STATE.WRONG]:
                    <div className={ wrongGroup }>
                        <div className={ wrongRedText }>
                            NOPE!
                        </div>
                        <br></br>
                        <div className={ wrongText }>
                            CORRECT ANSWER WAS:
                        </div>
                        <br></br>
                        <div className={ wrongHintDiv }>
                            <div className={ wrongHintTopDiv }>
                                <div className={ wrongHintText }>
                                        { topHint }
                                </div>
                                <SpeakerButton
                                    japanese={ japanese }
                                />
                            </div>
                            <br></br>
                            <div className={ wrongHintBottomDiv }>
                                <div className={ wrongHintText }>
                                    { translation }
                                </div>
                            </div>
                        </div>
                    </div>
                }[state]
            }
        </div>
    );
}

export default WordPrompt;