import { createUseStyles } from "react-jss";
import { getFontSizeByREMRatio } from "../util/font";
import { ScoreRecord } from '../interface/scoreRecord';

const useStyles = createUseStyles({
   main: {
    
   },
   label: {
    fontSize: getFontSizeByREMRatio(0.75),
    fontWeight: '500',
    letterSpacing: '0.1rem',
    marginBottom: '7px',
   },
   score: {
    fontSize: getFontSizeByREMRatio(1),
    fontWeight: '400',
    letterSpacing: '0.2rem',
    color: '#b7b8b9',
   },
   correctSpan: {

   },
   totalSpan: {
    color: '#5c6bc2',
   },
}, {
    name: 'ScoreViewer',
});

interface ScoreViewerProps {
    scoreRecord: ScoreRecord;
}

function ScoreViewer({ scoreRecord }: ScoreViewerProps) {
    const { main, label, score, correctSpan, totalSpan } = useStyles();

    const { correct, total } = scoreRecord;
    
    return (
        <div className={ main }>
            <div className={ label }>
                YOUR SCORE
            </div>
            <div className={ score }>
                <span className={ correctSpan }>{ correct }</span>
                /
                <span className={ totalSpan }>{ total }</span>
            </div>
        </div>
    );
}

export default ScoreViewer;