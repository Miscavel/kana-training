import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
    main: {
        display: 'flex',
        alignItems: 'center',
        width: 'fit-content',
        marginRight: '10px',
    },
    checkBox: {
        WebkitAppearance: 'none',
        MozAppearance: 'none',
        border: '1px solid #b7b8b9',
        width: '18px',
        height: '18px',
        borderRadius: '2px',
        '&:checked': {
            borderColor: '#5c6bc2',
        },
        '&:checked:after': {
            content: `'\\f00c'`,
            fontFamily: 'fontawesome, Gotham',
            position: 'relative',
            top: '1px',
            left: '3px',
            color: '#5c6bc2',
        },
        '&:checked + $label': {
            color: '#5c6bc2',
        }
    },
    label: {
        color: '#b7b8b9',
        fontWeight: '500',
        fontSize: '0.75rem',
        letterSpacing: '0.1rem',
        textTransform: 'uppercase',
        marginLeft: '5px',
        cursor: 'default',
    }
}, {
    name: 'CheckBox',
});

interface CheckBoxProps {
    isChecked: boolean;
    labelText: string;
    onClick: () => void;
}

function CheckBox({ isChecked, labelText, onClick }: CheckBoxProps) {
    const { main, checkBox, label } = useStyles();
    return (
        <div className={ main } onClick={ onClick }>
            <input type='checkbox' className={ checkBox } checked={ isChecked } readOnly={ true }></input>
            <div className={ label }>{ labelText }</div>
        </div>
    );
}

export default CheckBox;