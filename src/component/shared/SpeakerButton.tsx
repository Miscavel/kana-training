import { createUseStyles } from "react-jss";
import { playJapanesePronounciation } from "../../util/speechSynthesis";

const useStyles = createUseStyles({
    main: {
        '&:hover': {
            cursor: 'pointer',
        }
    },
}, {
    name: 'SpeakerButton',
});

interface SpeakerButtonProps {
    japanese: string;
};

function SpeakerButton({ japanese }: SpeakerButtonProps) {
    const { main } = useStyles();

    function onSpeakerButtonClick() {
        playJapanesePronounciation(japanese);
    }

    return (
        <div className={ main } onClick={ onSpeakerButtonClick }>
            🔊
        </div>
    )
}

export default SpeakerButton;