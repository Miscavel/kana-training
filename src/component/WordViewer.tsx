import { createUseStyles } from 'react-jss';
import { Entry } from '../interface/entry';
import { getFontSizeByREMRatio } from '../util/font';
import SpeakerButton from './shared/SpeakerButton';

const useStyles = createUseStyles({
    main: {
        fontFamily: 'sans-serif',
        color: '#5c6bc2',
        textAlign: 'center',
    },
    wordDiv: {
        fontSize: getFontSizeByREMRatio(15),
    },
    bottomDiv: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        fontSize: getFontSizeByREMRatio(3),
    },
    kanaDiv: {
        
    }
}, {
    name: 'WordViewer',
});

interface WordViewerProps {
    entry: Entry;
    showKana: boolean;
};

function WordViewer({ entry, showKana }: WordViewerProps) {
    const { main, wordDiv, bottomDiv, kanaDiv } = useStyles();

    const { japanese, kana } = entry;

    return (
        <div className={ main }>
            <div className={ wordDiv }>
                { japanese }
            </div>
            <div className={ bottomDiv }>
                {
                    showKana &&
                    <div className={ kanaDiv }>
                        { `${kana ? `(${kana})` : ''}` }
                    </div>
                }
                <SpeakerButton
                    japanese={ japanese }
                />
            </div>
        </div>
    );
}

export default WordViewer;