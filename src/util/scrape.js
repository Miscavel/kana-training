// Function used to scrape data from https://jlptsensei.com/jlpt-n5-vocabulary-list/
function scrape() {
	const final = [ ...document.querySelector('#jl-vocab > tbody').querySelectorAll('.jl-row') ].reduce((collection, row) => {
		const rowData = [ ...row.children ].reduce((res, child) => {
			const firstChild = child.children[0];
			
			if (firstChild) {
				if (firstChild.childNodes.length > 1 && firstChild.childNodes[1].innerText) {
					res.push(firstChild.childNodes[0].nodeValue + ` (${firstChild.childNodes[1].innerText})`);
				} else {
					res.push(firstChild.childNodes[0].nodeValue);
				}
			} else {
				res.push(child.innerText);
			}
			return res;
		}, []);
		collection.push(rowData);
		return collection;
	}, []);
	console.log(final);
}

// For processing scrapped entries into Array<Entry>
// console.log(getN5Vocabulary().map((entry) => {
//     const { id, japanese, romanization, category, translation } = entry;
//     const trimmedRoman = romanization.replace(/\(.*\)/, '').trim();
//     const kana = romanization.match(/\(.*\)/)?.[0].replace(/[/(/)]/g, '');
//     const newEntry = {
//       id: `n5_vocab_${id}`,
//       japanese,
//       romanization: trimmedRoman,
//       category,
//       translation,
//       alwaysShowHint: true,
//     } as Entry;
//     if (kana) {
//       newEntry.kana = kana;
//     }
//     return newEntry;
// }));