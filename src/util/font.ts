/**
 * Returns font size based on REM ratio
 * 
 * @param ratio Ration in REM
 * @returns FontSize in string
 */
export function getFontSizeByREMRatio(ratio: number) {
    const rem = ratio;
    const vmin = (ratio * 10 / 3).toFixed(2);

    const fontSize = `min(${rem}rem, ${vmin}vmin)`; //Use the smaller value between rem and vmin to prevent word from appearing too large on small devices

    return fontSize;
}