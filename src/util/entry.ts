import { getHiraganaList } from "../data/hiragana";
import { getKatakanaList } from "../data/katakana";
import { getQueryParam } from "./queryParam";

/**
 * Returns an array of hiragana and katakana entries
 * 
 * @param hiragana Boolean (whether to include hiragana entries)
 * @param katakana Boolean (whether to include katakana entries)
 * @param isMonograph Boolean (whether to ONLY include mono entries)
 * @returns Array of entries
 */
export function getConsolidatedEntries(hiragana: boolean, katakana: boolean, isMonograph: boolean) {
    const entries = [];
    
    hiragana && entries.push(...getHiraganaList());
    katakana && entries.push(...getKatakanaList());

    return entries.filter(function (val) {
        return isMonograph ? val.isMonograph : true;
    });
}

/**
 * Check if user queries for N-level vocab
 * @returns boolean
 */
export function isQueryNLevelVocab() {
    return getQueryParam('nlevel') === 'true';
}