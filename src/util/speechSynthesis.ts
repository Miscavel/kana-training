/**
 * Play japanese pronounciation (text to speech)
 * @param japanese string
 */
export function playJapanesePronounciation(japanese: string) {
    const msg = new SpeechSynthesisUtterance();
    const voices = window.speechSynthesis.getVoices();
    msg.voice = voices[13]; // 13th voice is a japanese voice
    msg.volume = 1; // 0 to 1
    msg.rate = 1; // 0.1 to 10
    msg.pitch = 0; //0 to 2
    msg.text = japanese;
    msg.lang = 'ja-JP';
    speechSynthesis.cancel();
    speechSynthesis.speak(msg);
}