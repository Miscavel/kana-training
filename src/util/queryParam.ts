export function getQueryParam(param: string) {
    const url = new URL(window.location.href);
    return url.searchParams.get(param);
}