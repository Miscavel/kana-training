/**
 * Returns a random element from a given array of type T
 * 
 * @param array Array of type T
 * @returns Random element of type T
 */
export function getRandomElement<T>(array: Array<T>) {
    return array[Math.floor(Math.random() * array.length)];
}