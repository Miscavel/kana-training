export enum WORD_PROMPT_STATE {
    NONE = 'none',
    CORRECT = 'correct',
    WRONG = 'wrong',
}